package glrender

import (
	"github.com/go-gl/gl/v4.1-core/gl"
)

// BufferTracker tracks the buffers in the gl context
type BufferTracker struct {
	VAO            uint32
	VertexVBO      uint32 // TODO: this and tex could become 1 again
	TextureVBO     uint32
	InstanceTexVBO uint32
	ModelVBO       uint32
	ColorVBO       uint32
}

// NewBufferTracker creates a new BufferTracker and binds the VBOs and VAO in the GL program
func NewBufferTracker(vert, tex, instTex, model, color []float32) BufferTracker {
	trk := BufferTracker{
		VertexVBO:      VBO(vert), // these don't change per instance
		TextureVBO:     VBO(tex),  // these don't change per instance
		InstanceTexVBO: VBO(instTex),
		ModelVBO:       VBO(model),
		ColorVBO:       VBO(color),
	}
	trk.VAO = VAO(trk.VertexVBO, trk.TextureVBO, trk.InstanceTexVBO, trk.ModelVBO, trk.ColorVBO)
	return trk
}

// Delete deletes all buffers in the tracker
func (trk BufferTracker) Delete() {
	gl.DeleteVertexArrays(1, &trk.VAO)
	gl.DeleteBuffers(1, &trk.InstanceTexVBO)
	gl.DeleteBuffers(1, &trk.ModelVBO)
	gl.DeleteBuffers(1, &trk.TextureVBO)
	gl.DeleteBuffers(1, &trk.VertexVBO)
	gl.DeleteBuffers(1, &trk.ColorVBO)
}

// VBO creates a GL vertex buffer object
// TODO: consider passing in draw option, as gl.DYNAMIC_DRAW may be more fitting
func VBO(points []float32) uint32 {
	var vbo uint32
	gl.GenBuffers(1, &vbo)
	gl.BindBuffer(gl.ARRAY_BUFFER, vbo)
	gl.BufferData(gl.ARRAY_BUFFER, 4*len(points), gl.Ptr(points), gl.STATIC_DRAW)
	return vbo
}

// VAO creates a GL vertex array object from the vbos
func VAO(vert, tex, instanceTex, model, color uint32) uint32 {
	// create the vao
	var vao uint32
	gl.GenVertexArrays(1, &vao)
	gl.BindVertexArray(vao)

	// bind vertex vbo (vec2)
	gl.BindBuffer(gl.ARRAY_BUFFER, vert)
	gl.EnableVertexAttribArray(VertShaderPosition)
	gl.VertexAttribPointer(VertShaderPosition, 2, gl.FLOAT, false, 0, gl.PtrOffset(0))
	// TODO: should this be instanced?

	// bind texture vbo (vec2)
	gl.BindBuffer(gl.ARRAY_BUFFER, tex)
	gl.EnableVertexAttribArray(TextureShaderPosition)
	gl.VertexAttribPointer(TextureShaderPosition, 2, gl.FLOAT, false, 0, gl.PtrOffset(0))

	// bind instanceTex (offset, width height) vbo (vec4, instanced!)
	gl.BindBuffer(gl.ARRAY_BUFFER, instanceTex)
	gl.EnableVertexAttribArray(InstanceTexShaderPosition)
	gl.VertexAttribPointer(InstanceTexShaderPosition, 4, gl.FLOAT, false, 0, gl.PtrOffset(0))
	gl.VertexAttribDivisor(InstanceTexShaderPosition, 1) // instanced!

	// bind model vbo (vec4, instanced)
	// NOTE: this a little different - a mat4 must be passed in from 4 vec4s.
	// vec4 is the VAO max, so we consider 4 vec4 columns interleaved.
	// we need to stride and use proper pointer offsets.
	vec4sz := 4 * 4             // 4 float32s, or 4 * 4 bytes
	stride := int32(4 * vec4sz) // 4 columns of vec4 should be advanced
	gl.BindBuffer(gl.ARRAY_BUFFER, model)
	for i := 0; i < 4; i++ {
		layout := ModelShaderPositionColumn0 + uint32(i)
		gl.EnableVertexAttribArray(layout)
		gl.VertexAttribPointer(layout, 4, gl.FLOAT, false, stride, gl.PtrOffset(i*vec4sz))
		// instanced: needed to move once per shader iteration, not vertex coord
		gl.VertexAttribDivisor(layout, 1)
	}

	// bind color, instanced
	gl.BindBuffer(gl.ARRAY_BUFFER, color)
	gl.EnableVertexAttribArray(ColorShaderPosition)
	gl.VertexAttribPointer(ColorShaderPosition, 4, gl.FLOAT, false, 0, gl.PtrOffset(0))
	gl.VertexAttribDivisor(ColorShaderPosition, 1)

	// clear bound array and buffer before next sprite / batch
	gl.BindVertexArray(0)
	gl.BindBuffer(gl.ARRAY_BUFFER, 0)

	return vao
}

// quadTextureCoords creates default texture coordinates for a quad. These
// account for half-pixel correction
// bad w/ use of shader?
// func quadTextureCoords(tex *Texture) []float32 {
// 	x := 0.5 / tex.Width
// 	y := 0.5 / tex.Height
// 	return []float32{
// 		0.0 + x, 1.0 - y,
// 		1.0 - x, 0.0 + y,
// 		0.0 + x, 0.0 + y,
// 		0.0 + x, 1.0 - y,
// 		1.0 - x, 1.0 - y,
// 		1.0 - x, 0.0 + y,
// 	}
// }
