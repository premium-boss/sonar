package glrender

import (
	"reflect"

	"github.com/go-gl/gl/v4.1-core/gl"
	"github.com/go-gl/mathgl/mgl32"

	"gitlab.com/premium-boss/sonar/camera"
	"gitlab.com/premium-boss/sonar/ecs"
	phy "gitlab.com/premium-boss/sonar/physics"
)

// TODO: change to use scene entity (camera) and update JSON?
// we probably want to separate the follow component from the main camera projection
// and fix all the naming. Main camera projection belongs on sys entity
// TODO: pre-init hook for first projection update?

// DisplaySystem operates camera projection display. It belongs to the render phase.
type DisplaySystem struct {
	ecs.GameSystem
	Program uint32
	TCam reflect.Type
}

// NewDisplaySystem creates a new camera display system
func NewDisplaySystem(program uint32) *DisplaySystem {
	sys := DisplaySystem{
		Program: program,
		TCam: reflect.TypeOf(&camera.Component{}),
	}
	sys.SetConstraints(sys.TCam)
	return &sys
}

// Update updates the camera system
// TODO:: this must occur after render system or you get a jump. Is this expected?
// Somewhat - we can't change the projection before updating positions, so it makes some sense.
// However, it is creating a glitch between scenes.
func (sys *DisplaySystem) Update(scene *ecs.Scene, ents []ecs.Entity, gin ecs.GameInfo) {
	// NOTE: we expect one and exactly one entity
	// TODO: grab from scene entity instead
	ent := ents[0]
	cam := ent.Component(sys.TCam).(*camera.Component)
	// recreate?
	// TODO: cache better? enable / disable zoom?
	dim := cam.Dimensions.Scale(0.5 / cam.Zoom) // we want a half step in either direction
	bds := phy.R(cam.Center.Add(dim), cam.Center.Sub(dim))
	bds = clampToWorld(cam.WorldBounds, bds)
	if bds.Max.IsSame(cam.CurrentBounds.Max) &&
		bds.Min.IsSame(cam.CurrentBounds.Min) {
		return // no update
	}
	cam.CurrentBounds = bds
	sys.updateProjection(bds)
}

// Close closes the system and releases resources
func (sys *DisplaySystem) Close(ents []ecs.Entity) {
	// TODO: reset projection here?
}

// clampToWorld clamps the bounds within the absolute world boundaries.
// NOTE: this assumes that the camera bounds will intersect the world edge
// before they get completely beyond
func clampToWorld(worldBounds, bounds phy.Rect) phy.Rect {
	// NOT a box box collision. we want it completely inside
	overlap := bounds.Intersect(worldBounds)
	if overlap == bounds {
		return bounds
	}
	x := float32(0)
	y := float32(0)
	// check the sides and get push values
	deltaMax := worldBounds.Max.Sub(bounds.Max)
	deltaMin := worldBounds.Min.Sub(bounds.Min)
	if deltaMax.X < 0.0 {
		x = deltaMax.X
	} else if deltaMin.X > 0 {
		x = deltaMin.X
	}
	if deltaMax.Y < 0.0 {
		y = deltaMax.Y
	} else if deltaMin.Y > 0.0 {
		y = deltaMin.Y
	}
	push := phy.V(x, y)
	max := bounds.Max.Add(push)
	min := bounds.Min.Add(push)
	return phy.R(max, min)
}

func (sys *DisplaySystem) updateProjection(bds phy.Rect) {
	projection := mgl32.Ortho2D(bds.Min.X, bds.Max.X, bds.Max.Y, bds.Min.Y) // NOTE: yes, y coords rev
	projectionUniform := gl.GetUniformLocation(sys.Program, gl.Str(attributeNameProjection))
	gl.UniformMatrix4fv(projectionUniform, 1, false, &projection[0])
}

// JSONDisplaySystemFactory returns a factory function for creating the system
func JSONDisplaySystemFactory(program uint32) ecs.JSONSystemFactory {
	return func() (ecs.System, error) {
		return NewDisplaySystem(program), nil
	}
}
