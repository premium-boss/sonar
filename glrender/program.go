package glrender

import (
	"fmt"
	"log"
	"runtime"
	"strings"

	"github.com/go-gl/gl/v4.1-core/gl"
)

// TODO: we could set a uniform for the tex bounds of the sprite sheet rather
// than constantly juggle the tex coords in code!!!
// set once per batch and we're solid. Think about this!!

// NewProgram creates a new GL Program and sets up shaders
// NOTE: must be called from main thread!
func NewProgram() uint32 {
	runtime.LockOSThread() // here is why we must be called from main thread!
	program, err := initOpenGL()
	if err != nil {
		log.Fatalln("Program: error initializing", err)
	}
	gl.UseProgram(program)
	return program
}

// initOpenGL initializes the OpenGL context and initializes a program?
// TODO: wrap in a sync.Once or something instead? this is really a new program!
func initOpenGL() (uint32, error) {
	// Initialize Glow
	if err := gl.Init(); err != nil {
		panic(err)
	}
	//version := gl.GoStr(gl.GetString(gl.VERSION))
	//log.Println("OpenGL version", version)

	vertexShader, err := compileShader(defaultVertexGLSL, gl.VERTEX_SHADER) //vertexShaderSource
	if err != nil {
		panic(err)
	}
	fragmentShader, err := compileShader(defaultFragmentGLSL, gl.FRAGMENT_SHADER) // fragmentShaderSource
	if err != nil {
		panic(err)
	}

	// program: set up shaders
	program := gl.CreateProgram()
	gl.AttachShader(program, vertexShader)
	gl.AttachShader(program, fragmentShader)

	// NOTE: this is for Mac OpenGL 3.2 compatibility
	// we can't use the "layout" keyword in the shaders, so we should bind
	// attribute locations PRIOR to linking the program
	gl.BindAttribLocation(program, attributeLayoutVert, gl.Str(attributeNameVert))
	gl.BindAttribLocation(program, attributeLayoutVerTexCoord, gl.Str(attributeNameVertTexCoord))
	gl.BindAttribLocation(program, attributeLayoutInstanceTexCoord, gl.Str(attributeNameInstanceTex))
	gl.BindAttribLocation(program, attributeLayoutModelCoord, gl.Str(attributeNameModel))
	gl.BindAttribLocation(program, attributeLayoutColor, gl.Str(attributeNameColor))

	gl.LinkProgram(program)

	var status int32
	gl.GetProgramiv(program, gl.LINK_STATUS, &status)
	if status == gl.FALSE {
		var logLength int32
		gl.GetProgramiv(program, gl.INFO_LOG_LENGTH, &logLength)

		log := strings.Repeat("\x00", int(logLength+1))
		gl.GetProgramInfoLog(program, logLength, nil, gl.Str(log))

		return 0, fmt.Errorf("RenderManager: failed to link program: %v", log)
	}

	// clear them
	gl.DeleteShader(vertexShader)
	gl.DeleteShader(fragmentShader)

	return program, nil
}
