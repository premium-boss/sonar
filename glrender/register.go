package glrender

import "gitlab.com/premium-boss/sonar/ecs"

// RegisterComponents registers all component loader factories in pacakge
func RegisterComponents(reg map[string]ecs.JSONComponentFactory) {

}

// RegisterSystems registers system factory loaders in package
func RegisterSystems(reg map[string]ecs.JSONSystemFactory, program uint32, opts ...SystemOption) {
	reg["glrender.System"] = JSONSystemFactory(program, opts...) // TODO: do we like the pattern? overkill?
	reg["glrender.DisplaySystem"] = JSONDisplaySystemFactory(program)
}
