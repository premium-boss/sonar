package glrender

import (
	"fmt"
	"strings"

	"github.com/go-gl/gl/v4.1-core/gl"
)

// Shader attribute position constants, instead of layout attribute
const (
	VertShaderPosition = uint32(iota)
	TextureShaderPosition
	InstanceTexShaderPosition
	ModelShaderPositionColumn0
	ModelShaderPositionColumn1
	ModelShaderPositionColumn2
	ModelShaderPositionColumn3
	ColorShaderPosition
)

// Attribute names for shaders: must be null terminated
const (
	attributeNameModel        = "model\x00"
	attributeNameInstanceTex  = "instanceTex\x00"
	attributeNameProjection   = "projection\x00"
	attributeNameCamera       = "camera\x00"
	attributeNameTex          = "tex\x00"
	attributeNameVert         = "vert\x00"
	attributeNameVertTexCoord = "vertTexCoord\x00"
	attributeNameColor        = "color\x00"
)

// Attribute Locations for shaders: Mac OpenGL 3.2 compatibility: no Layout.
// We can't just iota it because a mat4, e.g. may take up several positions.
const (
	attributeLayoutVert             uint32 = 0
	attributeLayoutVerTexCoord      uint32 = 1
	attributeLayoutInstanceTexCoord uint32 = 2
	attributeLayoutModelCoord       uint32 = 3 // mat4: reserves 3,4,5,6
	attributeLayoutColor            uint32 = 7
)

// defaultVertexGLSL contains default vertex shader source for sprites.
// It expects the attributes to be set prior to linking the shader program for
// OpenGL 3.2 compatibility on Macs. vert and vertTexCoord are standard, while
// instanceTex and model are per-instance data. instanceTex x y are the offsets
// into the spritesheet, while z and w are width and height. See the swizzling.
// TODO: add coords for instanceTextColor?, though not per vertex?
const defaultVertexGLSL = `
#version 330

in vec2 vert;
in vec2 vertTexCoord;
in vec4 instanceTex;
in vec4 color;
in mat4 model;

out vec2 fragTexCoord;
out vec4 fragColor;
uniform mat4 projection;

void main()
{
	fragTexCoord = vertTexCoord * instanceTex.zw + instanceTex.xy;
	fragColor = color;
	gl_Position = projection * model * vec4(vert, 0.0, 1.0);
}
` + "\x00"

// defaultFragmentGLSL contains default fragment shader source for sprites.
const defaultFragmentGLSL = `
#version 330

in vec2 fragTexCoord;
in vec4 fragColor;
out vec4 outputColor;

uniform sampler2D tex;

void main()
{
	vec4 rgba = fragColor / 255.0;
	outputColor = rgba * texture(tex, fragTexCoord);
}
` + "\x00"

// compileShader takes shader source code as a string of GLSL and compiles a
// shader.
func compileShader(source string, shaderType uint32) (uint32, error) {
	shader := gl.CreateShader(shaderType)

	csources, free := gl.Strs(source)
	gl.ShaderSource(shader, 1, csources, nil)
	free()
	gl.CompileShader(shader)

	var status int32
	gl.GetShaderiv(shader, gl.COMPILE_STATUS, &status)
	if status == gl.FALSE {
		var logLength int32
		gl.GetShaderiv(shader, gl.INFO_LOG_LENGTH, &logLength)

		log := strings.Repeat("\x00", int(logLength+1))
		gl.GetShaderInfoLog(shader, logLength, nil, gl.Str(log))

		return 0, fmt.Errorf("compileShader: failed to compile shader %v: %v", source, log)
	}
	return shader, nil
}
