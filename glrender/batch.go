package glrender

import (
	"github.com/go-gl/gl/v4.1-core/gl"
	"github.com/pkg/errors"
	ren "gitlab.com/premium-boss/sonar/render"
	"log"
)

// Vertex counts
const (
	quadVerts    = 12 // 6 2-dimensional points (2 triangles of 3 vertices)
	modelVerts   = 16 // 4 vec4s (mat4)
	instTexVerts = 4  // vec4 (offset point, width and height), instanced!
	colorVerts   = 4  // vec4 (RGBA), instanced!s
)

// Sprites to start, rough estimate
const (
	startCap = 100
)

// Batch draws multiple Sprites using the same Texture at once
type Batch struct {
	Program uint32
	Texture ren.Texture
	Tracker BufferTracker

	count     int32
	prevCount int32
	vert      []float32
	tex       []float32
	instTex   []float32
	model     []float32
	color     []float32
}

// NewBatch creates a new SpriteBatch. It requires a spritesheet Texture
func NewBatch(spriteSheet ren.Texture, program uint32) (*Batch, error) {
	vert := quadVertexCoords()
	tex := quadVertexCoords()
	instTex := make([]float32, instTexVerts*startCap)
	model := make([]float32, modelVerts*startCap)
	color := make([]float32, colorVerts*startCap)
	// create vbos
	trk := NewBufferTracker(vert, tex, instTex, model, color)
	sb := Batch{
		Program: program,
		Texture: spriteSheet,
		Tracker: trk,
		vert:    vert,
		tex:     tex,
		instTex: instTex,
		model:   model,
		color:   color,
	}
	return &sb, nil
}

// Add adds a Sprite's data to the batch
// TODO: implement a more complex expand op as needed
func (sb *Batch) Add(sprite ren.Sprite) (bool, error) {
	if sb == nil {
		log.Fatalln("batch:Add - nil Batch!")
	}
	if sprite.Texture.Name != sb.Texture.Name {
		return false, errors.New("glrender:Batch: received Sprite with incompatible texture")
	}
	sb.instTex = append(sb.instTex, sprite.InstanceTex...)
	cmo := sprite.ModelMatrix.ColumnMajor() // TODO: expose for now, but otherwise, can we ditch til Adding? do we need? update in shader?
	sb.model = append(sb.model, cmo[:]...)
	// since we need to cast anyway, might as well make float here
	sb.color = append(sb.color, float32(sprite.Color.R),
		float32(sprite.Color.G),
		float32(sprite.Color.B),
		float32(sprite.Color.A))
	sb.count++
	return true, nil
}

// Clear clears the batch
// We're doing to do this per frame? Or check indexes? so we should
// TODO: CHECKLEAK
func (sb *Batch) Clear() {
	sb.instTex = sb.instTex[:0]
	sb.model = sb.model[:0]
	sb.color = sb.color[:0]
	sb.count = 0
}

// Update updates the Batch for Sprite changes within
func (sb *Batch) Update() {
	sb.update()
}

// Draw draws the batch
func (sb *Batch) Draw() {
	// draw instanced!
	// TODO: use shader if needed...
	gl.BindVertexArray(sb.Tracker.VAO)
	gl.ActiveTexture(gl.TEXTURE0)
	gl.BindTexture(gl.TEXTURE_2D, sb.Texture.ID)
	gl.DrawArraysInstanced(gl.TRIANGLES, 0, 6, sb.count)
}

// Close closes the batch, freeing GL buffers
func (sb *Batch) Close() {
	sb.Clear()
	sb.vert = nil
	sb.tex = nil
	sb.instTex = nil
	sb.model = nil
	sb.color = nil
	sb.Tracker.Delete()
}

// update updates the gl buffers for the Sprites in the batch
func (sb *Batch) update() {
	// sub data!
	// NOTE: we could check for a subset if dirty, but that would only help
	// if all the dirty sprites we're tightly packed. The intuition is that
	// multiple sub data calls would be worse then one call that swaps all,
	// possibly unnecessarily.
	// TODO: could also check if batch is at all dirty though
	var updater func([]float32)
	if sb.prevCount < sb.count {
		updater = reset
	} else {
		updater = swap
	}

	// update offset
	gl.BindBuffer(gl.ARRAY_BUFFER, sb.Tracker.InstanceTexVBO) // Note: not really needed for text...
	updater(sb.instTex)
	//gl.BufferSubData(gl.ARRAY_BUFFER, 0, 4*len(sb.instTex), gl.Ptr(sb.instTex))

	// update model
	gl.BindBuffer(gl.ARRAY_BUFFER, sb.Tracker.ModelVBO)
	updater(sb.model)
	//gl.BufferSubData(gl.ARRAY_BUFFER, 0, 4*len(sb.model), gl.Ptr(sb.model)) // can we use the array?

	// update color
	gl.BindBuffer(gl.ARRAY_BUFFER, sb.Tracker.ColorVBO)
	updater(sb.color)
	//gl.BufferSubData(gl.ARRAY_BUFFER, 0, 4*len(sb.color), gl.Ptr(sb.color))

	sb.prevCount = sb.count // TODO: do we want a way to reclaim?
}

func reset(coords []float32) {
	// TODO: don't like that we have this twice now (buffer_tracker.go)
	gl.BufferData(gl.ARRAY_BUFFER, 4*len(coords), gl.Ptr(coords), gl.STATIC_DRAW)
}

func swap(coords []float32) {
	gl.BufferSubData(gl.ARRAY_BUFFER, 0, 4*len(coords), gl.Ptr(coords))
}

// quadVertexCoords creates default vertex coordinates for a quad
func quadVertexCoords() []float32 {
	return []float32{
		0.0, 1.0,
		1.0, 0.0,
		0.0, 0.0,
		0.0, 1.0,
		1.0, 1.0,
		1.0, 0.0,
	}
}
