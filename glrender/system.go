package glrender

// TODO: move other gl logic (window)
//  - consider: if only one system uses them, just make that a glsystem and ditch the interface???

import (
	"gitlab.com/premium-boss/sonar/ecs"
	"gitlab.com/premium-boss/sonar/frame"
	ren "gitlab.com/premium-boss/sonar/render"
	"image/color"
	"log"
	"reflect"

	"github.com/go-gl/gl/v4.1-core/gl"
)

// System for OpenGL rendering
type System struct {
	ecs.GameSystem
	Program    uint32
	TFrame     reflect.Type
	TexManager ren.TextureManager
	Batches    map[string]*Batch // Do we expect many textures?
	DrawOrder  []string
}

// SystemOption to pass to initialization
type SystemOption func(sys *System)

// NewSystem creates a new System
func NewSystem(program uint32, opts ...SystemOption) *System {
	sys := System{
		TFrame:    reflect.TypeOf(&frame.Component{}),
		Batches:   make(map[string]*Batch),
		DrawOrder: make([]string, 0),
		Program:   program,
	}
	sys.SetConstraints(sys.TFrame)
	for _, opt := range opts {
		opt(&sys)
	}
	return &sys
}

// Update updates the System
func (sys *System) Update(scene *ecs.Scene, ents []ecs.Entity, gin ecs.GameInfo) {
	// TODO: decide if the scene should reg components or if you want to go through the pointer
	frame := scene.Entity.Component(sys.TFrame).(*frame.Component)
	for _, batch := range sys.Batches {
		batch.Clear()
	}
	for _, sprite := range frame.Sprites {
		sys.assignToBatch(sprite)
	}
	Clear()
	for _, nm := range sys.DrawOrder {
		batch := sys.Batches[nm]
		batch.Update()
		batch.Draw()
	}
}

// WithClearColor option
func WithClearColor(color color.RGBA) SystemOption {
	return func(sys *System) {
		var rel float32 = 255.0
		r := float32(color.R) / rel
		g := float32(color.G) / rel
		b := float32(color.B) / rel
		a := float32(color.A) / rel
		gl.ClearColor(r, g, b, a)
	}
}

// WithTexManager system option
func WithTexManager(tex ren.TextureManager) SystemOption {
	return func(sys *System) {
		sys.TexManager = tex
	}
}

// Clear clears widow
func Clear() {
	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
}

// Close closes the system and releases resources
func (sys *System) Close(ents []ecs.Entity) {
	for _, batch := range sys.Batches {
		batch.Close()
	}
}

// JSONSystemFactory returns a factory function for creating the system
func JSONSystemFactory(program uint32, opts ...SystemOption) ecs.JSONSystemFactory {
	return func() (ecs.System, error) {
		return NewSystem(program, opts...), nil
	}
}

func (sys *System) createBatch(name string) *Batch {
	// create the batch
	tex, ok := sys.TexManager.GetTexture(name)
	if !ok {
		log.Fatalln("glrender:System - renderable added with unknown texture:", name)
	}
	batch, err := NewBatch(tex, sys.Program)
	if err != nil {
		log.Fatalln("glrender:System: - failed to create batch for texture:", name)
	}
	return batch
}

func (sys *System) assignToBatch(sprite ren.Sprite) {
	nm := sprite.Texture.Name
	batch, ok := sys.Batches[nm]
	if !ok {
		batch = sys.createBatch(nm)
		sys.Batches[nm] = batch
		sys.DrawOrder = append(sys.DrawOrder, nm) // TODO:
	}
	batch.Add(sprite)
}
