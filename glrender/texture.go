package glrender

import (
	"fmt"
	"image"
	"image/draw"
	_ "image/png" // by default, we can decode pngs
	"os"

	"github.com/go-gl/gl/v4.1-core/gl"
	ren "gitlab.com/premium-boss/sonar/render"
)

// GLTextureManager is the OpenGL implementation of TextureManager
type GLTextureManager struct {
	Textures map[string]ren.Texture
	Program  uint32
}

// NewGLTextureManager creates a new instance of GLTextureManager
func NewGLTextureManager(program uint32) *GLTextureManager {
	mgr := GLTextureManager{
		Textures: make(map[string]ren.Texture),
		Program:  program,
	}
	textureUniform := gl.GetUniformLocation(mgr.Program, gl.Str(attributeNameTex))
	gl.Uniform1i(textureUniform, 0)
	gl.Enable(gl.TEXTURE_2D)
	gl.Enable(gl.BLEND)                          // enable for alpha channel / transparency with an RGBA
	gl.BlendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA) // this will let us draw over things! see Porter Duff composition methods
	return &mgr
}

// GetTexture returns a texture by name
func (mgr *GLTextureManager) GetTexture(name string) (ren.Texture, bool) {
	tex, ok := mgr.Textures[name]
	return tex, ok
}

// NewTextureFromFile creates a new GL texture from an image file. It binds the texture
// to the GL context and returns a rectangle for the image bounds.
func (mgr *GLTextureManager) NewTextureFromFile(file, name string) (ren.Texture, error) {
	// open file
	imgFile, err := os.Open(file)
	if err != nil {
		return ren.Texture{}, fmt.Errorf("texture %q not found on disk: %v", file, err)
	}
	// decode image
	img, _, err := image.Decode(imgFile)
	if err != nil {
		return ren.Texture{}, err
	}
	// draw image
	rgba := image.NewRGBA(img.Bounds())
	if rgba.Stride != rgba.Rect.Size().X*4 {
		return ren.Texture{}, fmt.Errorf("unsupported stride")
	}
	draw.Draw(rgba, rgba.Bounds(), img, image.Point{0, 0}, draw.Src)
	return mgr.NewTextureFromRGBA(rgba, name)
}

// NewTextureFromRGBA creates a new Texture from an RGBA, usually drawn onto
// after decoding an image
// NOTE: nearest seems to avoid the edge clipping issue, even without snapping pixels
// but if we need more, there are other strategies...
func (mgr *GLTextureManager) NewTextureFromRGBA(rgba *image.RGBA, name string) (ren.Texture, error) {
	// create GL texture
	var texture uint32
	gl.GenTextures(1, &texture)
	gl.ActiveTexture(gl.TEXTURE0)
	gl.BindTexture(gl.TEXTURE_2D, texture)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)
	gl.TexImage2D(
		gl.TEXTURE_2D,
		0,
		gl.RGBA,
		int32(rgba.Rect.Size().X),
		int32(rgba.Rect.Size().Y),
		0,
		gl.RGBA,
		gl.UNSIGNED_BYTE,
		gl.Ptr(rgba.Pix))
	// store in our struct
	bounds := rgba.Bounds()
	tex := ren.Texture{
		ID:     texture,
		Name:   name,
		Width:  float32(bounds.Max.X),
		Height: float32(bounds.Max.Y),
	}
	mgr.Textures[name] = tex
	return tex, nil
}

// Delete removes all GLTextures
func (mgr *GLTextureManager) Delete() {
	for _, tex := range mgr.Textures {
		gl.DeleteTextures(1, &tex.ID)
	}
}
