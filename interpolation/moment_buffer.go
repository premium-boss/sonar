package interpolation

import (
	"gitlab.com/premium-boss/sonar/tickbuf"
)

// TODO: any more and we've really timed out, but consider configuration
const momentBufferSize = 100

// MomentBuffer is a simple sequence buffer for tracking actor Moment.
type MomentBuffer struct {
	tb *tickbuf.Buffer
}

// NewMomentBuffer creates a new MomentBuffer
func NewMomentBuffer() *MomentBuffer {
	tbuf := NewTickBuffer(momentBufferSize)
	hbuf := MomentBuffer{tb: tbuf}
	return &hbuf
}

// Set sets a Moment at a given game tick
func (buf *MomentBuffer) Set(tick uint64, h *Moment) {
	buf.tb.Set(tick, h)
}

// Get retrieves a Moment at a given game tick
func (buf *MomentBuffer) Get(tick uint64) (bool, *Moment) {
	ok, data := buf.tb.Get(tick)
	if !ok {
		return ok, nil
	}
	h := data.(*Moment)
	if h == nil {
		return false, nil
	}
	return ok, h
}

// Clear ...
func (buf *MomentBuffer) Clear(tick uint64) {
	buf.tb.Clear(tick)
}
