package interpolation

import (
	"reflect"

	"gitlab.com/premium-boss/sonar/ecs"
)

// MaxSnaps details number of snaps we'll buffer. As this is a Client system,
// anything beyond pretty much indicates a timeout
const MaxSnaps int = 128

// HistorySystem is a system for interpolating movements based on History component
type HistorySystem struct {
	ecs.GameSystem
	T     reflect.Type
	Snaps chan *Snapshot
}

// NewHistorySystem creates a new History system
func NewHistorySystem() *HistorySystem {
	h := History{
		T:     reflect.TypeOf(&HistoryComponent{}),
		Snaps: make(chan *Snapshot, MaxSnaps),
	}
	h.SetConstraints(h.T)
	return &h
}

// Update updates the system. It applies MomentΔs to each entity's buffer.
func (sys *HistorySystem) Update(ents []ecs.Entity, gin ecs.GameInfo) {
	// NOTE: this algorithm assumes that both entities and Δs are ordered by ID
	// If we need to free up the scene at all, we must change this accordingly
	ok, snap := checkSnap(sys.Snaps)
	for ok {
		last := 0
		for i := range snap.Δs {
			for j, ent := range ents[last:] {
				if snap.Δs[i].ID == ent.ID() {
					hist := ent.Component(sys.T).(*HistoryComponent)
					// track latest
					snap.Δs[i].Restore(&hist.Latest)
					hist.Latest = snap.Δs[i]
					// if we have a buffer, use it for later interpolation
					if hist.Chronicle != nil {
						mo := Moment{Tick: gin.Tick, Δ: hist.Latest}
						hist.Chronicle.Set(gin.Tick, &mo)
					}
					last = j // start from here next loop
					break
				}
			}
		}
		ok, snap = checkSnap(sys.Snaps)
	}
}

func checkSnap(snaps <-chan *Snapshot) (bool, *Snapshot) {
	// non-blocking check
	select {
	case snap := <-snaps:
		return true, snap
	default:
		return false, nil
	}
}
