package interpolation

import (
	"reflect"

	"gitlab.com/premium-boss/sonar/ecs"
	"gitlab.com/premium-boss/sonar/input"
	"gitlab.com/premium-boss/sonar/movement"
)

// PredictionSystem system rewind / replays inputs after a server input / position
type PredictionSystem struct {
	ecs.GameSystem
	TPred reflect.Type
	THist reflect.Type
	TMov  reflect.Type
	TIpt  reflect.Type
}

// NewPredictionSystem creates a new Prediction system
func NewPredictionSystem() *PredictionSystem {
	pr := Prediction{
		TPred: reflect.TypeOf(&PredictionComponent{}),
		THist: reflect.TypeOf(&HistoryComponent{}),
		TMov:  reflect.TypeOf(&movement.Component{}),
		TIpt:  reflect.TypeOf(&input.Component),
	}
	pr.SetConstraints(pr.TPred, pr.THist, pr.TMov, pr.TIpt)
	return &pr
}

// Update updates the Prediction system
func (sys *PredictionSystem) Update(scene *ecs.Scene, ents []ecs.Entity, gin ecs.GameInfo) {
	for _, ent := range ents {
		pred := ent.Component(sys.TPred).(*Prediction)
		hist := ent.Component(sys.THist).(*HistoryComponent)
		mov := ent.Component(sys.TMov).(*movement.Component)
		ipt := ent.Component(sys.TIpt).(*input.Component)
		// see if we got a new server snap
		if pred.LastInputTickRx == hist.Latest.InputTick {
			continue
		}
		// if we've applied any inputs since, snap and reapply
		if pred.InputTick > hist.Latest.InputTick {
			mov.Mov = hist.Latest.Mov // snap
			for t := hist.Latest.InputTick + 1; t != pred.InputTick+1; t++ {
				ok, fut := pred.InputBuffer.Get(t)
				if ok {
					// update the player
					ipt.State = *fut
					mov.UpdateVelocity(ipt.State.Movement)
					mov.UpdateMovement(gin.DT)
				}
			}
		}

		// clean any inputs that have been received and won't be reapplied
		for tt := pred.LastInputTickRx + 1; tt != hist.Latest.InputTick+1; tt++ {
			pred.InputBuffer.Clear(tt)
		}

		// update tracker for next cleaning round
		pred.LastInputTickRx = hist.Latest.InputTick
	}
}
