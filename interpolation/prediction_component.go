package interpolation

import (
	"gitlab.com/premium-boss/sonar/ecs"
	"gitlab.com/premium-boss/sonar/input"
)

// PredictionComponent component replays inputs after a server input / position.
// Consider renaming to input tracking...
type PredictionComponent struct {
	InputTick       uint64
	InputBuffer     *input.StateBuffer
	LastInputTickRx uint64
}

// NewPredictionComponent creates a new Prediction component
func NewPredictionComponent() *PredictionComponent {
	return &PredictionComponent{
		InputBuffer: input.NewStateBuffer(),
	}
}

// Init initializes with entity
func (pr *PredictionComponent) Init(ent ecs.Entity) {
}

// OnUnregister handles hook for entity removing this component
func (pr *PredictionComponent) OnUnregister(ent ecs.Entity) {
}
