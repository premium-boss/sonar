package interpolation

import (
	"gitlab.com/premium-boss/sonar/ecs"
	phy "gitlab.com/premium-boss/sonar/physics"
)

// Δ bitmask constants.
const (
	MomentΔInput = uint8(1) << iota
	MomentΔState
	MomentΔMov
)

// HistoryComponent component tracks Moments for interpolation and prediction.
type HistoryComponent struct {
	Latest    MomentΔ
	Chronicle *MomentBuffer
}

// MomentΔ tracks general changes to an entity.
// We can send only the pieces we want over the network, indicated in ΔBits
type MomentΔ struct {
	ID        uint32
	ΔBits     uint8
	InputTick uint64
	State     uint8
	Mov       phy.Move2
}

// Moment maintains a moment in history
type Moment struct {
	Tick uint64
	Δ    MomentΔ
}

// Snapshot is a view of one game frame, and not necessarily the entire world.
type Snapshot struct {
	Tick   uint64
	Length uint8
	Δs     []MomentΔ
}

// NewHistoryComponent creates a new History component without a buffer
func NewHistoryComponent() *HistoryComponent {
	return &HistoryComponent{}
}

// NewBufferedHistoryComponent creates a new History component with a Moment Buffer
func NewBufferedHistoryComponent() *HistoryComponent {
	return &History{
		Chronicle: NewMomentBuffer(),
	}
}

// Init initializes with entity
func (h *HistoryComponent) Init(ent ecs.Entity) {
}

// OnUnregister handles hook for entity removing this component
func (h *HistoryComponent) OnUnregister(ent ecs.Entity) {
}

// ComputeΔ calculates a diff between two MomentΔs.
// It's not really a Δ in that we can't just apply and transform, but we only
// send what has changed which is nice.
func (mo *MomentΔ) ComputeΔ(prev *MomentΔ) {
	bits := uint8(0)
	if mo.InputTick != prev.InputTick {
		bits |= MomentΔInput
	}
	if mo.State != prev.State {
		bits |= MomentΔState
	}
	// TODO: we could get more granular here too w/ Movement diff
	if !mo.Mov.IsSame(&prev.Mov) {
		bits |= MomentΔMov
	}
	mo.ΔBits = bits
}

// Restore restores a new Δ based on previous Δ.
// If a field hasn't changed, it is preserved from the previous Δ.
func (mo *MomentΔ) Restore(prev *MomentΔ) {
	if !mo.HasDiff(MomentΔInput) {
		mo.InputTick = prev.InputTick
	}
	if !mo.HasDiff(MomentΔState) {
		mo.State = prev.State
	}
	if !mo.HasDiff(MomentΔInput) {
		mo.Mov = prev.Mov
	}
}

// HasDiff checks if a field has changed based on DeltaBits.
func (mo *MomentΔ) HasDiff(flag uint8) bool {
	return mo.ΔBits&flag != 0
}

// NewSnapshot creates a new snapshot from a slice of Δs
func NewSnapshot(tick uint64, Δs []MomentΔ) Snapshot {
	return Snapshot{
		Tick:   tick,
		Length: uint8(len(Δs)),
		Δs:     Δs,
	}
}
