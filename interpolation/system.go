package interpolation

import (
	"math"
	"reflect"

	"gitlab.com/premium-boss/sonar/movement"

	ani "gitlab.com/premium-boss/sonar/animation"
	"gitlab.com/premium-boss/sonar/ecs"
	mov "gitlab.com/premium-boss/sonar/movement"
)

// Interpolation constants
// NOTE: we might want to make this configurable
// for tol: at 50 FPS, DT is 20 ms, therefore 5 ticks inside the interp period,
// so 6 is extra, e.g. into last period. This should probably be a calculation
// based on DT settings
const (
	lerpDT float32 = 0.1
	tol    uint64  = 6
)

// System is a system for entity interpolation
type System struct {
	ecs.GameSystem
	THist reflect.Type
	TMov  reflect.Type
	TAni  reflect.Type
}

// NewSystem creates a new Interpolation system
func NewSystem() *System {
	i := System{
		THist: reflect.TypeOf(&HistoryComponent{}),
		TMov:  reflect.TypeOf(&movement.Component{}),
		TAni:  reflect.TypeOf(&ani.Component{}), // TODO: this is now a problem: need to split it out!
	}
	i.SetConstraints(i.THist, i.TMov, i.TAni)
	return &i
}

// Update updates the system
func (sys *System) Update(scene *ecs.Scene, ents []ecs.Entity, gin ecs.GameInfo) {
	ok, tick, start, end := getInterpolationTicks(gin)
	if !ok {
		return // too early
	}
	for _, ent := range ents {
		sys.interpolate(ent, tick, start, end)
	}
}

func (sys *System) interpolate(ent ecs.Entity, tick float32, s, e uint64) {
	hist := ent.Component(sys.THist).(*HistoryComponent)
	if hist.Chronicle == nil {
		return // nothing to interpolate: this is the player
	}
	found, st, startMo := findStartMoment(hist, s)
	if !found {
		return // nothing to interpolate between: we need a snap
	}
	found, et, endMo := findEndMoment(hist, e)
	// TODO: if not found, attempt to extrapolate end eventually (up to .25 sec)
	if !found {
		return
	}
	// interpolate
	// TODO: use new position comp
	mov := ent.Component(sys.TMov).(*mov.Component)
	anim := ent.Component(sys.TAni).(*ani.Component)
	alpha := lerpAmount(tick, st, et)
	mov.Previous = mov.Mov.Pos
	mov.Mov = startMo.Δ.Mov.Lerp(&endMo.Δ.Mov, alpha)
	anim.State.Previous = anim.State.Current
	anim.State.Current = startMo.Δ.State // NOTE: could consider using end state
}

func lerpAmount(tick float32, st, et uint64) float32 {
	// calculate lerp amount, but avoid NaN or Inf or -Inf on same tick that
	// would arise if we have an exact match
	// NOTE: we don't use the internal input tick: that is player-relative.
	// we use the tick of the snap that was retrieved!
	alpha := float32(0)
	if st != et {
		alpha = (tick - float32(st)) / float32(et-st)
	}
	return alpha
}

func findStartMoment(hist *HistoryComponent, s uint64) (bool, uint64, *Moment) {
	// NOTE: return the snap tick, not the player-relative input tick
	for t := s; t != s-tol-1; t-- {
		ok, mo := hist.Chronicle.Get(t)
		if ok {
			return true, t, mo
		}
	}
	return false, 0, nil
}

func findEndMoment(hist *HistoryComponent, e uint64) (bool, uint64, *Moment) {
	// NOTE: return the snap tick, not the player-relative input tick
	for t := e; t != e+tol+1; t++ {
		ok, mo := hist.Chronicle.Get(t)
		if ok {
			return true, t, mo
		}
	}
	return false, 0, nil
}

func getInterpolationTicks(gin ecs.GameInfo) (bool, float32, uint64, uint64) {
	rt := gin.Tot - lerpDT // find render time in seconds (client time - interp)
	if rt < 0 {
		return false, 0.0, 0, 0
	}
	tick := rt / gin.DT // find exactly what tick this would be (will be even w/ 50 ms! rt always in steps of dt!)
	tick64 := float64(tick)
	f := math.Floor(tick64)
	c := math.Ceil(tick64)
	s := uint64(f)
	e := uint64(c)
	return true, tick, s, e
}
