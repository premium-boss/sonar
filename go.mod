module gitlab.com/premium-boss/sonar

require (
	github.com/cheekybits/is v0.0.0-20150225183255-68e9c0620927 // indirect
	github.com/coconaut/glfw v0.0.0-20180408015004-31a6c51f0a32
	github.com/cryptix/wav v0.0.0-20180415113528-8bdace674401
	github.com/go-gl/gl v0.0.0-20180407155706-68e253793080
	github.com/go-gl/mathgl v0.0.0-20180319210751-5ab0e04e1f55
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0
	github.com/pkg/errors v0.8.0
	golang.org/x/image v0.0.0-20180403161127-f315e4403028
	golang.org/x/mobile v0.0.0-20180922163855-920b52be609a
)
