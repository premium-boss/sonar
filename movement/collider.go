package movement

import (
	"encoding/json"
	"log"

	"gitlab.com/premium-boss/sonar/ecs"
	phy "gitlab.com/premium-boss/sonar/physics"
)

// ColliderComponent for collision detection
type ColliderComponent struct {
	IsActive     bool
	ColliderType string
	Collider     phy.Collider
}

// Init initializes component with entity
func (cc *ColliderComponent) Init(ent ecs.Entity) {
	pos, pOk := ent.Component(tPos).(*PositionComponent)
	if pOk {
		// TODO: if not, should we error hard?
		cc.Collider.Update(pos.Value)
	}
}

// OnUnregister handles unregistering component
func (cc *ColliderComponent) OnUnregister(ent ecs.Entity) {}

// JSONColliderComponentFactory returns factory function for loading components from JSON
func JSONColliderComponentFactory(reg phy.ColliderRegistry) ecs.JSONComponentFactory {
	return func(ent ecs.Entity, desc ecs.JSONComponentDescriptor) (ecs.Component, error) {
		cc := ColliderComponent{}
		err := json.Unmarshal(desc.Data, &cc)
		if err != nil {
			return nil, err
		}
		colliderFac, ok := reg[cc.ColliderType]
		if !ok {
			log.Fatalln("bad collider name on collider component")
		}
		collider := colliderFac()
		// now, unmarshall the data into the collider itself.
		// we expect the necessary rectangle, radius, etc. depending on type
		err = json.Unmarshal(desc.Data, collider)
		if err != nil {
			return nil, err
		}
		cc.Collider = collider
		// update for the first position, esp. for static comps
		// TODO: do we like enforcing load order here? might make serialization hard... maybe specify in data...
		cc.Init(ent) // TODO: consider returning an error here
		return &cc, nil
	}
}
