package movement

import (
	"reflect"

	"gitlab.com/premium-boss/sonar/ecs"
	phy "gitlab.com/premium-boss/sonar/physics"
)

var tPos = reflect.TypeOf(&PositionComponent{})
var tSpace = reflect.TypeOf(&SpaceComponent{})

// TODO: switch to value? Would this mess up spatial hash at all?
// TODO: why even have this??? just use entity? Can we extend like that?

// EntitySpacer for placing entities in a spatial hash
type EntitySpacer struct {
	Entity ecs.Entity
}

// NewEntitySpacer creates the EntitySpacer wrapped around the entity
func NewEntitySpacer(ent ecs.Entity) *EntitySpacer {
	return &EntitySpacer{Entity: ent}
}

// Position gets entity position
func (es *EntitySpacer) Position() phy.Vec2 {
	pos := es.Entity.Component(tPos).(*PositionComponent)
	return pos.Value
}

// Dimensions gets entity width and height
// TODO: consider a separate component from Sprite?
func (es *EntitySpacer) Dimensions() phy.Vec2 {
	spc := es.Entity.Component(tSpace).(*SpaceComponent)
	return spc.Dimensions
}

// IsSame determines if this is the same spaceable. // TODO better name? Equals?
func (es *EntitySpacer) IsSame(s phy.Spaceable) bool {
	if other, ok := s.(*EntitySpacer); ok {
		return es.Entity.ID == other.Entity.ID
	}
	return false
}
