package movement

import (
	"encoding/json"

	"gitlab.com/premium-boss/sonar/ecs"
	"gitlab.com/premium-boss/sonar/physics"
)

// VelocityComponent tracks velocity
type VelocityComponent struct {
	Value physics.Vec2
}

// NewVelocityComponent creates the component
func NewVelocityComponent(vel physics.Vec2) *VelocityComponent {
	return &VelocityComponent{Value: vel}
}

// Init initializes the Movement component with an entity
func (m *VelocityComponent) Init(ent ecs.Entity) {}

// OnUnregister handles hook for entity removing this component
func (m *VelocityComponent) OnUnregister(ent ecs.Entity) {}

// JSONVelocityComponentFactory returns factory function for loading components from JSON
func JSONVelocityComponentFactory() ecs.JSONComponentFactory {
	return func(ent ecs.Entity, desc ecs.JSONComponentDescriptor) (ecs.Component, error) {
		vc := VelocityComponent{}
		err := json.Unmarshal(desc.Data, &vc)
		if err != nil {
			return nil, err
		}
		vc.Init(ent)
		return &vc, nil
	}
}
