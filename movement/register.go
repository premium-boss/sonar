package movement

import (
	"gitlab.com/premium-boss/sonar/ecs"
	phy "gitlab.com/premium-boss/sonar/physics"
)

// RegisterComponents registers all component loader factories in pacakge
func RegisterComponents(reg map[string]ecs.JSONComponentFactory, colliders phy.ColliderRegistry) {
	reg["movement.PositionComponent"] = JSONPositionComponentFactory()
	reg["movement.VelocityComponent"] = JSONVelocityComponentFactory()
	reg["movement.Component"] = JSONComponentFactory()
	reg["movement.SpaceComponent"] = JSONSpaceComponentFactory()
	reg["movement.ColliderComponent"] = JSONColliderComponentFactory(colliders)
}

// RegisterSystems register system factory function loaders for this package
func RegisterSystems(reg map[string]ecs.JSONSystemFactory, sh *phy.SpatialHash) {
	reg["movement.PlayerMovementSystem"] = JSONPlayerMovementSystemFactory(sh)
	reg["movement.StaticMovementSystem"] = JSONStaticMovementSystemFactory(sh)
}
