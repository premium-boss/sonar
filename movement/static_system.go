package movement

import (
	"reflect"

	"gitlab.com/premium-boss/sonar/ecs"
	phy "gitlab.com/premium-boss/sonar/physics"
)

// StaticMovementSystem tracks positions in a spatial hash
type StaticMovementSystem struct {
	ecs.GameSystem
	TPos        reflect.Type
	TSpace      reflect.Type
	TSprite     reflect.Type
	SpatialHash *phy.SpatialHash
}

// NewStaticMovementSystem creates a new system. It shares the spatial hash
// among other systems.
func NewStaticMovementSystem(sh *phy.SpatialHash) *StaticMovementSystem {
	mov := StaticMovementSystem{
		TPos:        reflect.TypeOf(&PositionComponent{}),
		TSpace:      reflect.TypeOf(&SpaceComponent{}),
		SpatialHash: sh,
	}
	mov.SetConstraints(mov.TPos, mov.TSpace)
	return &mov
}

// Update updates the system.
// TODO: consider a way to bail after initial load? detect dirty, etc? or a better
// way to set these up in a shared Space upon creation of entities / position components?
// if we include sh in Context at load time, we could ditch this system...
func (sys *StaticMovementSystem) Update(scene *ecs.Scene, ents []ecs.Entity, gin ecs.GameInfo) {
	for _, ent := range ents {
		space := ent.Component(sys.TSpace).(*SpaceComponent) // TOOO!
		if space.IsTracked {
			continue
		}
		// TODO: can we make this a struct instead of the pointer! probably a lot of allocation wasted here...
		es := NewEntitySpacer(ent)
		sys.SpatialHash.Add(es)
		space.IsTracked = true
	}
}

// Close closes the system and releases resources
func (sys *StaticMovementSystem) Close(ents []ecs.Entity) {
}

// JSONStaticMovementSystemFactory returns a factory function for creating the system
func JSONStaticMovementSystemFactory(sh *phy.SpatialHash) ecs.JSONSystemFactory {
	return func() (ecs.System, error) {
		return NewStaticMovementSystem(sh), nil
	}
}
