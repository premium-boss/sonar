package movement

import (
	"encoding/json"

	"gitlab.com/premium-boss/sonar/ecs"
	"gitlab.com/premium-boss/sonar/physics"
)

// Component is a component for movement and physics state
// TODO: make dedicated player movement? or a flag?
type Component struct {
	PrevPos   physics.Vec2 // track previous position
	IsDynamic bool
}

// NewComponent creates a new Movement component
func NewComponent(dyn bool) *Component {
	return &Component{IsDynamic: dyn}
}

// Init initializes the Movement component with an entity
func (m *Component) Init(ent ecs.Entity) {
}

// OnUnregister handles hook for entity removing this component
func (m *Component) OnUnregister(ent ecs.Entity) {
}

// JSONComponentFactory returns factory function for loading components from JSON
func JSONComponentFactory() ecs.JSONComponentFactory {
	return func(ent ecs.Entity, desc ecs.JSONComponentDescriptor) (ecs.Component, error) {
		mc := Component{}
		err := json.Unmarshal(desc.Data, &mc)
		if err != nil {
			return nil, err
		}
		mc.Init(ent)
		return &mc, nil
	}
}
