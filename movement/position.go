package movement

import (
	"encoding/json"

	"gitlab.com/premium-boss/sonar/ecs"
	"gitlab.com/premium-boss/sonar/physics"
)

// PositionComponent tracks position. We could have made it directly a Vec2, but
// that would require us to correctly assign to the dereferenced pointer to
// properly update the component's value, and this would likely be error-prone.
type PositionComponent struct {
	Value physics.Vec2
}

// NewPositionComponent creates the component
func NewPositionComponent(pos physics.Vec2) *PositionComponent {
	return &PositionComponent{Value: pos}
}

// Init initializes the Position component with an entity
func (m *PositionComponent) Init(ent ecs.Entity) {}

// OnUnregister handles hook for entity removing this component
func (m *PositionComponent) OnUnregister(ent ecs.Entity) {}

// JSONPositionComponentFactory returns factory function for loading components from JSON
func JSONPositionComponentFactory() ecs.JSONComponentFactory {
	return func(ent ecs.Entity, desc ecs.JSONComponentDescriptor) (ecs.Component, error) {
		pc := PositionComponent{}
		err := json.Unmarshal(desc.Data, &pc)
		if err != nil {
			return nil, err
		}
		pc.Init(ent)
		return &pc, nil
	}
}
