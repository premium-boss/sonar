package movement

import (
	"reflect"

	ani "gitlab.com/premium-boss/sonar/animation"
	"gitlab.com/premium-boss/sonar/ecs"
	"gitlab.com/premium-boss/sonar/input"
	phy "gitlab.com/premium-boss/sonar/physics"
)

// Movement constants
// TODO: move to the component, make settable, etc.
const (
	speed float32 = 250.0
)

// PlayerMovementSystem handles player movement.
type PlayerMovementSystem struct {
	ecs.GameSystem
	TMov   reflect.Type
	TPos   reflect.Type
	TVel   reflect.Type
	TCol   reflect.Type
	TIpt   reflect.Type
	TAni   reflect.Type
	TSpace reflect.Type
	Space  *phy.SpatialHash
}

// NewPlayerMovementSystem creates a new movement system. It shares a spatial
// hash among other systems.
func NewPlayerMovementSystem(sh *phy.SpatialHash) *PlayerMovementSystem {
	mov := PlayerMovementSystem{
		TMov:   reflect.TypeOf(&Component{}),
		TSpace: reflect.TypeOf(&SpaceComponent{}),
		TPos:   reflect.TypeOf(&PositionComponent{}),
		TVel:   reflect.TypeOf(&VelocityComponent{}), // add accel if needed
		TCol:   reflect.TypeOf(&ColliderComponent{}),
		TIpt:   reflect.TypeOf(&input.Component{}),
		TAni:   reflect.TypeOf(&ani.Component{}),
		Space:  sh,
	}
	mov.SetConstraints(mov.TMov, mov.TSpace, mov.TPos, mov.TVel, mov.TIpt, mov.TAni) // his is tough: we need to fetch static comps? dif set? anti constraint?
	return &mov
}

// Update updates the Movement and StateTracking.
// NOTE: initial input / velocity is expected to have been updated already?
// by the Input system (for player) or an AI system (as needed for npcs / enemies)
// TODO: consider passing in scene?
// TODO: consider calling this a player movement system instead?
func (sys *PlayerMovementSystem) Update(scene *ecs.Scene, ents []ecs.Entity, gin ecs.GameInfo) {
	for _, ent := range ents {
		mov := ent.Component(sys.TMov).(*Component)
		if !mov.IsDynamic {
			continue
		}
		ani := ent.Component(sys.TAni).(*ani.Component)
		pos := ent.Component(sys.TPos).(*PositionComponent)
		vel := ent.Component(sys.TVel).(*VelocityComponent)
		ipt := ent.Component(sys.TIpt).(*input.Component)
		col, cOk := ent.Component(sys.TCol).(*ColliderComponent)
		//spc := ent.Component(sys.TSpace).(*SpaceComponent)

		// remove prev pos from spatial hash
		// TODO: watch position change before this but after entry?
		// TODO: if we didn't move, avoid this? unless collisions too?
		// TODO: can we make this a struct instead of the pointer! probably a lot of allocation wasted here...
		es := NewEntitySpacer(ent)
		sys.Space.Remove(es)

		// update the entity
		mov.PrevPos = pos.Value
		updateVelocity(ipt, vel)         // update velocity from input (NOTE: not everything will have ipt...)
		updatePosition(vel, pos, gin.DT) // update position from velocity

		// find colliders and adjust
		if cOk && col.IsActive {
			col.Collider.Update(pos.Value)
			others := sys.Space.Nearby(es)
			for _, oth := range others {
				// cast to EntitySpacer as needed.... fetch collider? colliion test, move for mtv
				// TODO: may have to check for self if we don't remove above
				// TODO: may have to de-dup? or de-dup in the spatial hash? or get rid of spaceable interface
				// and use something concrete
				oes, ok := oth.(*EntitySpacer)
				if !ok {
					continue
				}
				othCol, othOk := oes.Entity.Component(sys.TCol).(*ColliderComponent)
				if !othOk || !othCol.IsActive {
					continue
				}

				collision := col.Collider.Collision(othCol.Collider)
				if !collision.IsColliding {
					continue
				}
				// TODO: any effects on collisions
				// TODO: need to update this collider's position, possibly the other? maybe part of "effects"
				pos.Value = pos.Value.Add(collision.MTV)
				col.Collider.Update(pos.Value)
			}
		}

		// update the spatial hash with final position
		sys.Space.Add(es)

		// anything that moves should have an animation, even if just 1 idle / static...
		ani.State.Previous = ani.State.Current
		ani.State.Current = state(vel.Value)
	}
}

// Close closes the system and releases resources
func (sys *PlayerMovementSystem) Close(ents []ecs.Entity) {
}

// JSONPlayerMovementSystemFactory returns a factory function for creating the system
func JSONPlayerMovementSystemFactory(sh *phy.SpatialHash) ecs.JSONSystemFactory {
	return func() (ecs.System, error) {
		return NewPlayerMovementSystem(sh), nil
	}
}

func updateVelocity(ipt *input.Component, vel *VelocityComponent) {
	vel.Value = ipt.State.Movement
	if !vel.Value.IsZero() {
		vel.Value = vel.Value.Normalize().Scale(speed)
	}
}

func updatePosition(vel *VelocityComponent, pos *PositionComponent, dt float32) {
	dp := vel.Value.Scale(dt)
	pos.Value = pos.Value.Add(dp)
}

func state(v phy.Vec2) uint8 {
	var s uint8
	switch {
	case v.IsZero():
		s = ani.Idle
	case v.X > 0.0:
		s = ani.WalkRight
	case v.X < 0.0:
		s = ani.WalkLeft
	case v.Y < 0.0:
		s = ani.Climb
	default:
		s = ani.Jump // TODO: add more here!!!
	}
	return s
}
