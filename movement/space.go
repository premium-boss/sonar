package movement

import (
	"encoding/json"

	"gitlab.com/premium-boss/sonar/ecs"
	"gitlab.com/premium-boss/sonar/physics"
)

// SpaceComponent tracks space occupancy feature of an entity
type SpaceComponent struct {
	IsTracked  bool
	Dimensions physics.Vec2
}

// Init inits component with Entity. We fetch the target component by target
// type, tracking this as our spaceable
func (c *SpaceComponent) Init(ent ecs.Entity) {

}

// OnUnregister hook
func (c *SpaceComponent) OnUnregister(ent ecs.Entity) {}

// JSONSpaceComponentFactory returns factory function for loading components from JSON
func JSONSpaceComponentFactory() ecs.JSONComponentFactory {
	return func(ent ecs.Entity, desc ecs.JSONComponentDescriptor) (ecs.Component, error) {
		sc := SpaceComponent{}
		err := json.Unmarshal(desc.Data, &sc)
		if err != nil {
			return nil, err
		}
		sc.Init(ent)
		return &sc, nil
	}
}
