package movement

import (
	"gitlab.com/premium-boss/sonar/ecs"
	"gitlab.com/premium-boss/sonar/physics"
)

// AccelerationComponent tracks velocity
type AccelerationComponent struct {
	Value physics.Vec2
}

// NewAccelerationComponent creates the component
func NewAccelerationComponent(acc physics.Vec2) *AccelerationComponent {
	return &AccelerationComponent{Value: acc}
}

// Init initializes the Movement component with an entity
func (m *AccelerationComponent) Init(ent ecs.Entity) {}

// OnUnregister handles hook for entity removing this component
func (m *AccelerationComponent) OnUnregister(ent ecs.Entity) {}
