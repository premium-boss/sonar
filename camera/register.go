package camera

import (
	"gitlab.com/premium-boss/sonar/ecs"
)

// RegisterComponents registers all loader factories in pacakge
func RegisterComponents(reg map[string]ecs.JSONComponentFactory) {
	reg["camera.Component"] = JSONComponentFactory()
}

// RegisterSystems registers systems in package
func RegisterSystems(reg map[string]ecs.JSONSystemFactory) {
	reg["camera.ControlSystem"] = JSONControlSystemFactory()
}
