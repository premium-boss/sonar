package camera

import (
	"encoding/json"
	//"math"

	"gitlab.com/premium-boss/sonar/ecs"
	"gitlab.com/premium-boss/sonar/physics"
)

// Component is a basic camera component. It contains the bounds that will be
// used in an Ortho2D projection. We can build a system to manipulate it.
type Component struct {
	Center     physics.Vec2
	Dimensions physics.Vec2
	Zoom       float32
	// TODO: can we ditch o.g. center then?
	CurrentBounds physics.Rect
	WorldBounds   physics.Rect
}

// NewComponent creates a new camera component
func NewComponent(dim physics.Vec2) *Component {
	center := physics.V(dim.X/2.0, dim.Y/2.0)
	return &Component{
		Center:     center,
		Dimensions: dim,
		Zoom:       1.0,
	}
}

// Init inits component with Entity
func (c *Component) Init(ent ecs.Entity) {
}

// OnUnregister hook
func (c *Component) OnUnregister(ent ecs.Entity) {}

// JSONComponentFactory returns a factory function for loading components from JSON
func JSONComponentFactory() ecs.JSONComponentFactory {
	return func(ent ecs.Entity, desc ecs.JSONComponentDescriptor) (ecs.Component, error) {
		cc := Component{}
		err := json.Unmarshal(desc.Data, &cc)
		if err != nil {
			return nil, err
		}
		cc.Init(ent)
		return &cc, nil
	}
}
