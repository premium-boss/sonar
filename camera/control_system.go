package camera

import (
	"reflect"

	"gitlab.com/premium-boss/sonar/ecs"
	"gitlab.com/premium-boss/sonar/input"
	"gitlab.com/premium-boss/sonar/movement"
)

// Camera constants
const (
	MaxZoom    = 2.0
	MaxDrift   = 100.0
	DriftSpeed = 10.0 // TODO:
)

// ControlSystem handles camera movement. We can change this to operate based
// on input or result of player movement, etc. It belongs to the physics phase.
type ControlSystem struct {
	ecs.GameSystem
	TCam reflect.Type
	TIpt reflect.Type
	TPos reflect.Type
}

// NewControlSystem creates a new control system for the camera
// TODO: do we need window bounds? or any sort of clamping when we create "world" boundaries?
func NewControlSystem() *ControlSystem {
	sys := ControlSystem{
		TCam: reflect.TypeOf(&Component{}),
		TIpt: reflect.TypeOf(&input.Component{}),
		TPos: reflect.TypeOf(&movement.PositionComponent{}),
	}
	sys.SetConstraints(sys.TCam, sys.TIpt, sys.TPos)
	return &sys
}

// Update updates the camera control system
func (sys *ControlSystem) Update(scene *ecs.Scene, ents []ecs.Entity, gin ecs.GameInfo) {
	// NOTE: we expect one and exactly one entity
	ent := ents[0]
	cam := ent.Component(sys.TCam).(*Component)
	ipt := ent.Component(sys.TIpt).(*input.Component)
	pos := ent.Component(sys.TPos).(*movement.PositionComponent)

	// TODO: smooth out zoom and drift up to max, smooth down when released

	// follow the player
	// TODO: don't need to keep in center, but could move along when a certain
	// distance is exceeded

	// update the camera center: allow a slight offset: we can smoo
	drift := ipt.State.Camera
	if !drift.IsZero() {
		drift = drift.Normalize().Scale(MaxDrift)
	}
	cam.Center = pos.Value.Add(drift)
	// update the camera zoom
	if ipt.State.Zoom {
		cam.Zoom = MaxZoom
	} else {
		cam.Zoom = 1.0
	}
}

// Close closes the system and releases resources
func (sys *ControlSystem) Close(ents []ecs.Entity) {

}

// JSONControlSystemFactory returns a factory function for creating the system
func JSONControlSystemFactory() ecs.JSONSystemFactory {
	return func() (ecs.System, error) {
		return NewControlSystem(), nil
	}
}
