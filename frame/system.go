package frame

import (
	"log"
	"reflect"

	ani "gitlab.com/premium-boss/sonar/animation"
	"gitlab.com/premium-boss/sonar/ecs"
	mov "gitlab.com/premium-boss/sonar/movement"
	phy "gitlab.com/premium-boss/sonar/physics"
	ren "gitlab.com/premium-boss/sonar/render"
)

// TODO: the scene is an entity! Add some magic here, but this will give us
// a: extensibility: can add any components we want to it, even ones that don't yet exist
// b: compatibility: will work with all existing components and systems
// c: sharing: all systems can access through scene, if we don't want to pass it in!

// separate Sprite, Texture from anything
// make a dedicated GL graphics system
// none of this crazy manager stuff, maybe one for Texture mapping, that's it
// this should make sprites and lerp
//   - possibly batch, maybe not!
// let the other system draw the batches

// System creates a final frame state prior to rendering.
// Part one is interpolation between frames for fixed timestep with
// an accumulator. If we render the current frame as-is, then our physics time
// would not line up with our render time. We must account for the accumulator.
// Rendering produces time and physics simulation consumes it in steps of dt.
type System struct {
	ecs.GameSystem
	TMov reflect.Type
	TPos reflect.Type
	TRen reflect.Type // TODO: do we want a frame component? ehh...
	TAni reflect.Type
	TFrm reflect.Type
}

// NewSystem creates a new Render system
func NewSystem() *System {
	sys := System{
		TMov: reflect.TypeOf(&mov.Component{}),
		TRen: reflect.TypeOf(&ren.Component{}),
		TPos: reflect.TypeOf(&mov.PositionComponent{}),
		TAni: reflect.TypeOf(&ani.Component{}),
		TFrm: reflect.TypeOf(&Component{}),
	}
	sys.SetConstraints(sys.TRen)
	return &sys
}

// Update updates the system. This cares about dynamic entities.
func (sys *System) Update(scene *ecs.Scene, ents []ecs.Entity, gin ecs.GameInfo) {
	alpha := gin.Acc / gin.DT
	fc := scene.Entity.Component(sys.TFrm)
	if fc == nil {
		log.Fatal("frame:System: no frame.Component on Scene")
	}
	frame := fc.(*Component)
	// clear without leaking, but prevent reallocation unnecessarily?
	// TODO: need to make sure this is safe - believe it should be if we go struct
	// TODO: CHECKLEAK
	// TODO: we could clear at end of glrender instead...
	frame.Sprites = frame.Sprites[:0]

	for _, ent := range ents {
		rc := ent.Component(sys.TRen).(*ren.Component)
		pos, hasPos := ent.Component(sys.TPos).(*mov.PositionComponent) // NOTE: may be nil
		mc, hasMov := ent.Component(sys.TMov).(*mov.Component)          // NOTE: may be nil
		ac, hasAni := ent.Component(sys.TAni).(*ani.Component)          // NOTE: may be nil

		if !rc.IsActive {
			// TODO: also occlude from rendering if not visible? for this
			// we need the current bounds from the camera display system
			// ^^^ should probably be part of context, along with other managers
			// camera itself really shouldn't be a component on just one entity
			// unless we want a "scene" entity
			// But, now that we have access to the scene,
			// let's just put it there!
			// TODO: do we want to flag anything as not in scene vs in scene on the render comp?
			continue
		}

		target := ent.Component(rc.Target).(ren.Renderable)

		if !hasMov {
			// no movement to calculate, position is fine - we assume sprite pos has been set
			// still need to add to frame though!!
			if sprite, ok := target.Sprite(); ok {
				frame.Sprites = append(frame.Sprites, sprite)
			} else if sprites, ok := target.Sprites(); ok {
				frame.Sprites = append(frame.Sprites, sprites...)
			} else {
				log.Fatalln("frame:System: Renderable with no Sprites")
			}
			continue
		}

		if !hasPos {
			log.Fatalln("frame:System: nil position component with movement")
		}
		fp := framePosition(mc, pos, alpha)

		// update a single sprite
		// TODO: the pointer was easier...
		if sprite, ok := target.Sprite(); ok {
			sprite := updateSprite(sprite, fp, hasAni, ac)
			target.UpdateSprite(sprite)
			frame.Sprites = append(frame.Sprites, sprite)
			continue
		}

		// update a sprite group
		sprites, ok := target.Sprites() // NOTE: we expect this to update in place
		if !ok {
			log.Fatalln("frame:System: Renderable with no Sprites")
		}
		for i, sprite := range sprites {
			sprite := updateSprite(sprite, fp, hasAni, ac)
			target.UpdateAt(i, sprite)
			frame.Sprites = append(frame.Sprites, sprite)
		}

		// if we have a movement component, assume we need to lerp and update
		// renderable pos. We may need to broaden this for effects, or for
		// entities with animation but no movement
		// NOTE: assumes pos is not nil
		// TODO: should we finialize some comp here, just get that??
		// This should really be culling and lerping, not sure why we need
		// the animation here...
		// TODO: should our output be something on the scene, rather than per entity? Otherwise, still must
		// loop and check whether in-scene or not...
		// TODO: check performance here...
	}
}

// Close closes the system and releases resources
func (sys *System) Close(ents []ecs.Entity) {
}

// JSONSystemFactory returns a factory function for creating the system
func JSONSystemFactory() ecs.JSONSystemFactory {
	return func() (ecs.System, error) {
		return NewSystem(), nil
	}
}

// framePosition calculates final frame position.
// For moving entities, we want to lerp between frames for accumulator.
// For static entities, we just take the position
// TODO: wasteful for static...
func framePosition(mc *mov.Component, pos *mov.PositionComponent, alpha float32) phy.Vec2 {
	if mc != nil && mc.IsDynamic {
		return mc.PrevPos.Lerp(pos.Value, alpha)
	}
	return pos.Value
}

func updateSprite(sprite ren.Sprite, framePos phy.Vec2, hasAni bool, ac *ani.Component) ren.Sprite {
	if hasAni {
		return updateAnimated(sprite, framePos, ac)
	}
	return updateStatic(sprite, framePos)
}

// updateStatic updates non-animated renderables
func updateStatic(sprite ren.Sprite, framePos phy.Vec2) ren.Sprite {
	return sprite.Moved(framePos)
}

// updateAnimated updates animated renderables.
// We need to adjust the texture offsets for the Sprite(s)
func updateAnimated(sprite ren.Sprite, framePos phy.Vec2, ac *ani.Component) ren.Sprite {
	// update the sprites for texture (ani) and position
	animator := ac.Animators[ac.State.Current]
	tx := animator.GetFrame()
	// TODO: should update width and height based on frame? otherwise they don't matter...
	// TODO: right now, tx is returing a sprite index!!! pay attention if you change
	// TODO: should this be part of animation system?
	sprite = sprite.TexIndex(tx)
	if animator.Flip && !sprite.IsFlipped() {
		sprite = sprite.FlippedHorizontally(true)
	} else if !animator.Flip && sprite.IsFlipped() {
		sprite = sprite.FlippedHorizontally(false)
	}

	sprite = sprite.Moved(framePos)

	// if we flip, must offset by width
	// TODO: pass flags to shader instead?
	if animator.Flip {
		sprite = sprite.MovedOffset(phy.V(sprite.Width, 0.0))
	}
	return sprite
}
