package frame

import (
	"encoding/json"
	"gitlab.com/premium-boss/sonar/ecs"
	ren "gitlab.com/premium-boss/sonar/render"
)

// TODO: read that tutorial, what did that guy include in his FrameState?

type Component struct {
	Sprites []ren.Sprite
}

// Init initializes with Entity
func (c *Component) Init(Entity ecs.Entity) {
	c.Sprites = make([]ren.Sprite, 0)
}

// OnUnregister hook for an Entity having this component removed
func (c *Component) OnUnregister(ent ecs.Entity) {
}

// JSONComponentFactory returns factory function for loading components from JSON
func JSONComponentFactory() ecs.JSONComponentFactory {
	return func(ent ecs.Entity, desc ecs.JSONComponentDescriptor) (ecs.Component, error) {
		c := Component{}
		// NOTE: we don't have anything to unmarshal really...
		err := json.Unmarshal(desc.Data, &c)
		if err != nil {
			return nil, err
		}
		c.Init(ent)
		return &c, nil
	}
}
