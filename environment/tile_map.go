package environment

import (
	"encoding/json"
	"errors"
	"fmt"

	"gitlab.com/premium-boss/sonar/ecs"
	"gitlab.com/premium-boss/sonar/physics"
	"gitlab.com/premium-boss/sonar/render"
	ren "gitlab.com/premium-boss/sonar/render"
)

// TileDescriptor denotes the dimensions of a specific tile and where to find it
// in the Texture. Dimensions are relative to the TileMap's TileSize
type TileDescriptor struct {
	Dimensions physics.Vec2 `json:"dimensions"`
	TexOffset  physics.Vec2 `json:"texOffset"`
}

// TileArea groups rows and cols of tiles, used for one initial window view or
// however desired. More human-readable than a big blob.
type TileArea struct {
	Start physics.Vec2
	Tiles [][]int `json:"tiles"`
}

// TileMap describes a layout of tile selections from a texture. The number in
// the 2D slice represents an index into the slice of descriptors. A -1 means
// there is no tile drawn here, possibly do to the size of an adjacent tile.
type TileMap struct {
	Name        string           `json:"name"`
	TexName     string           `json:"texName"`
	TileSize    physics.Vec2     `json:"tileSize"` // size of a standard 1x1 tile
	Descriptors []TileDescriptor `json:"descriptors"`
	Areas       []TileArea       `json:"areas"`
}

// TileMapComponent contains the renderable sprites for 2D backgrounds
type TileMapComponent struct {
	Name    string // use to retrieve tileMap
	TileMap TileMap
	sprites []ren.Sprite
}

// Len gets length of TileArea, assuming it's a rectangle
func (ta *TileArea) Len() int {
	rows := len(ta.Tiles)
	if rows == 0 {
		return 0
	}
	return rows * len(ta.Tiles[0])
}

// Len gets length of TileMap
func (tm *TileMap) Len() int {
	ln := len(tm.Areas)
	if ln > 0 {
		ln *= tm.Areas[0].Len()
	}
	return ln
}

// NewTileMapComponent creates a new renderable component from a TileMap.
// NOTE: all areas are assumed to be the same size
// NOTE: relying on New makes it tougher to load? could do init!
func NewTileMapComponent(tm TileMap) *TileMapComponent {
	return &TileMapComponent{
		TileMap: tm,
		sprites: make([]ren.Sprite, 0, tm.Len()),
	}
}

// Init inits component with Entity
func (c *TileMapComponent) Init(ent ecs.Entity) {

}

// OnUnregister hook
func (c *TileMapComponent) OnUnregister(ent ecs.Entity) {}

// Load loads sprites: TODO: org better? shouldn't really be in component but...
// we may wish to move this...
// TODO: simplify this: index and offset combo is weird, pick one
// this means we need to be able to create from index w/ size different than
// width and height in sprites
// TODO: allow non-rectangular areas
// TODO: move this out of component if possible? maybe into the factory func?
func (c *TileMapComponent) load(tm TileMap, tex render.Texture) error {
	// create the sprites
	c.TileMap = tm
	c.sprites = make([]ren.Sprite, 0, tm.Len())
	ctDesc := len(c.TileMap.Descriptors)
	for _, area := range c.TileMap.Areas {
		for i := range area.Tiles { // loop rows
			for j := range area.Tiles[i] { // loop cols
				// do we want to draw this tile?
				idx := area.Tiles[i][j]
				if idx < 0 {
					continue
				}
				if idx >= ctDesc {
					return errors.New("TileMap: index greater then bounds of TileDescriptors")
				}
				pos := area.Start.Add(physics.V(float32(j)*c.TileMap.TileSize.X, float32(i)*c.TileMap.TileSize.Y))
				//fmt.Println("i:", i, "j:", j, "pos:", pos)
				spr := ren.NewSpriteOffset(c.TileMap.Descriptors[idx].Dimensions,
					c.TileMap.Descriptors[idx].TexOffset, tex).Moved(pos)
				c.sprites = append(c.sprites, spr)
			}
		}
	}
	return nil
}

// Sprite stub for renderable interface
func (c *TileMapComponent) Sprite() (ren.Sprite, bool) {
	return ren.Sprite{}, false
}

// Sprites returns sprites for renderable interface
func (c *TileMapComponent) Sprites() ([]ren.Sprite, bool) {
	return c.sprites, true
}

// UpdateSprite stub for Renderable interface
func (c *TileMapComponent) UpdateSprite(sprite ren.Sprite) {
}

// UpdateAt updates a Sprite at index i
func (c *TileMapComponent) UpdateAt(i int, sprite ren.Sprite) {
	c.sprites[i] = sprite
}

// JSONTileMapComponentFactory returns a factory function for loading TileMapComponents from JSON
func JSONTileMapComponentFactory(texMgr render.TextureManager, tileMaps map[string]TileMap,
) ecs.JSONComponentFactory {
	return func(ent ecs.Entity, desc ecs.JSONComponentDescriptor) (ecs.Component, error) {
		tmc := TileMapComponent{}
		err := json.Unmarshal(desc.Data, &tmc)
		if err != nil {
			return nil, err
		}
		tm := tileMaps[tmc.Name]
		tex, ok := texMgr.GetTexture(tm.TexName)
		if !ok {
			return nil, fmt.Errorf("TileMap: bad texture on tile map")
		}
		err = tmc.load(tm, tex)
		if err != nil {
			return nil, err
		}
		tmc.Init(ent)
		return &tmc, nil
	}
}
