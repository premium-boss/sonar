package environment

import (
	"gitlab.com/premium-boss/sonar/ecs"
	ren "gitlab.com/premium-boss/sonar/render"
)

// RegisterRenderables registers renderables from the environment package with the render registry
func RegisterRenderables(reg ren.RenderableRegistry) {
	reg["environment.TileMapComponent"] = &TileMapComponent{}
}

// RegisterComponents registers all loader factories in pacakge
func RegisterComponents(reg map[string]ecs.JSONComponentFactory, texMgr ren.TextureManager,
	tileMaps map[string]TileMap,
) {
	reg["environment.TileMapComponent"] = JSONTileMapComponentFactory(texMgr, tileMaps)
}
