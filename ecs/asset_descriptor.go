package ecs

// AssetDescriptor defines name and path for loading assets
type AssetDescriptor struct {
	Name     string
	FileName string
}
