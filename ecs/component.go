package ecs

import (
	"encoding/json"
	"reflect"
	"sort"
)

// ComponentRegistry is a global tracker of Entites per component
type ComponentRegistry map[reflect.Type][]Entity

// JSONComponentDescriptor indicates the type of Component and data for which to use
// in deserialization. You do not have to use JSON, but this is provided as a default.
type JSONComponentDescriptor struct {
	Type string
	Data json.RawMessage
}

// JSONComponentFactory function for loading components from JSON.
type JSONComponentFactory func(Entity, JSONComponentDescriptor) (Component, error)

// JSONSystemFactory function for loading systems from JSON
// TODO: add descriptor if we need more configurable args
type JSONSystemFactory func() (System, error)

// Component represents any aspect we can maintain for an Entity.
// TODO: do we need these hooks?
type Component interface {
	Init(ent Entity)
	OnUnregister(ent Entity)
}

// NewComponentRegistry creates a new ComponentRegistry
func NewComponentRegistry() ComponentRegistry {
	return ComponentRegistry(make(map[reflect.Type][]Entity))
}

// Register registers an Entity as having a Component. NOTE: does NOT check
// for unique. We could consider a map...
func (cr ComponentRegistry) Register(ent Entity, comp Component) {
	t := reflect.TypeOf(comp)
	ents := append(cr[t], ent)
	sort.Sort(ByID(ents)) // NOTE: this could be made more efficient as needed: we could let the end user sort when complete..., or else fetch map then sort...
	cr[t] = ents
}

// Unregister unregisters an Entity for a Component
// TODO: use a map instead?
func (cr ComponentRegistry) Unregister(ent Entity, comp Component) {
	t := reflect.TypeOf(comp)
	ents := cr[t]
	for i := range ents {
		if ents[i].ID == ent.ID {
			// remove the entity
			copy(ents[i:], ents[i+1:])
			last := len(ents) - 1
			//ents[last] = nil // TODO: needed if we switch back to pointer
			ents = ents[:last]
			comp.OnUnregister(ent)
			break
		}
	}
	cr[t] = ents
}
