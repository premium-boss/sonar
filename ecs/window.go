package ecs

// TODO: is this agnostic enough? kinda tight w/ input
// could also make a window package...

// OnKey callback
type OnKey func(key, action, mod int)

// OnMouseButton callback
type OnMouseButton func(btn, action, mod int)

// OnMouseMove callback
type OnMouseMove func(x, y float32)

// Window is a window interface
type Window interface {
	Update() // TODO: move input system update to here, swap buffers, etc...
	SetOnKey(handler OnKey)
	SetOnMouseButton(handler OnMouseButton)
	SetOnMouseMove(handler OnMouseMove)
	ShouldClose() bool
	SetShouldClose()
	Destroy()
}
