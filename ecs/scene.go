package ecs

import (
	"time"
)

// Phase index constants
const (
	PhasePhysics Phase = iota
	PhaseRender
)

// Transition constants. A scene's Physics update phase may return one of these to change or end the scene.
const (
	TransitionClose TransitionType = iota
	TransitionNext
)

// Phase indexer
type Phase int

// TransitionType enum
type TransitionType int

// Scene represents a game scene with Entities
// TODO: register systems to phases, but must be ordered
// TODO: we should have an EntityManager that handles registering, not Scene directly.
type Scene struct {
	// TODO push entities and comp reg to EntityManager
	EntityManager *EntityManager
	Systems       [][]System
	ShouldClose   bool            // TODO: could have types that let us keep stored, etc.
	Transition    SceneTransition // TODO: could have more of a real TransitionManager that systems can manipulate
	Entity        Entity          // entity for camera, input, framestate, anything like that
	// TODO: consider making the ShouldClose, Transition, etc. components? can tack on the Scene that way...
	// Still need something external to access, so maybe not worth it...
}

// SceneTransition info
type SceneTransition struct {
	Type TransitionType
	Name string
}

// JSONSceneDescriptor2D for loading a scene from data. You do not have to use JSON, but this is provided as a default.
type JSONSceneDescriptor2D struct {
	Name     string
	Systems  []SystemDescriptor
	TileMap  AssetDescriptor
	Fonts    []AssetDescriptor
	Sounds   []AssetDescriptor
	Textures []AssetDescriptor
	Entity   JSONEntityDescriptor // TODO: load this up!!! Do we want it to go through Entity Mgr?
	Entities []JSONEntityDescriptor
	TileSets []AssetDescriptor
}

// NewScene creates a new Scene
func NewScene(mgr *EntityManager) *Scene {
	sce := Scene{
		EntityManager: mgr,
		Systems:       make([][]System, 2),
	}
	return &sce
}

// RegisterSystem adds a system to a phase.
// Systems will update in the order in which they are registered.
func (s *Scene) RegisterSystem(phase Phase, sys System) {
	s.Systems[phase] = append(s.Systems[phase], sys)
}

// Run runs the Scene
func (s *Scene) Run(win Window, dt float32) {
	gin := GameInfo{DT: dt, FPS: 1.0 / dt} // TODO:
	tick := time.NewTicker(time.Second / time.Duration(gin.FPS))
	last := time.Now()
	var frametime float32
	defer tick.Stop()

GAMELOOP:
	// TODO: consider registering phases... but for now, acc setup easier w/out
	// TODO: do we want this in game run instead of scene run? inner scene run?
	for {
		<-tick.C
		if win.ShouldClose() || s.ShouldClose {
			break GAMELOOP
		}
		// update game clock
		frametime, last = updateTime(last)
		gin.Acc += frametime
		// physics fixed steps
		for gin.Acc >= gin.DT {
			s.Update(PhasePhysics, gin)
			// TODO: consider bailing out here if s.ShouldClose
			gin.Acc -= gin.DT
			gin.Tot += gin.DT
			gin.Tick++
		}
		s.Update(PhaseRender, gin) // render
		win.Update()               // window maintenance
	}
}

// Update updates a Scene through its systems
// NOTE: system order is important!
func (s *Scene) Update(phase Phase, gin GameInfo) {
	for _, sys := range s.Systems[phase] {
		ents := s.getEntities(sys)
		sys.Update(s, ents, gin)
	}
}

// Close closes a scene, freeing resources
// TODO: may need more flexibility here, esp. for changing scenes...
func (s *Scene) Close() {
	// close all systems
	phases := [2]Phase{PhasePhysics, PhaseRender}
	for _, phase := range phases {
		for _, sys := range s.Systems[phase] {
			ents := s.getEntities(sys)
			sys.Close(ents)
		}
	}

}

// TODO: can we do this more efficiently?? this slice is potentially a costly allocation
func (s *Scene) getEntities(sys System) []Entity {
	constraints := sys.Constraints() // TODO: overhaul with new constraints? but type makes checking faster...
	var ents []Entity
	n := len(constraints)
	if n == 0 {
		ents = s.EntityManager.Entities // get all entities // TODO: watch out here!
	} else {
		// get all entities with first constraint
		// NOTE: systems may rely on entities ordered by ID, we rely on registry
		met := s.EntityManager.Components[constraints[0]]
		if n == 1 {
			ents = met
		} else {
		MET:
			// if there are more contraints, ensure them
			for _, ent := range met {
				for _, c := range constraints[1:] {
					if ent.Component(c) == nil {
						continue MET // missing component
					}
				}
				ents = append(ents, ent)
			}
		}
	}
	return ents
}

func updateTime(last time.Time) (float32, time.Time) {
	now := time.Now()
	frametime := float32(now.Sub(last).Seconds())
	if frametime > 0.25 {
		frametime = 0.25 // cap it: equivalent to dropping frames?
	}
	return frametime, now
}
