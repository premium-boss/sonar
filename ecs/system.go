package ecs

import "reflect"

// System manages a specific behavior for any Entities that meet component
// type constraints
type System interface {
	Constraints() []reflect.Type
	Update(scene *Scene, ents []Entity, gin GameInfo)
	Close(ents []Entity)
}

// SystemDescriptor for dynamically loading systems per scene
type SystemDescriptor struct {
	Phase   int
	Systems []string
}

// GameSystem is a shared base System to deal w/ constraints wire-up
type GameSystem struct {
	constraints []reflect.Type
}

// Constraints returns the constraints for the system
func (gs *GameSystem) Constraints() []reflect.Type {
	return gs.constraints
}

// SetConstraints sets the constraints of the GameSystem
func (gs *GameSystem) SetConstraints(ts ...reflect.Type) {
	gs.constraints = ts
}
