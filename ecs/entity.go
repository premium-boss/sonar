package ecs

import (
	"reflect"
)

// Entity is a basic game object
type Entity struct {
	ID         uint32
	Name       string
	components map[reflect.Type]Component // TODO: if this hashing is bad, go to slice but cache type to only reflect once...
}

// ByID is a sortable collection
type ByID []Entity

// JSONEntityDescriptor for serialized entities, basically a list of components.
// You do not have to use JSON, but this is provided as a default.
// The HasExternalData flag indicates we want to merge some other data in, e.g. from
// a TileMap object layer
type JSONEntityDescriptor struct {
	Name            string
	HasExternalData bool
	Components      []JSONComponentDescriptor
}

// EntityManager controls entity creation and component registration
type EntityManager struct {
	NextID     uint32
	Entities   []Entity // TODO: pointer or value?
	Components ComponentRegistry
}

// NewEntityManager creates a new EntityManager
func NewEntityManager() *EntityManager {
	mgr := EntityManager{
		Entities:   make([]Entity, 0),
		Components: NewComponentRegistry(),
	}
	return &mgr
}

// NewEntity creates a new entity
func (mgr *EntityManager) NewEntity(name string) Entity {
	ent := Entity{
		ID:         mgr.NextID,
		Name:       name,
		components: make(map[reflect.Type]Component),
	}
	mgr.NextID++
	mgr.Entities = append(mgr.Entities, ent)
	return ent
}

// AddComponent adds a Component to the Entity
func (mgr *EntityManager) AddComponent(ent Entity, c Component) {
	ent.components[reflect.TypeOf(c)] = c // TODO: cache on Component and add to interface if slow...
	mgr.Components.Register(ent, c)
}

// RemoveComponent removes a Component from the Entity
func (mgr *EntityManager) RemoveComponent(ent Entity, c Component) {
	delete(ent.components, reflect.TypeOf(c))
	mgr.Components.Unregister(ent, c)
}

// Component gets a Component with a type
func (ge Entity) Component(t reflect.Type) Component {
	return ge.components[t]
}

// Len returns length of the collection
func (b ByID) Len() int {
	return len(b)
}

// Swap swaps two elements in place
func (b ByID) Swap(i, j int) {
	b[i], b[j] = b[j], b[i]
}

// Less compares IDs
func (b ByID) Less(i, j int) bool {
	return b[i].ID < b[j].ID
}
