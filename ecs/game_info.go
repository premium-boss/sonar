package ecs

// GameInfo contains any detail needed in the update loop of a system. It covers
// frame and time info for fixed-timestep physics with an accumulator.
// TODO: FPS or DT?
type GameInfo struct {
	DT   float32
	FPS  float32
	Tick uint64
	Tot  float32
	Acc  float32
}
