package render

import (
	"image"
)

// Texture holds information about a texture and its bounds. The renderer should
// know how to handle the ID
type Texture struct {
	ID     uint32 // GL id? may want an interface?
	Name   string
	Width  float32
	Height float32
}

// TextureManager manages creation and maintenance of textures for platform
// specific graphics
type TextureManager interface {
	GetTexture(name string) (Texture, bool)
	NewTextureFromFile(file, name string) (Texture, error)
	NewTextureFromRGBA(rgba *image.RGBA, name string) (Texture, error)
}
