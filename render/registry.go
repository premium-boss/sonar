package render

// RenderableRegistry maps names to renderables for deserialization
type RenderableRegistry map[string]Renderable

// NewRenderableRegistry provides a default registry of renderables
func NewRenderableRegistry() RenderableRegistry {
	reg := RenderableRegistry{
		"render.SpriteComponent": &SpriteComponent{},
	}
	return reg
}
