package render

import (
	"log"

	"github.com/coconaut/glfw/v3.2/glfw"
	"gitlab.com/premium-boss/sonar/ecs"
)

// TODO: consider moving to glrender?

// GLWindow implements Window for OpenGL
type GLWindow struct {
	IsVSync bool
	Title   string
	Width   int
	Height  int

	Window        *glfw.Window
	onKey         ecs.OnKey
	onMouseButton ecs.OnMouseButton
	onMouseMove   ecs.OnMouseMove
}

// GLWindowOption for initializing the GLWindow
type GLWindowOption func(w *GLWindow)

// Title option for setting title
func Title(title string) GLWindowOption {
	return func(w *GLWindow) {
		w.Title = title
	}
}

// Dimensions option for setting dimensions
func Dimensions(width, height int) GLWindowOption {
	return func(w *GLWindow) {
		w.Width = width
		w.Height = height
	}
}

// VSync option enables VSync on GLWindow
func VSync() GLWindowOption {
	return func(w *GLWindow) {
		if w.Window != nil {
			glfw.SwapInterval(1)
		}
	}
}

// OnKey sets up key callback
func OnKey(handler ecs.OnKey) GLWindowOption {
	return func(w *GLWindow) {
		if w.Window != nil {
			w.SetOnKey(handler)
		}
	}
}

// OnMouseButton sets up mouse button callbacks
func OnMouseButton(handler ecs.OnMouseButton) GLWindowOption {
	return func(w *GLWindow) {
		if w.Window != nil {
			w.SetOnMouseButton(handler)
		}
	}
}

// OnMouseMove sets mouse move callback
func OnMouseMove(handler ecs.OnMouseMove) GLWindowOption {
	return func(w *GLWindow) {
		if w.Window != nil {
			w.SetOnMouseMove(handler)
		}
	}
}

// NewGLWindow creates a new GLWindow
func NewGLWindow(opts ...GLWindowOption) *GLWindow {
	if err := glfw.Init(); err != nil {
		log.Fatalln("Window: failed to initialize glfw:", err)
	}
	// defaults
	w := GLWindow{
		Title:  "New Window",
		Width:  960,
		Height: 640,
	}
	// options
	// TODO: this is a little hacky, but some are needed pre-creation, and some post.
	// We apply twice...
	for _, opt := range opts {
		opt(&w)
	}

	glfw.WindowHint(glfw.Resizable, glfw.False)
	glfw.WindowHint(glfw.ContextVersionMajor, 4)
	glfw.WindowHint(glfw.ContextVersionMinor, 1)
	glfw.WindowHint(glfw.OpenGLProfile, glfw.OpenGLCoreProfile)
	glfw.WindowHint(glfw.OpenGLForwardCompatible, glfw.True)
	win, err := glfw.CreateWindow(w.Width, w.Height, w.Title, nil, nil)

	if err != nil {
		log.Fatalln("Window: failed to create window:", err)
	}
	win.MakeContextCurrent() // MUST DO
	w.Window = win

	// options that apply to window
	for _, opt := range opts {
		opt(&w)
	}

	// TODO: make more options
	win.SetInputMode(glfw.StickyKeysMode, glfw.False)
	win.SetInputMode(glfw.StickyMouseButtonsMode, glfw.False)

	return &w
}

// Update updates the window, swaps buffers, polls input, etc.
func (win *GLWindow) Update() {
	win.Window.SwapBuffers()
	glfw.PollEvents()
}

// Destroy cleans up window resources
func (win *GLWindow) Destroy() {
	win.Window.Destroy()
	glfw.Terminate()
}

// ShouldClose returns whether window should close
func (win *GLWindow) ShouldClose() bool {
	return win.Window.ShouldClose()
}

// SetShouldClose overrides should close flag on window
func (win *GLWindow) SetShouldClose() {
	win.Window.SetShouldClose(true)
}

// SetOnKey sets key press handler
func (win *GLWindow) SetOnKey(handler ecs.OnKey) {
	win.onKey = handler
	win.Window.SetKeyCallback(win.onKeyEvent) // TODO any way to cancel?
}

// SetOnMouseButton sets mouse button handler
func (win *GLWindow) SetOnMouseButton(handler ecs.OnMouseButton) {
	win.onMouseButton = handler
	win.Window.SetMouseButtonCallback(win.onMouseButtonEvent)
}

// SetOnMouseMove sets mouse move handler
func (win *GLWindow) SetOnMouseMove(handler ecs.OnMouseMove) {
	win.onMouseMove = handler
	win.Window.SetCursorPosCallback(win.onMouseMoveEvent)
}

// onKeyEvent is an internal relay to handle key press
// events. Action is be press, release, or repeat.
func (win *GLWindow) onKeyEvent(gw *glfw.Window, key glfw.Key, scancode int,
	action glfw.Action, mod glfw.ModifierKey) {
	win.onKey(int(key), int(action), int(mod))
}

// onMouseButtonEvent hanldes mouse button events. Action is press or release.
func (win *GLWindow) onMouseButtonEvent(gw *glfw.Window, btn glfw.MouseButton,
	action glfw.Action, mod glfw.ModifierKey) {
	win.onMouseButton(int(btn), int(action), int(mod))
}

// onMouseMoveEvent tracks mouse movement
func (win *GLWindow) onMouseMoveEvent(gw *glfw.Window, x, y float64) {
	win.onMouseMove(float32(x), float32(y))
}
