package render

import (
	"encoding/json"
	"fmt"
	"reflect"

	"gitlab.com/premium-boss/sonar/ecs"
)

// Renderable is an interface for renderable targets of a render Component. It
// should provide a Sprite or a slice or Sprites to render
type Renderable interface {
	Sprite() (Sprite, bool)
	UpdateSprite(Sprite)
	Sprites() ([]Sprite, bool) // NOTE: can update in place...? Is that a dangerous assumption?
	UpdateAt(int, Sprite)
}

// Component is a render component. It may have one or more renderable targets
// within
type Component struct {
	IsActive    bool
	IsDrawReady bool         // tracks whether it has been initialized / added to batch
	Target      reflect.Type // type of component to render, must be Renderable
	TargetName  string       // name of target type, used for first retrieval
}

// NewComponent creates a new render Component with a Renderable
func NewComponent(target reflect.Type) *Component {
	return &Component{
		Target:   target,
		IsActive: true,
	}
}

// Init inits component with Entity
func (c *Component) Init(ent ecs.Entity) {
}

// OnUnregister hook
func (c *Component) OnUnregister(ent ecs.Entity) {}

// JSONComponentFactory creates a factory function for loading JSON components
func JSONComponentFactory(reg RenderableRegistry) ecs.JSONComponentFactory {
	return func(ent ecs.Entity, desc ecs.JSONComponentDescriptor) (ecs.Component, error) {
		rc := Component{}
		err := json.Unmarshal(desc.Data, &rc)
		if err != nil {
			return nil, err
		}
		// get the Target comp!
		tc := reg[rc.TargetName]
		if tc == nil {
			return nil, fmt.Errorf("renderLoader: failed to get type for renderable: %s, ent: %d", rc.TargetName, ent.ID)
		}
		rc.Target = reflect.TypeOf(tc)
		rc.Init(ent)
		return &rc, nil
	}
}
