package render

import (
	"encoding/json"
	"fmt"
	"image/color"
	"reflect"

	"gitlab.com/premium-boss/sonar/ecs"
	mv "gitlab.com/premium-boss/sonar/movement"
	phy "gitlab.com/premium-boss/sonar/physics"
)

// Tex offset constants for initialization and/or static Sprites
const (
	OffsetNone TexOffsetType = iota
	OffsetIndex
	OffsetAbsolute
)

// TexOffsetType describes how to use coords to navigate Texture
type TexOffsetType int

// SpriteComponent is a sprite component...
type SpriteComponent struct {
	Instance Sprite
}

// TexturePosition for drawing the Sprite
type TexturePosition struct {
	OffsetType  TexOffsetType
	Coordinates phy.Vec2
}

// Sprite is a game sprite with texture and size
type Sprite struct {
	Color           color.RGBA
	Width           float32
	Height          float32
	InstanceTex     []float32 // x, y, width, height rel to texture // TODO: 4? get rid of??
	Texture         Texture
	TexturePosition TexturePosition

	isDrawInitialized bool
	isFlipped         bool // TODO: move to animation component? // TODO: remove

	// TODO: do we want here, or calc on the fly?
	// Can't be on render because we may have slice of sprites
	ScaleMatrix     phy.Mat4
	TranslateMatrix phy.Mat4
	ModelMatrix     phy.Mat4
}

var tPos = reflect.TypeOf(&mv.PositionComponent{})
var tSpace = reflect.TypeOf(&mv.SpaceComponent{})

// NewSpriteComponent creates a component with the Sprite
func NewSpriteComponent(sprite Sprite) *SpriteComponent {
	return &SpriteComponent{Instance: sprite}
}

// Init inits the SpriteComponent
func (sc *SpriteComponent) Init(ent ecs.Entity) {
	// get width and height from space component...
	space, ok := ent.Component(tSpace).(*mv.SpaceComponent)
	if ok {
		sc.Instance.Width = space.Dimensions.X
		sc.Instance.Height = space.Dimensions.Y
	}
	sc.Instance.InstanceTex = make([]float32, 4, 4)
	sc.Instance = sc.Instance.initModel() // TODO: make this more apparent
	pos, ok := ent.Component(tPos).(*mv.PositionComponent)
	if ok {
		sc.Instance = sc.Instance.Moved(pos.Value)
	}
	switch sc.Instance.TexturePosition.OffsetType {
	case OffsetIndex:
		sc.Instance = sc.Instance.TexIndex(sc.Instance.TexturePosition.Coordinates)
	case OffsetAbsolute:
		sc.Instance = sc.Instance.TexOffset(sc.Instance.TexturePosition.Coordinates)
	}
}

// OnUnregister hook
func (sc *SpriteComponent) OnUnregister(ent ecs.Entity) {}

// Sprite returns Sprite for renderable interface
func (sc *SpriteComponent) Sprite() (Sprite, bool) {
	return sc.Instance, true
}

// Sprites stub for Renderable interface
func (sc *SpriteComponent) Sprites() ([]Sprite, bool) {
	return nil, false
}

// UpdateSprite updates the Sprite of the component
func (sc *SpriteComponent) UpdateSprite(sprite Sprite) {
	sc.Instance = sprite
}

// UpdateAt stub for Renderalbe interface
func (sc *SpriteComponent) UpdateAt(i int, sprite Sprite) {

}

// NewSpriteOffset creates a new Sprite from a texture and size. The offsets are
// used to setup the texture position.
func NewSpriteOffset(dim, offset phy.Vec2, tex Texture) Sprite {
	s := newSprite(dim, tex).TexOffset(offset)
	return s
}

// NewSpriteIndexed creates a new Sprite from a texture and size. The row and
// col are used to intialize the texture offset
func NewSpriteIndexed(dim, idx phy.Vec2, tex Texture) Sprite {
	s := newSprite(dim, tex).TexIndex(idx)
	return s
}

func newSprite(dim phy.Vec2, tex Texture) Sprite {
	s := Sprite{
		Width:   dim.X,
		Height:  dim.Y,
		Texture: tex,
		Color:   color.RGBA{255, 255, 255, 255},
	}
	s.InstanceTex = make([]float32, 4, 4)
	s = s.initModel() // TODO:!!!
	return s
}

// MoveTo moves the Spirtes matrix to a new position
func (s Sprite) Moved(pos phy.Vec2) Sprite {
	s.TranslateMatrix = phy.Translate4(pos.X, pos.Y, 0.0)
	s.ModelMatrix = s.TranslateMatrix.Mul(s.ScaleMatrix)
	return s
}

// MoveOffset moves the Sprite by an amount in the x and y directions
func (s Sprite) MovedOffset(offset phy.Vec2) Sprite {
	// a little hacky...
	// do we really need? can we calc when needed instead?
	oldX := s.TranslateMatrix.Elem(0, 3)
	oldY := s.TranslateMatrix.Elem(1, 3)
	return s.Moved(phy.V(offset.X+oldX, offset.Y+oldY))
}

// TexOffset resets texture offset
// TODO: we're using sprite width, so this dosen't account for animation frames...
// TODO: we want to get rid of this, push to GPU w/ uniform per tex
func (s Sprite) TexOffset(offset phy.Vec2) Sprite {
	s = s.setTexCoords(offset)
	// track so we can display or save eventually
	s.TexturePosition.OffsetType = OffsetAbsolute
	s.TexturePosition.Coordinates = offset
	return s
}

func (s Sprite) setTexCoords(offset phy.Vec2) Sprite {
	w := s.Width / s.Texture.Width
	h := s.Height / s.Texture.Height
	s.InstanceTex[0] = offset.X / s.Texture.Width
	s.InstanceTex[1] = offset.Y / s.Texture.Height
	s.InstanceTex[2] = w
	s.InstanceTex[3] = h
	return s
}

// TexIndex calculates the x and y offset for the Sprite, given a row
// and col into a spritesheet Texture
// TODO:
func (s Sprite) TexIndex(idx phy.Vec2) Sprite {
	x := idx.X * s.Width
	y := idx.Y * s.Height
	s = s.setTexCoords(phy.V(x, y))
	// track so we can display or save eventually
	s.TexturePosition.OffsetType = OffsetIndex
	s.TexturePosition.Coordinates = idx
	return s
}

// TODO: tex index off standard? e.g. 32 as multiplier, but width and height can be different...

// Scale scales the sprite
func (s Sprite) Scaled(scl phy.Vec2) Sprite {
	s.ScaleMatrix = phy.Scale4(s.Width*scl.X, s.Height*scl.Y, 1.0)
	return s
}

// Flip flips the sprite horizontally
// TODO: fix flip naming, flags
func (s Sprite) FlippedHorizontally(flip bool) Sprite {
	s.isFlipped = flip
	if flip {
		return s.Scaled(phy.V(-1.0, 1.0)) // TODO: will have to adjust if we have additional scale effects
	}
	return s.Scaled(phy.V(1.0, 1.0))
}

// IsFlipped indicates flip state
func (s Sprite) IsFlipped() bool {
	return s.isFlipped
}

// initModel sets up the matrices for the sprite
func (s Sprite) initModel() Sprite {
	s.ScaleMatrix = phy.Scale4(s.Width, s.Height, 1.0)
	s.TranslateMatrix = phy.Translate4(0.0, 0.0, 0.0)
	s.ModelMatrix = s.TranslateMatrix.Mul(s.ScaleMatrix)
	return s
}

// JSONSpriteComponentFactory returns factory function to create SpriteComponents from data
func JSONSpriteComponentFactory(texMgr TextureManager) ecs.JSONComponentFactory {
	return func(ent ecs.Entity, cd ecs.JSONComponentDescriptor) (ecs.Component, error) {
		sc := SpriteComponent{}
		err := json.Unmarshal(cd.Data, &sc)
		if err != nil {
			return nil, err
		}
		// need to retrieve real texture object!
		tex, ok := texMgr.GetTexture(sc.Instance.Texture.Name)
		if !ok {
			return nil, fmt.Errorf("unable to retrieve texture from sprite descriptor: %s", sc.Instance.Texture.Name)
		}
		// need to setup initial tex offset!!! must pick from data!
		sc.Instance.Texture = tex
		sc.Init(ent)
		return &sc, nil
	}
}
