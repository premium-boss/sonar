package render

import (
	"gitlab.com/premium-boss/sonar/ecs"
)

// RegisterComponents registers all component loader factories in pacakge
func RegisterComponents(reg map[string]ecs.JSONComponentFactory, renderables RenderableRegistry, texMgr TextureManager) {
	reg["render.Component"] = JSONComponentFactory(renderables)
	reg["render.SpriteComponent"] = JSONSpriteComponentFactory(texMgr)
}
