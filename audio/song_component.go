package audio

import (
	"encoding/json"

	"gitlab.com/premium-boss/sonar/ecs"
)

// Playback states
const (
	Uninitialized = iota
	Playing
	Paused
	Stopped
	Unloaded
)

// PlaybackState for audio state. We can keep a current and previous state on the
// component to guide system logic
type PlaybackState int

// SongComponent holds info for playing song assets. We need to be able to set and
// detect state changes based on actions.
type SongComponent struct {
	ecs.Component
	Name          string
	IsLooping     bool
	State         PlaybackState
	PreviousState PlaybackState
	Instance      *SoundInstance
}

// Init initializes component with entity. We always set previous state to Idle
// so we can start immediately if desired
func (c *SongComponent) Init(ent ecs.Entity) {
	c.PreviousState = Uninitialized // ALWAYS, even we want to start directly
}

// OnUnregister hook
func (c *SongComponent) OnUnregister(ent ecs.Entity) {}

// JSONSongComponentFactory returns factory function for loading components from JSON
func JSONSongComponentFactory() ecs.JSONComponentFactory {
	return func(ent ecs.Entity, desc ecs.JSONComponentDescriptor) (ecs.Component, error) {
		sc := SongComponent{}
		err := json.Unmarshal(desc.Data, &sc)
		if err != nil {
			return nil, err
		}
		sc.Init(ent)
		return &sc, nil
	}
}
