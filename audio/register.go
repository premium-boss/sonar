package audio

import "gitlab.com/premium-boss/sonar/ecs"

// RegisterComponents registers all component loader factories in package
func RegisterComponents(reg map[string]ecs.JSONComponentFactory) {
	reg["audio.SongComponent"] = JSONSongComponentFactory()
}

// RegisterSystems registers systems in package
func RegisterSystems(reg map[string]ecs.JSONSystemFactory, mgr Manager, sounds map[string]*Sound) {
	reg["audio.SongSystem"] = JSONSongSystemFactory(mgr, sounds)
}
