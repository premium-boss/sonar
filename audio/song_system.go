package audio

import (
	"reflect"

	"gitlab.com/premium-boss/sonar/ecs"
)

// SongSystem represents an audio playback system for long running background music
type SongSystem struct {
	ecs.GameSystem
	Manager Manager
	Sounds  map[string]*Sound // TODO: do we want to move to manager?
	TSong   reflect.Type
}

// NewSongSystem creats new audio system. For now, it handles initializing OpenAL.
func NewSongSystem(mgr Manager, sounds map[string]*Sound) *SongSystem {
	sys := SongSystem{
		Manager: mgr,
		TSong:   reflect.TypeOf(&SongComponent{}),
		Sounds:  sounds,
	}
	sys.SetConstraints(sys.TSong)
	return &sys
}

// Update updates the song system, given state that could be manipulated by other systems
func (sys *SongSystem) Update(scene *ecs.Scene, ents []ecs.Entity, gin ecs.GameInfo) {
	// TODO: if position, need to update the source!
	for _, ent := range ents {
		song := ent.Component(sys.TSong).(*SongComponent)
		if song.State == song.PreviousState {
			continue // TODO: continue here? or keep processing? or is that already another goroutine?
		}
		switch song.State {
		case Playing:
			if song.PreviousState == Uninitialized {
				// create instance
				sound := sys.Sounds[song.Name]
				song.Instance = sys.Manager.Instance(sound)
			}
			if song.IsLooping {
				sys.Manager.Loop(song.Instance)
			} else {
				sys.Manager.Play(song.Instance)
			}
		case Paused:
			sys.Manager.Pause(song.Instance)
		case Stopped:
			sys.Manager.Stop(song.Instance)
		case Unloaded:
			// TODO: do we need this one?
			sys.Manager.Destroy(song.Instance)
			song.Instance = nil
		}
		song.PreviousState = song.State
	}
}

// Close closes the SongSystem and releases resources. Do we want as part of interface? May need entity... Otherwise, flag
// on system to check in Update. Do we really needs ents? Could just close if we track instances, etc. in system. The hook
// might be good though, esp. if we ever do really need it...
// TODO: up to outside to close the sounds, mgr still
func (sys *SongSystem) Close(ents []ecs.Entity) {
	for _, ent := range ents {
		song := ent.Component(sys.TSong).(*SongComponent)
		if song.Instance != nil && !song.Instance.IsDestroyed {
			sys.Manager.Stop(song.Instance)
			sys.Manager.Destroy(song.Instance)
		}
	}
}

// JSONSongSystemFactory returns a factory function for creating the system
func JSONSongSystemFactory(mgr Manager, sounds map[string]*Sound) ecs.JSONSystemFactory {
	return func() (ecs.System, error) {
		return NewSongSystem(mgr, sounds), nil
	}
}
