package audio

import (
	"fmt"
	"io"
	"os"

	"github.com/cryptix/wav"
	"golang.org/x/mobile/exp/audio/al"
)

// OpenAL formats
var alFormats = [2][2]int{
	{al.FormatMono8, al.FormatMono16},
	{al.FormatStereo8, al.FormatStereo16},
}

// SoundDescriptor describes a song for loading
type SoundDescriptor struct {
	Name     string
	FileName string
}

// Sound tracks a registered sound that can be used to create instances
type Sound struct {
	IsClosed      bool
	IsSong        bool
	Format        uint32
	Size          uint32
	Rate          int32
	StartPosition int64
	Input         io.ReadSeeker // TODO: want a more generic io.ReadSeeker, but also want close...
	Data          interface{}   //implementation specific: TODO: ditch this, commit to an AL system, composition
}

// SoundInstance tracks a registered, playable instance of a sound
type SoundInstance struct {
	IsDestroyed bool
	IsLoop      bool
	Sound       *Sound      // TODO: is this going to hurt us? does it need to be a ref? all we gain is Closed...
	Data        interface{} //implementation specific
}

type alSoundData struct {
	buffers          []al.Buffer
	buffersPreLoaded uint8 // deal w/ reader? TODO; rename, total used or something
}

type alSoundInstanceData struct {
	source     al.Source
	lastBuffer int
}

// Reset resets the underlying io.ReadSeeker
func (sound *Sound) Reset() error {
	if sound.Input != nil {
		_, err := sound.Input.Seek(sound.StartPosition, os.SEEK_SET) // SEEK_SET indicates relative to origin of file
		return err
	}
	return nil // TODO: consider an error?
}

// Close closes the underlying io.ReadSeeker if it's actually an io.Closer
// TODO: anything more elegant here?
func (sound *Sound) Close() error {
	var err error
	if sound.Input != nil {
		cl, ok := sound.Input.(io.Closer)
		if ok {
			err = cl.Close()
		}
		sound.Input = nil
	}
	return err
}

func loadSongData(sound *Sound) error {
	// create the source, and make it relative to listener
	// TODO: can we just make this dynamic?
	// TODO: should we queue the source buffers here, or in instance?
	// source := mgr.createSource(phy.Vec3{}, phy.Vec3{})
	data := alSoundData{}
	data.buffers = al.GenBuffers(longRunningBufferCount)
	return loadSongBuffers(sound, data)
}

func loadSongBuffers(sound *Sound, data alSoundData) error {
	// TODO: not liking the side effects here...
	// TODO: better reset function
	for _, buf := range data.buffers {
		more, any, err := bufferChunked(buf, sound)
		if err != nil {
			return err
		}
		if any {
			data.buffersPreLoaded++
			// TODO: want to move this part to instances
			//sound.source.QueueBuffers(buf)
		}
		if !more {
			// TODO: special case if song doesn't need 4 buffers
			//sound.Reader.Close() // not needed anymore, yes it is, for reset!
			//sound.Reader = nil
			break
		}
	}
	sound.Data = data
	return nil
}

func loadEffectData(sound *Sound) error {
	data := alSoundData{}
	data.buffers = al.GenBuffers(1)
	err := bufferAll(data.buffers[0], sound)
	if err != nil {
		return err
	}
	data.buffersPreLoaded = 1
	// TODO: we want ability to close reader early... agh...
	sound.Close()
	sound.Data = data
	return nil
}

// TODO: assumes wav
// TODO: parse your own wav headers, we don't need sample by sample, just the num channels, rate, pos of data
// TODO: variadic function options: long running, relative to listener (SONG MODE)
// TODO: just use a seq for ids? then we need atomics... id might be safer...
// TODO: could just autobuf and handle, remove user constraint of knowing long running... the relative is nice though...
// TODO: this is a pain. Either return an interface, or deal. For now, get something going.
func loadALSound(path string, isSong bool) (*Sound, error) {
	// read wav file
	info, err := os.Stat(path)
	if err != nil {
		return nil, err
	}
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	reader, err := wav.NewReader(f, info.Size())
	if err != nil {
		return nil, err
	}

	// get dumb reader
	wavFile := reader.GetFile()
	format, err := getAudioFormat(reader)
	if err != nil {
		f.Close()
		return nil, err
	}
	rate := int32(reader.GetSampleRate())

	// create song object
	sound := Sound{
		IsSong:        isSong,
		Format:        uint32(format),
		Rate:          rate,
		Size:          wavFile.SoundSize,
		StartPosition: int64(reader.FirstSampleOffset()),
		Input:         f,
	}

	// get to start of wav portion
	err = sound.Reset()
	if err != nil {
		sound.Close()
		return nil, err
	}

	if isSong {
		err = loadSongData(&sound)
	} else {
		err = loadEffectData(&sound)
	}
	if err != nil {
		sound.Close()
	}
	return &sound, err
}

func getAudioFormat(reader *wav.Reader) (int, error) {
	ch := reader.GetNumChannels() - 1 // 0 for mono, 1 for stereo
	if ch > 1 {
		ch = 1
	}
	bits := reader.GetBitsPerSample()/8 - 1 // 0 for 8, 1 for 16
	if ch > 1 {
		return 0, fmt.Errorf("audio format: max 16 bits per sample, received %d", bits)
	}
	return alFormats[ch][bits], nil
}
