package audio

import (
	"fmt"
	"io"
	"log"
	"sync/atomic"
	"time"

	phy "gitlab.com/premium-boss/sonar/physics"
	"golang.org/x/mobile/exp/audio/al"
)

// TODO:
// - build in the utilities
// - have mutliple systems: e.g. a FollowListener component and system, a general sound effect movement system to update position after movement
// - a footstep system? vel + footstep component, or do we want audio component w/ trigger of velocity

// additional OpenAL constants
// TODO: these are taken from linux: AL/al.h. Make sure they're consistent across platforms...
const (
	alLooping        = 0x1007
	alSourceRelative = 0x0202
)

// sound directives
const (
	play soundDirective = iota
	pause
	stop
	loop
	destroy      // instance
	unload       // sound
	closeManager // manager
	listenAt
)

// buffer size and count
const (
	dataChunkSize          = 4096 // TODO: should we try to use file's chunk size?
	longRunningBufferCount = 4
)

type soundDirective int

// Manager interface for audio contexts
// TODO: consider interface separation? but really, we need one manager to do all of this, can't mix and match
type Manager interface {
	Init() error
	Close()
	ListenAt(pos phy.Vec3)
	Load(path string, isSong bool) (*Sound, error)
	Unload(*Sound)
	Instance(*Sound) *SoundInstance
	Play(*SoundInstance)
	Loop(*SoundInstance)
	Pause(*SoundInstance)
	Stop(*SoundInstance)
	Destroy(*SoundInstance)
}

// ALManager is an OpenAL implementation of the audio manager
type ALManager struct {
	closed    uint32
	done      chan struct{}
	updates   chan alSignal
	instances map[al.Source]*SoundInstance
}

type alSignal struct {
	directive soundDirective
	// will have one or the other
	Sound    *Sound
	Instance *SoundInstance
	Position phy.Vec3
}

// NewALManager ...
func NewALManager() *ALManager {
	mgr := ALManager{}
	return &mgr
}

// Init sets up default listener and device
// TODO: may need to handle changes?
func (mgr *ALManager) Init() error {
	mgr.done = make(chan struct{})
	mgr.instances = make(map[al.Source]*SoundInstance)
	mgr.updates = make(chan alSignal, 100) // TODO: count here? can this buffer fill? esp. if we update all per frame... max sources? default? err? shouldn't be change though...
	err := al.OpenDevice()
	if err != nil {
		return fmt.Errorf("audio manager: unable to get device: %v", err)
	}

	// we have a default listener
	initListener()

	// start processing
	go mgr.run()

	return nil
}

// Close closes AL devices. This is safe to call more than once, and signals the processing loop.
func (mgr *ALManager) Close() {
	// TODO: do this for other managers, be consistent
	if atomic.CompareAndSwapUint32(&mgr.closed, 0, 1) {
		log.Println("Closing!")
		//NOTE: this needs to be a directive, or will beat pending actions
		sig := alSignal{
			directive: closeManager,
		}
		mgr.updates <- sig
	} else {
		log.Println("already closed????")
	}
}

// Done waits for the manager to close. This is a blocking call.
func (mgr *ALManager) Done() {
	<-mgr.done
}

// TODO: just make the OpenAL calls in sync, OpenAL will do from bg thread!
// get rid of unnecessary pointers
// only track those w/ multiple buffers, e.g. songs, for the process loop. otherwise we're done after play!
// process loop is the only thing we need directives for: play, pause, stop. but we still need them
// anyway, could do it like this still for starters if we want!

// ListenAt moves listener
func (mgr *ALManager) ListenAt(pos phy.Vec3) {
	sig := alSignal{
		directive: listenAt,
		Position:  pos,
	}
	mgr.updates <- sig
}

// Unload signals manager to close a sound and release its resources
func (mgr *ALManager) Unload(sound *Sound) {
	// TODO: guard? if we use the flags here and don't care for the instances, we could stop using pointers...
	sound.IsClosed = true
	sig := alSignal{
		directive: unload,
		Sound:     sound,
	}
	mgr.updates <- sig
}

// Play plays a SoundInstance
func (mgr *ALManager) Play(instance *SoundInstance) {
	if instance.IsDestroyed { // TODO: log error?
		return
	}
	sig := alSignal{
		directive: play,
		Instance:  instance,
	}
	mgr.updates <- sig
}

// Loop loops a SoundInstance
func (mgr *ALManager) Loop(instance *SoundInstance) {
	if instance.IsDestroyed { // TODO: error?
		return
	}
	sig := alSignal{
		directive: loop,
		Instance:  instance,
	}
	mgr.updates <- sig
}

// Pause pauses a SoundInstance
func (mgr *ALManager) Pause(instance *SoundInstance) {
	if instance.IsDestroyed { // TODO: error?
		return
	}
	sig := alSignal{
		directive: pause,
		Instance:  instance,
	}
	mgr.updates <- sig
}

// Stop stops a SoundInstance
func (mgr *ALManager) Stop(instance *SoundInstance) {
	if instance.IsDestroyed { // TODO: error?
		return
	}
	sig := alSignal{
		directive: stop,
		Instance:  instance,
	}
	mgr.updates <- sig
}

// Destroy destroys a SoundInstance. This will close the source.
// TODO: is it a problem if we close sound but still have instances out there? potentially, YES
// TODO: return instance, should be Destroyed, etc.
func (mgr *ALManager) Destroy(instance *SoundInstance) {
	if instance.IsDestroyed {
		return
	}
	instance.IsDestroyed = true
	sig := alSignal{
		directive: destroy,
		Instance:  instance,
	}
	mgr.updates <- sig
}

// Load loads a sound, initializing buffers
func (mgr *ALManager) Load(path string, isSong bool) (*Sound, error) {
	return loadALSound(path, isSong)
}

// Instance returns a re-useable SoundInstance
func (mgr *ALManager) Instance(sound *Sound) *SoundInstance {
	// TODO: check sound closed?
	source := al.GenSources(1)[0]
	if sound.IsSong {
		// SONG MODE
		// TODO: is this okay here?
		setRelative(source)
	}
	data := alSoundInstanceData{
		source: source,
	}
	// TODO: if song, may need a reset option... re-use across, scenes, etc.
	sd := sound.Data.(alSoundData)
	bufsPreLoaded := sd.buffers[:sd.buffersPreLoaded]
	source.QueueBuffers(bufsPreLoaded...)
	return &SoundInstance{Sound: sound, Data: data}
}

func (mgr *ALManager) run() {
	// TODO: setup to handle multiple, either in 1 loop or separate goroutines
	// fanning out based on number of cores as a max makes some sense
	// then we want to process samples using buffers off a queue
	// we're tracking the reader io either in the component or a mapping here...
	// or just handle in one?
	// anyway, get basic going, then consider strategy
	// TODO: need a ticker here too!
	// TODO: should we make a struct instead???
	ticker := time.NewTicker(10 * time.Millisecond)
RUN_LOOP:
	for {
		select {
		case <-ticker.C:
			// TODO: process
			// TODO: do we want to clean up stopped non-loops automatically? or provide an option for one-time after all?
			// TODO: if not a song, we don't even really need process loop or tracking of the instance, could just do
			// from the outer call, perhaps get rid of the pointers. only care about songs really for the buffer shuffling,
			// then we don't need to worry about cleanup
			// unless it turns out that most effects also require multiple buffers...
			for _, i := range mgr.instances {
				if i.Sound.IsSong {
					mgr.processSong(i)
				}
			}
		// TODO: do we like this (below) as a channel? or should we do this part in
		// sync, then update all. We could, but would have to lock on instances...
		// we could do a swap instead during the tick, which would require a lock every tick
		// we could let the system update
		// ALTERNATELY, we could have the system decide whether or not to keep playing, and always
		// queue the actions, which then include the notion of a "tick", then this routine always
		// processes the queue until there are no actions, rather than ticks... that forces audio
		// to process at the physics rate though, which may not be desirable. still, worth considering...
		// system could just call play, stop, loop, etc. as needed, then we just close at end...
		case upd := <-mgr.updates:
			switch upd.directive {
			case closeManager:
				log.Println("closing manager")
				mgr.cleanup()
				ticker.Stop()
				break RUN_LOOP

			case listenAt:
				al.SetListenerPosition(upd.Position.AsArray())

			case unload:
				// TODO: do we care if there are any instances? probably, as the buffer is shared. this would mean we
				// need to backtrack sound to soundInstance, which could be a pain. Alternately, just leave up to the
				// user, as we'll eventually clean up all anyway...
				// TODO: how do we know when we can close an instance if we don't update a status on it? or close once stopped
				// we might really need more of an update chnage-set as the input is doing...
				// if there are no instances, do we even need to handle here?
				upd.Sound.Close()
				sd := upd.Sound.Data.(alSoundData)
				al.DeleteBuffers(sd.buffers...)
				log.Println("unloading sound!")

			case play:
				// this may be first play, or a resume, or changing from loop.
				// if a resume from pause, OpenAL will resume the buffer at position. If stop, this will start over.
				// TODO: for a song, we will need an explicit reset for the reader...
				// TODO: this is requiring us to use pointers: is that okay?
				data := upd.Instance.Data.(alSoundInstanceData)
				if upd.Instance.IsLoop {
					setLooping(data.source, false)
				}
				upd.Instance.IsLoop = false
				mgr.instances[data.source] = upd.Instance
				al.PlaySources(data.source)
				log.Println("playing sound!")

			case loop:
				// this is first play, or changing from play to loop, etc. Either way, we should be fine
				data := upd.Instance.Data.(alSoundInstanceData)
				if !upd.Instance.Sound.IsSong {
					setLooping(data.source, true)
				}
				// in case a song, track looping on data, so we can start over
				upd.Instance.IsLoop = true
				mgr.instances[data.source] = upd.Instance
				al.PlaySources(data.source)
				log.Println("looping sound!")

			case pause:
				data := upd.Instance.Data.(alSoundInstanceData)
				al.PauseSources(data.source)
				delete(mgr.instances, data.source) // TODO: some way to resume vs reset?
				log.Println("pausing sound!")

			case stop:
				data := upd.Instance.Data.(alSoundInstanceData)
				al.StopSources(data.source)
				delete(mgr.instances, data.source)
				// TODO: if song, we need to dequeue and reset the buffers and reader!
				// TODO: make this private and not settable, or potential mischief
				if upd.Instance.Sound.IsSong {
					soundData := upd.Instance.Sound.Data.(alSoundData)
					data.source.UnqueueBuffers(soundData.buffers...) // TODO: might not be queued...
					// reset the reader and initial buffers!
					upd.Instance.Sound.Reset()
					// TODO: better reset function for buffers, this makes soundData out here stale, kinda weird...
					soundData.buffersPreLoaded = 0
					loadSongBuffers(upd.Instance.Sound, soundData)
				}
				log.Println("stopping sound!")

			case destroy:
				mgr.destroyInstance(upd.Instance)

			default:
				log.Printf("ALManager: unknown directive: %d\n", upd.directive)
			}
		}
	}
	log.Println("signalling done")
	close(mgr.done)
}

func (mgr *ALManager) destroyInstance(instance *SoundInstance) {
	instanceData := instance.Data.(alSoundInstanceData)
	soundData := instance.Sound.Data.(alSoundData)
	if instanceData.source.State() != al.Stopped {
		// TODO: do we need this?
		al.StopSources(instanceData.source)
	}

	// TODO: technically, may not be queued if we never started playing, or close to end
	// do we need to guard this?. not sure if these are all buffered or not...
	instanceData.source.UnqueueBuffers(soundData.buffers...)
	al.DeleteSources(instanceData.source)
	delete(mgr.instances, instanceData.source)
	log.Println("destroying sound!")
}

func (mgr *ALManager) processSong(instance *SoundInstance) error {
	insData := instance.Data.(alSoundInstanceData)
	soundData := instance.Sound.Data.(alSoundData)
	proc := int(insData.source.BuffersProcessed())
	var more, any bool
	var err error
	for i := 0; i < proc; i++ {
		buf := soundData.buffers[insData.lastBuffer]
		insData.source.UnqueueBuffers(buf)
		more, any, err = bufferChunked(buf, instance.Sound)
		if err != nil {
			return err
		}
		if any {
			insData.source.QueueBuffers(buf) // add back to queue
		}
		if !more {
			// TODO: need to mark something if done and song and not looping..., otherwise if looping, need to reset
			// the reader, etc.
			if instance.IsLoop {
				log.Println("reset loop!")
				instance.Sound.Reset() // TODO: keep processing, or wait til next pass and allow chance of silence?
			} else {
				log.Println("no more samples in song, exiting, TODO!")
				delete(mgr.instances, insData.source)
			}
			break
		}
		insData.lastBuffer++
		if insData.lastBuffer == len(soundData.buffers) {
			insData.lastBuffer = 0
		}
	}
	instance.Data = insData
	if more && insData.source.State() == al.Stopped {
		log.Println("STOPPED prematurely!!!")
		al.PlaySources(insData.source)
	}
	return nil
}

func (mgr *ALManager) cleanup() {
	al.CloseDevice()
	for _, instance := range mgr.instances {
		mgr.destroyInstance(instance)
	}
	// TODO: track sounds, or rely on caller?
	log.Println("audio: al manager closed")
}

func bufferChunked(buf al.Buffer, sound *Sound) (bool, bool, error) {
	buffer := [dataChunkSize]byte{}
	bs := buffer[:]
	more := true
	n, err := sound.Input.Read(bs)
	if err == io.EOF {
		more = false
		err = nil
	} else if err != nil {
		return false, false, err
	}
	// NOTE: we can get bytes read and EOF at same time. alternately, use io.ReadFull and track length up to size
	any := n > 0
	if any {
		buf.BufferData(sound.Format, bs[:n], sound.Rate)
	}
	return more, any, err
}

func bufferAll(buf al.Buffer, sound *Sound) error {
	buffer := make([]byte, sound.Size)
	_, err := io.ReadFull(sound.Input, buffer)
	if err != nil {
		return err
	}
	buf.BufferData(sound.Format, buffer, sound.Rate)
	return nil
}

func initListener() {
	al.GetListenerf(1)
	al.SetListenerPosition(al.Vector{0, 0, 1})
	al.SetListenerVelocity(al.Vector{0, 0, 0})
	al.SetListenerOrientation(al.Orientation{
		Forward: al.Vector{0, 0, 1},
		Up:      al.Vector{0, 1, 0},
	})
}

func (mgr *ALManager) createSource(pos, vel phy.Vec3) al.Source {
	sources := al.GenSources(1)
	source := sources[0]
	source.SetGain(1.0)
	source.SetPosition(pos.AsArray())
	source.SetVelocity(vel.AsArray())
	return source
}

func setRelative(source al.Source) {
	source.Seti(alSourceRelative, 1)
}

func setLooping(source al.Source, loop bool) {
	var val int32
	if loop {
		val = 1
	}
	source.Seti(alLooping, val)
}
