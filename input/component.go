package input

import (
	"encoding/json"

	"gitlab.com/premium-boss/sonar/ecs"
	"gitlab.com/premium-boss/sonar/physics"
)

// TODO: would this be simpler as part of GameInfo and not a specific component?
// really is tied to camera in a way...

// Component holds input state data. It wraps InputState to make updates
// to original easier through entire struct swap.
type Component struct {
	State State
}

// State maintains info on Input
type State struct {
	Tick             uint64
	Button1          bool
	Button2          bool
	Button3          bool
	Zoom             bool
	Camera           physics.Vec2
	Movement         physics.Vec2
	MousePosition    physics.Vec2
	MouseButtonLeft  bool
	MouseButtonRight bool
}

// NewComponent creates a new Input component.
func NewComponent() *Component {
	return &Component{}
}

// Init initializes with Entity
func (i *Component) Init(Entity ecs.Entity) {
}

// OnUnregister hook for an Entity having this component removed
func (i *Component) OnUnregister(ent ecs.Entity) {
}

// JSONComponentFactory returns factory function for loading components from JSON
func JSONComponentFactory() ecs.JSONComponentFactory {
	return func(ent ecs.Entity, desc ecs.JSONComponentDescriptor) (ecs.Component, error) {
		ic := Component{}
		err := json.Unmarshal(desc.Data, &ic)
		if err != nil {
			return nil, err
		}
		ic.Init(ent)
		return &ic, nil
	}
}
