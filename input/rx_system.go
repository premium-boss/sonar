package input

import (
	"reflect"
	"sync"

	"gitlab.com/premium-boss/sonar/ecs"
)

// Input system constants
const (
	MaxΔs = 128
)

// RxSystem handles logic to update entities with a InputRx component
type RxSystem struct {
	ecs.GameSystem
	TIpt reflect.Type
	TRx  reflect.Type
	Δs   *sync.Map
}

// NewRxSystem initializes a system with constraints
func NewRxSystem() *RxSystem {
	i := RxSystem{
		TIpt: reflect.TypeOf(&Component{}),
		TRx:  reflect.TypeOf(&RxComponent{}),
		Δs:   new(sync.Map),
	}

	i.SetConstraints(i.TIpt, i.TRx)
	return &i
}

// QueueΔ accepts deltas from another goroutine and queues them for later
// application to input components
func (sys *RxSystem) QueueΔ(d *Δ) {
	var deltas chan *Δ
	val, ok := sys.Δs.Load(d.PlayerID)
	if ok {
		deltas = val.(chan *Δ)
	} else {
		deltas = make(chan *Δ, MaxΔs)
		sys.Δs.Store(d.PlayerID, deltas)
	}
	deltas <- d
}

// RemovePlayer removes a player from the input queue
func (sys *RxSystem) RemovePlayer(id uint32) {
	sys.Δs.Delete(id)
}

// Update updates all entities with a InputRx component. It applies any Δs
// waiting in the channel. This should run before any movement updates.
func (sys *RxSystem) Update(scene *ecs.Scene, ents []ecs.Entity, gin ecs.GameInfo) {
	for _, e := range ents {
		rx := e.Component(sys.TRx).(*RxComponent)
		// if they have no channel yet, find it.
		if rx.Δs == nil {
			val, ok := sys.Δs.Load(e.ID)
			if !ok {
				continue
			}
			rx.Δs = val.(chan *Δ)
		}
		// check for an input
		select {
		case delta := <-rx.Δs:
			ipt := e.Component(sys.TIpt).(*Component)
			ApplyΔ(&ipt.State, delta)
		default:
		}
	}
}

// Close closes the system and releases resources
func (sys *RxSystem) Close(ents []ecs.Entity) {
	// TODO:
}

// JSONRxSystemFactory returns a factory function for creating the system
func JSONRxSystemFactory() ecs.JSONSystemFactory {
	return func() (ecs.System, error) {
		return NewRxSystem(), nil
	}
}
