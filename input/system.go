package input

import (
	"reflect"

	"gitlab.com/premium-boss/sonar/ecs"
	"gitlab.com/premium-boss/sonar/physics"
)

// System system applies input to movement
// TODO: when we have collisions / actions, this will need renaming...
// could leave with movement...
type System struct {
	ecs.GameSystem
	TIpt    reflect.Type
	Manager Manager
}

// NewSystem creates a new Input system
func NewSystem(mgr Manager) *System {
	ipt := System{
		TIpt:    reflect.TypeOf(&Component{}),
		Manager: mgr,
	}
	ipt.SetConstraints(ipt.TIpt)
	return &ipt
}

// Update updates the Input system
func (sys *System) Update(scene *ecs.Scene, ents []ecs.Entity, gin ecs.GameInfo) {
	sys.Manager.Update() // update from polling system with latest events
	for _, ent := range ents {
		ipt := ent.Component(sys.TIpt).(*Component)
		sys.updateInput(ipt)
	}
}

// Close closes the system and releases resources
func (sys *System) Close(ents []ecs.Entity) {

}

// JSONSystemFactory returns a factory function for creating the system
func JSONSystemFactory(mgr Manager) ecs.JSONSystemFactory {
	return func() (ecs.System, error) {
		return NewSystem(mgr), nil
	}
}

// updateInput updates current input state based on keys / buttons
// TODO: update as needed for addtional buttons, joystick, etc.
// TODO: need to translate screen coords to world
func (sys *System) updateInput(ipt *Component) {
	// update velocity inputs
	vel := physics.ZV2
	if sys.Manager.KeyState(KeyRight).Pressed {
		vel.X += 1.0
	}
	if sys.Manager.KeyState(KeyLeft).Pressed {
		vel.X -= 1.0
	}
	if sys.Manager.KeyState(KeyUp).Pressed {
		vel.Y -= 1.0 // coords reversed...
	}
	if sys.Manager.KeyState(KeyDown).Pressed {
		vel.Y += 1.0
	}
	ipt.State.Movement = vel
	// update camera inputs
	cam := physics.ZV2
	if sys.Manager.KeyState(KeyD).Pressed {
		cam.X += 1.0
	}
	if sys.Manager.KeyState(KeyA).Pressed {
		cam.X -= 1.0
	}
	if sys.Manager.KeyState(KeyW).Pressed {
		cam.Y -= 1.0 // coords reversed...
	}
	if sys.Manager.KeyState(KeyS).Pressed {
		cam.Y += 1.0
	}
	// update mouse position and clicks?
	ipt.State.MousePosition = sys.Manager.MousePosition()
	ipt.State.MouseButtonLeft = sys.Manager.MouseState(MouseButtonLeft).Pressed
	ipt.State.MouseButtonRight = sys.Manager.MouseState(MouseButtonRight).Pressed
	ipt.State.Camera = cam
	ipt.State.Zoom = sys.Manager.KeyState(KeySpace).Pressed // TODO: testing camera for now

}
