package input

import (
	"gitlab.com/premium-boss/sonar/ecs"
	"gitlab.com/premium-boss/sonar/physics"
)

// RxComponent tracks external updates to inputs.
// This should be used on the server.
type RxComponent struct {
	Δs chan *Δ
}

// Δ is a delta for input state in transit
type Δ struct {
	PlayerID   uint32
	Buttons    uint16
	Horizontal float32
	Vertical   float32
}

// NewRxComponent creates a new rx component.
// NOTE: the channel is NOT created here, but by the system
func NewRxComponent() *RxComponent {
	return &RxComponent{}
}

// Init initializes with Entity
func (i *RxComponent) Init(Entity ecs.Entity) {
}

// OnUnregister hook for an Entity having this component removed
func (i *RxComponent) OnUnregister(ent ecs.Entity) {
}

// ApplyΔ applies the changes to an input State
func ApplyΔ(st *State, delta *Δ) {
	// TODO: decode the button mask
	st.Tick++ // increment every time we get an input
	st.Movement = physics.V(delta.Horizontal, delta.Vertical)
}
