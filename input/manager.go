package input

import (
	"gitlab.com/premium-boss/sonar/ecs"
	"gitlab.com/premium-boss/sonar/physics"
)

// Manager is an interface to platform-specific input functionality
type Manager interface {
	Update()
	KeyState(key Key) KeyState
	MouseState(button MouseButton) MouseButtonState
	MousePosition() physics.Vec2
	OnKey() ecs.OnKey
	OnMouseButton() ecs.OnMouseButton
	OnMouseMove() ecs.OnMouseMove
}

// GLManager handles input events. It holds two sets of key states to
// watch. We want to be able to directly index through the key value, though
// this wastes a bit of memory in the gaps.
type GLManager struct {
	currentKeys          []KeyState
	nextKeys             []KeyState
	currentMouseButtons  []MouseButtonState
	nextMouseButtons     []MouseButtonState
	currentMousePosition physics.Vec2
	nextMousePosition    physics.Vec2
}

// NewGLManager sets up the InputManager.
func NewGLManager() *GLManager {
	keyCt := KeyLast + 1
	mouseCt := MouseButtonLast + 1
	im := GLManager{
		currentKeys:         make([]KeyState, keyCt, keyCt),
		nextKeys:            make([]KeyState, keyCt, keyCt),
		currentMouseButtons: make([]MouseButtonState, mouseCt, mouseCt),
		nextMouseButtons:    make([]MouseButtonState, mouseCt, mouseCt),
	}
	return &im
}

// Update updates the GLManager. Because we do not have Mouse repeat events,
// we handle the MouseButtonState here.
func (im *GLManager) Update() {
	copy(im.currentKeys, im.nextKeys)
	im.currentMousePosition = im.nextMousePosition
	for i := 0; i <= int(MouseButtonLast); i++ {
		im.currentMouseButtons[i].checkState(im.nextMouseButtons[i])
	}
}

// KeyState returns state of a key
func (im *GLManager) KeyState(key Key) KeyState {
	return im.currentKeys[key]
}

// MouseState returns state of MouseButton
func (im *GLManager) MouseState(button MouseButton) MouseButtonState {
	return im.currentMouseButtons[button]
}

// MousePosition returns position of cursor
func (im *GLManager) MousePosition() physics.Vec2 {
	return im.currentMousePosition
}

// OnKey returns key callback
func (im *GLManager) OnKey() ecs.OnKey {
	return im.onKey
}

// OnMouseButton returns mouse button callback
func (im *GLManager) OnMouseButton() ecs.OnMouseButton {
	return im.onMouseButton
}

// OnMouseMove returns mouse move callback
func (im *GLManager) OnMouseMove() ecs.OnMouseMove {
	return im.onMouseMove
}

// OnKey can be assigned to handle key press events. Action is be press,
// release, or repeat.
func (im *GLManager) onKey(key, action, mod int) {
	im.nextKeys[key].update(Action(action))
}

// OnMouseButton hanldes mouse button events. Action is press or release.
func (im *GLManager) onMouseButton(btn, action, mod int) {
	im.nextMouseButtons[btn].update(Action(action))
}

// OnMouseMove tracks mouse movement
func (im *GLManager) onMouseMove(x, y float32) {
	im.nextMousePosition.X = x
	im.nextMousePosition.Y = y
}

// update updates a KeyState
func (ks *KeyState) update(action Action) {
	switch action {
	case ActionPress:
		ks.JustPressed = !ks.Pressed // if this is first press
		ks.Pressed = true
	case ActionRelease:
		ks.Pressed = false
		ks.JustPressed = false
		ks.Repeated = false
	case ActionRepeat:
		ks.Repeated = true
		ks.JustPressed = false
	}
}

// update updates a MouseButtonState. NOTE: a MouseButton does not register
// repeats or multiple presses, only the press and release events. This means
// the manager's Update call has to track whether it was already pressed
// frame-to-frame, but better than forcing on the client.
func (mb *MouseButtonState) update(action Action) {
	switch action {
	case ActionPress:
		mb.JustPressed = !mb.Pressed // if this is first press (it will be)
		mb.Pressed = true
	case ActionRelease:
		mb.Pressed = false
		mb.JustPressed = false
	}
}

// checkPress determines if Pressed for first time between Update calls
func (mb *MouseButtonState) checkState(nxt MouseButtonState) {
	if mb.Pressed {
		mb.JustPressed = false // pressed for 2 straight update calls
	} else {
		mb.JustPressed = nxt.JustPressed
	}
	mb.Pressed = nxt.Pressed
}
