package input

import (
	"gitlab.com/premium-boss/sonar/ecs"
)

// RegisterComponents registers all loader factories in pacakge
func RegisterComponents(reg map[string]ecs.JSONComponentFactory) {
	reg["input.Component"] = JSONComponentFactory()
}

// RegisterSystems registers systems in package
func RegisterSystems(reg map[string]ecs.JSONSystemFactory, mgr Manager) {
	reg["input.System"] = JSONSystemFactory(mgr)
	reg["input.RxSystem"] = JSONRxSystemFactory()
}
