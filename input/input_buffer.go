package input

import "gitlab.com/premium-boss/sonar/tickbuf"

const bufferSize = 512

// StateBuffer is a simple sequence buffer for tracking input states
type StateBuffer struct {
	tb *tickbuf.Buffer
}

// NewStateBuffer ...
func NewStateBuffer() *StateBuffer {
	tbuf := tickbuf.NewBuffer(bufferSize)
	ibuf := StateBuffer{tb: tbuf}
	return &ibuf
}

// Set sets an input for an input tick
func (ibuf *StateBuffer) Set(tick uint64, ipst *State) {
	ibuf.tb.Set(tick, ipst)
}

// Get retrives input at a given input tick
func (ibuf *StateBuffer) Get(tick uint64) (bool, *State) {
	ok, data := ibuf.tb.Get(tick)
	if !ok {
		return ok, nil
	}
	ipt := data.(*State)
	if ipt == nil {
		return false, nil
	}
	return ok, ipt
}

// Clear clears input at a given input tick
func (ibuf *StateBuffer) Clear(tick uint64) {
	ibuf.tb.Clear(tick)
}
