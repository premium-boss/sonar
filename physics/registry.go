package physics

// ColliderRegistry is a registry of names to factory functions for generating colliders
type ColliderRegistry map[string]ColliderFactory

// NewRegistry provides a registry of default colliders and names for use in deserialization.
func NewRegistry() ColliderRegistry {
	reg := ColliderRegistry{
		"physics.BoxCollider":    func() Collider { return &BoxCollider{} },
		"physics.CircleCollider": func() Collider { return &CircleCollider{} },
	}
	return reg
}
