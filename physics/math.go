package physics

import "math"

// --- Math utils ---

// Sqrt ...
func Sqrt(f float32) float32 {
	return float32(math.Sqrt(float64(f)))
}

// Abs ...
func Abs(f float32) float32 {
	return float32(math.Abs(float64(f)))
}

// Sin ...
func Sin(angle float32) float32 {
	return float32(math.Sin(float64(angle)))
}

// Cos ...
func Cos(angle float32) float32 {
	return float32(math.Cos(float64(angle)))
}
