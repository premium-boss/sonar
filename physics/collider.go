package physics

// TODO: add edges and general polygons
// TODO: change this to value semantics? then moving pos is much easier...

// Collider handles collisions
type Collider interface {
	Collision(o Collider) Collision
	Contains(point Vec2) bool
	Update(pos Vec2)
}

// Collision describes if we have a collision, and provides the minimum
// translation vector for adjusting position
// TODO: include other Collider in result if needed for a force or other props
type Collision struct {
	IsColliding bool
	MTV         Vec2
}

// ColliderFactory generates instances of colliders
type ColliderFactory func() Collider

func collisionBoxBox(b *BoxCollider, o *BoxCollider) Collision {
	cr := Collision{}
	if !b.Bounds.Intersects(o.Bounds) {
		return cr
	}
	cr.IsColliding = true
	cr.MTV = b.Bounds.MTV(o.Bounds)
	return cr
}

func collisionCircleCircle(c *CircleCollider, o *CircleCollider) Collision {
	// NOTE: two circles can be said to collide if the distance between their
	// centers is less than the sum of their radii
	col := Collision{}
	rsum := c.Circle.Radius + o.Circle.Radius
	rsum2 := rsum * rsum
	d2 := c.Circle.Center.DistanceSq(o.Circle.Center)
	if d2 >= rsum2 {
		return col // no collision
	}
	// we have a collision: calculate MTV
	col.IsColliding = true
	if d2 == 0.0 {
		col.MTV = V(0, rsum) // centers are aligned
	} else {
		dist := Sqrt(d2)
		scale := 1.0 / dist
		norm := c.Circle.Center.Sub(o.Circle.Center).Scale(scale)
		amt := dist - rsum
		col.MTV = norm.Scale(amt)
	}
	return col
}

func collisionBoxCircle(b *BoxCollider, c *CircleCollider) Collision {
	// NOTE: the trick here is to convert into the Circle vs Circle detection
	// but pay attention if Circle is deep in the Rect, as it will need to move
	// more than its radius
	col := Collision{}
	pt := b.Bounds.ClosestPoint(c.Circle.Center)
	d2 := pt.DistanceSq(c.Circle.Center)
	r2 := c.Circle.Radius * c.Circle.Radius
	inside := b.Bounds.Contains(c.Circle.Center)
	if d2 >= r2 && !inside {
		return col // no collision
	}
	// we have a collision: calculate MTV
	col.IsColliding = true
	if d2 == 0.0 {
		// Circle center is on border of Rect
		// NOTE: we could consider checking if the center is on a corner and
		// move outwards, but for now favor x-axis if a tie
		switch {
		case pt.X == b.Bounds.Min.X:
			col.MTV = V(-c.Circle.Radius, 0.0)
		case pt.X == b.Bounds.Max.X:
			col.MTV = V(c.Circle.Radius, 0.0)
		case pt.Y == b.Bounds.Min.Y:
			col.MTV = V(0.0, -c.Circle.Radius)
		default:
			col.MTV = V(0.0, c.Circle.Radius)
		}
	} else {
		dist := Sqrt(d2)
		scale := 1.0 / dist
		norm := c.Circle.Center.Sub(pt).Scale(scale)
		var amt float32
		if inside {
			amt = -(dist + c.Circle.Radius)
		} else {
			amt = dist - c.Circle.Radius
		}
		col.MTV = norm.Scale(amt)
	}
	return col
}
