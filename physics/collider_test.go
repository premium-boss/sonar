package physics

import "testing"

func TestCollisionBoxBox(t *testing.T) {
	tests := []struct {
		rect1    Rect
		rect2    Rect
		collides bool
		mtv      Vec2
	}{
		{R(V(5.0, 5.0), V(0.0, 0.0)), R(V(10.0, 10.0), V(5.0, 5.0)), false, V(0.0, 0.0)},
		{R(V(-20.0, -15.0), V(-1.0, 1.0)), R(V(5.0, 5.0), V(6.0, 6.0)), false, V(0.0, 0.0)},
		{R(V(5.0, 5.0), V(0.0, 0.0)), R(V(10.0, 10.0), V(3.0, 3.0)), true, V(0.0, -2.0)},
		{R(V(5.0, 5.0), V(0.0, 0.0)), R(V(5.0, 5.0), V(0.0, 0.0)), true, V(0, 5.0)},
		{R(V(5.0, 5.0), V(0.0, 0.0)), R(V(4.0, 4.0), V(3.0, 3.0)), true, V(0.0, -2.0)},
	}
	for i, tt := range tests {
		bc1 := BoxCollider{Bounds: tt.rect1}
		bc2 := BoxCollider{Bounds: tt.rect2}
		col := bc1.Collision(&bc2)
		if col.IsColliding != tt.collides {
			t.Errorf("test %d: bad collision, expected: %v, got: %v", i, tt.collides, col.IsColliding)
		}
		if tt.collides && !col.MTV.IsSame(tt.mtv) {
			t.Errorf("test %d: bad collision, expected: %v, got: %v", i, tt.mtv, col.MTV)
		}
	}
}

func TestCollisionCircleCirle(t *testing.T) {
	tests := []struct {
		a        Circle
		b        Circle
		collides bool
		mtv      Vec2
	}{
		{NewCircle(V(0, 0), 2), NewCircle(V(0, 0), 2), true, V(0, 4)},
		{NewCircle(V(0, 0), 2), NewCircle(V(0, 0), 5), true, V(0, 7)},
		{NewCircle(V(0, 0), 2), NewCircle(V(2, 2), 1), true, V(0.121320374, 0.121320374)},
		{NewCircle(V(0, 0), 2), NewCircle(V(4, 4), 2), false, V(0, 0)},
	}
	for i, tt := range tests {
		cb := CircleCollider{tt.b}
		ca := CircleCollider{tt.a}
		col := ca.Collision(&cb)
		if col.IsColliding != tt.collides {
			t.Errorf("test %d: bad collision, expected: %v, got: %v", i, tt.collides, col.IsColliding)
		}
		if tt.collides && !col.MTV.IsSame(tt.mtv) {
			t.Errorf("test %d: bad collision, expected: %v, got: %v", i, tt.mtv, col.MTV)
		}
	}
}

func TestCollisionBoxCircle(t *testing.T) {
	tests := []struct {
		a        Rect
		b        Circle
		collides bool
		mtv      Vec2
	}{
		{R(V(5.0, 5.0), V(0.0, 0.0)), NewCircle(V(0, 0), 2), true, V(-2, 0)},
		{R(V(5.0, 5.0), V(0.0, 0.0)), NewCircle(V(0, 0), 5), true, V(-5, 0)},
		{R(V(5.0, 5.0), V(-5.0, -5.0)), NewCircle(V(0, 0), 2), true, V(7, 0)},
		{R(V(5.0, 5.0), V(0.0, 0.0)), NewCircle(V(2, 2), 1), true, V(-3, 0)}, // TODO: check
		{R(V(2.0, 2.0), V(0.0, 0.0)), NewCircle(V(4, 4), 2), false, V(0, 0)},
	}
	for i, tt := range tests {
		a := BoxCollider{Bounds: tt.a}
		b := CircleCollider{tt.b}
		col := a.Collision(&b)
		if col.IsColliding != tt.collides {
			t.Errorf("test %d: bad collision, expected: %v, got: %v", i, tt.collides, col.IsColliding)
		}
		if tt.collides && !col.MTV.IsSame(tt.mtv) {
			t.Errorf("test %d: bad collision, expected: %v, got: %v", i, tt.mtv, col.MTV)
		}
		// TODO: use better values for edge cases, all cases of overlap
	}
}
