package physics

// Spaceable represents an entity than can be spatially hashed.
type Spaceable interface {
	Position() Vec2
	Dimensions() Vec2
	IsSame(s Spaceable) bool
}

// SpatialHash is a structure for tracking entities in a grid
type SpatialHash struct {
	CellSize float32
	Grid     map[hashKey][]Spaceable
}

type hashKey struct {
	x int
	y int
}

// NewSpatialHash returns a new SpatialHash and initializes with cell size
func NewSpatialHash(cellSz int) *SpatialHash {
	sh := SpatialHash{
		CellSize: float32(cellSz),
		Grid:     make(map[hashKey][]Spaceable),
	}
	return &sh
}

// Add adds an entity to the SpatialHash
func (sh *SpatialHash) Add(s Spaceable) {
	pos := s.Position()
	dim := s.Dimensions()
	pts := sh.spaces(pos, dim)
	for i := range pts {
		key := sh.hash(pts[i])
		sh.Grid[key] = append(sh.Grid[key], s)
	}
}

// Nearby retrieves Spaceables near a Spaceable.
// NOTE: it does NOT deduplicate by default
// TODO: change that behavior?
func (sh *SpatialHash) Nearby(s Spaceable) []Spaceable {
	obs := make([]Spaceable, 0)
	pos := s.Position()
	dim := s.Dimensions()
	pts := sh.spaces(pos, dim)
	for i := range pts {
		key := sh.hash(pts[i])
		obs = append(obs, sh.Grid[key]...)
	}
	return obs
}

// Remove removes a Spaceable from the SpatialHash
func (sh *SpatialHash) Remove(s Spaceable) {
	pos := s.Position()
	dim := s.Dimensions()
	pts := sh.spaces(pos, dim)
	for i := range pts {
		key := sh.hash(pts[i])
		spaces := sh.Grid[key]
		for j := range spaces {
			if spaces[j].IsSame(s) {
				copy(spaces[j:], spaces[j+1:])
				last := len(spaces) - 1
				spaces[last] = nil
				spaces = spaces[:last]
				sh.Grid[key] = spaces
				break
			}
		}
	}
}

// Clear clears the spatial hash
func (sh *SpatialHash) Clear() {
	sh.Grid = make(map[hashKey][]Spaceable)
}

func (sh *SpatialHash) spaces(pt Vec2, dim Vec2) []Vec2 {
	max := pt.Add(dim)
	cols := int(max.X/sh.CellSize-pt.X/sh.CellSize) + 1
	rows := int(max.Y/sh.CellSize-pt.Y/sh.CellSize) + 1
	// now loop and increment
	var spaces []Vec2
	sample := pt
	for i := 0; i < cols; i++ {
		for j := 0; j < rows; j++ {
			spaces = append(spaces, sample)
			sample.Y += sh.CellSize // increment Y
		}
		sample.Y = pt.Y         // reset Y
		sample.X += sh.CellSize // increment X
	}
	return spaces
}

func (sh *SpatialHash) hash(pt Vec2) hashKey {
	hk := hashKey{
		x: int(pt.X / sh.CellSize),
		y: int(pt.Y / sh.CellSize),
	}
	return hk
}
