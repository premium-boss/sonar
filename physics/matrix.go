package physics

// Mat4 is a 4 x 4 matrix
type Mat4 [4][4]float32

// I4 is the 4 x 4 Identity matrix
var I4 = Mat4{
	{1, 0, 0, 0},
	{0, 1, 0, 0},
	{0, 0, 1, 0},
	{0, 0, 0, 1},
}

// Row returns a row of the matrix.
func (m Mat4) Row(i int) [4]float32 {
	return m[i]
}

// Col returns a column of the matrix.
func (m Mat4) Col(i int) [4]float32 {
	return [4]float32{m[0][i], m[1][i], m[2][i], m[3][i]}
}

// Elem grabs the element at row, col.
func (m Mat4) Elem(row, col int) float32 {
	return m[row][col]
}

// ColumnMajor returns the matrix as a column-major ordered array of float32s.
func (m Mat4) ColumnMajor() [16]float32 {
	res := [16]float32{}
	i := 0
	for col := 0; col < 4; col++ {
		for row := 0; row < 4; row++ {
			res[i] = m[row][col]
			i++
		}
	}
	return res
}

// TODO: write as an elem wise op?
// Add adds two Mat4s.
func (m Mat4) Add(o Mat4) Mat4 {
	for row := 0; row < 4; row++ {
		for col := 0; col < 4; col++ {
			m[row][col] += o[row][col]
		}
	}
	return m
}

// Sub subtracts a Mat4 from another.
func (m Mat4) Sub(o Mat4) Mat4 {
	for row := 0; row < 4; row++ {
		for col := 0; col < 4; col++ {
			m[row][col] -= o[row][col]
		}
	}
	return m
}

// Mul multiplies two Mat4s.
func (m Mat4) Mul(o Mat4) Mat4 {
	res := Mat4{}
	for row := 0; row < 4; row++ {
		for col := 0; col < 4; col++ {
			res[row][col] = dot4(m[row], o.Col(col))
		}
	}
	return res
}

// Scale4 returns a homogenous scaling matrix.
// See https://en.wikipedia.org/wiki/Scaling_(geometry)
func Scale4(x, y, z float32) Mat4 {
	return Mat4{
		{x, 0, 0, 0},
		{0, y, 0, 0},
		{0, 0, z, 0},
		{0, 0, 0, 1},
	}
}

// Translate4 returns a homogenous translation matrix.
// See https://en.wikipedia.org/wiki/Translation_(geometry)
func Translate4(x, y, z float32) Mat4 {
	return Mat4{
		{1, 0, 0, x},
		{0, 1, 0, y},
		{0, 0, 1, z},
		{0, 0, 0, 1},
	}
}

// Rotate4X returns a homogenous rotation matrix about the X-axis.
// See https://en.wikipedia.org/wiki/Rotation_matrix
func Rotate4X(angle float32) Mat4 {
	sin := Sin(angle)
	cos := Cos(angle)
	return Mat4{
		{1, 0, 0, 0},
		{0, cos, -sin, 0},
		{0, sin, cos, 0},
		{0, 0, 0, 1},
	}
}

// Rotate4Y returns a homogenous rotation matrix about the X-axis.
// See https://en.wikipedia.org/wiki/Rotation_matrix
func Rotate4Y(angle float32) Mat4 {
	sin := Sin(angle)
	cos := Cos(angle)
	return Mat4{
		{cos, 0, sin, 0},
		{0, 1, 0, 0},
		{-sin, 0, cos, 0},
		{0, 0, 0, 1},
	}
}

// Rotate4Z returns a homogenous rotation matrix about the X-axis.
// See https://en.wikipedia.org/wiki/Rotation_matrix
func Rotate4Z(angle float32) Mat4 {
	sin := Sin(angle)
	cos := Cos(angle)
	return Mat4{
		{cos, -sin, 0, 0},
		{sin, cos, 0, 0},
		{0, 0, 1, 0},
		{0, 0, 0, 1},
	}
}

func dot4(a, b [4]float32) float32 {
	var sum float32
	for i := range a {
		sum += a[i] * b[i]
	}
	return sum
}
