package physics

import (
	"math"
)

// TODO: move serialization out of here to reduce dep on orca eventually

// Vec2 is a basic 2D struct of float coordinates.
// It may also be used as a general 2D point, not just a true vector.
type Vec2 struct {
	X float32
	Y float32
}

// Vec3 is a basic 3D struct of float coordinates.
// It may also be used as a general 3D point, not just a true vector.
type Vec3 struct {
	X float32
	Y float32
	Z float32
}

// ZV2 is the zero vector
var ZV2 = Vec2{}

// Lerp performs linear interpolation
func Lerp(a, b, amt float32) float32 {
	return a + (b-a)*amt
}

// V is a convenience constructor for a Vec2
func V(x, y float32) Vec2 {
	return Vec2{X: x, Y: y}
}

// AsArray converts to an array
func (v Vec2) AsArray() [2]float32 {
	return [2]float32{v.X, v.Y}
}

// AsVec3 converts to a Vec3
func (v Vec2) AsVec3(z float32) Vec3 {
	return Vec3{v.X, v.Y, z}
}

// Add adds two Vec2s
func (v Vec2) Add(o Vec2) Vec2 {
	return Vec2{
		X: v.X + o.X,
		Y: v.Y + o.Y,
	}
}

// Sub subtracts two Vec2s
func (v Vec2) Sub(o Vec2) Vec2 {
	return Vec2{
		X: v.X - o.X,
		Y: v.Y - o.Y,
	}
}

// Scale multiplies a Vec2 by a scalar
func (v Vec2) Scale(s float32) Vec2 {
	return Vec2{
		X: v.X * s,
		Y: v.Y * s,
	}
}

// Dot calculates dot product of two Vec2s
func (v Vec2) Dot(o Vec2) float32 {
	return v.X*o.X + v.Y*o.Y
}

// ClampMax clamps Vec2 within max values
func (v Vec2) ClampMax(o Vec2) Vec2 {
	if v.X > o.X {
		v.X = o.X
	}
	if v.Y > o.Y {
		v.Y = o.Y
	}
	return v
}

// ClampMin clamps Vec2 within min values.
func (v Vec2) ClampMin(o Vec2) Vec2 {
	if v.X < o.X {
		v.X = o.X
	}
	if v.Y < o.Y {
		v.Y = o.Y
	}
	return v
}

// Clamp clamps a Vec2
func (v Vec2) Clamp(vmax, vmin Vec2) Vec2 {
	return v.ClampMax(vmax).ClampMin(vmin)
}

// Length provides length of a Vec2.
func (v Vec2) Length() float32 {
	return float32(math.Sqrt(float64(v.X*v.X) + float64(v.Y*v.Y)))
}

// Normalize returns norm of Vec2.
func (v Vec2) Normalize() Vec2 {
	if v.X == 0.0 && v.Y == 0.0 {
		return ZV2 // don't divide by zero!
	}
	ln := v.Length()
	s := 1.0 / ln
	n := v.Scale(s)
	return n
}

// Project projects a Vec v onto a unit Vec. It calculates a scalar distance
// along the unit Vec of the projection. We can the final Vec by scaling the
// unit Vec by this result.
func (v Vec2) Project(unit Vec2) Vec2 {
	d := v.Dot(unit)
	return unit.Scale(d)
}

// DistanceSq returns distance squared for comparison calculations
func (v Vec2) DistanceSq(o Vec2) float32 {
	x := o.X - v.X
	y := o.Y - v.Y
	return (x * x) + (y * y)
}

// Distance calcualtes distance between two points
func (v Vec2) Distance(o Vec2) float32 {
	sq := v.DistanceSq(o)
	return Sqrt(sq)
}

// IsZero returns bool whether zero vector or not
func (v Vec2) IsZero() bool {
	return v.X == 0 && v.Y == 0
}

// IsSame detects if two Vec2s are equivalent
func (v Vec2) IsSame(o Vec2) bool {
	return v.X == o.X && v.Y == o.Y
}

// Lerp performs linear interpolation between two Vec2s
func (v Vec2) Lerp(o Vec2, amt float32) Vec2 {
	return Vec2{
		X: Lerp(v.X, o.X, amt),
		Y: Lerp(v.Y, o.Y, amt),
	}
}

// LerpPrecise performs a more precise linear interpolation, but less efficient
func (v Vec2) LerpPrecise(o Vec2, amt float32) Vec2 {
	return o.Scale(amt).Add(v.Scale(1.0 - amt))
}

// AsArray converts to an array
func (v Vec3) AsArray() [3]float32 {
	return [3]float32{v.X, v.Y, v.Z}
}

// func writeVec2(buf *bytes.Buffer, v Vec2) error {
// 	err := om.WriteFloat32(buf, v.X)
// 	if err == nil {
// 		err = om.WriteFloat32(buf, v.Y)
// 	}
// 	return err
// }

// func writeVec3(buf *bytes.Buffer, v Vec3) error {
// 	err := om.WriteFloat32(buf, v.X)
// 	if err == nil {
// 		err = om.WriteFloat32(buf, v.Y)
// 	}
// 	if err == nil {
// 		err = om.WriteFloat32(buf, v.Z)
// 	}
// 	return err
// }

// func readVec2(rd *bytes.Reader, v *Vec2) (int, error) {
// 	// NOTE: must use pointer here
// 	err := om.ReadFloat32(rd, &v.X)
// 	if err == nil {
// 		err = om.ReadFloat32(rd, &v.Y)
// 	}
// 	if err != nil {
// 		return 0, err
// 	}
// 	return 8, nil
// }

// func readVec3(rd *bytes.Reader, v *Vec3) (int, error) {
// 	// NOTE: must use pointer here
// 	err := om.ReadFloat32(rd, &v.X)
// 	if err == nil {
// 		err = om.ReadFloat32(rd, &v.Y)
// 	}
// 	if err == nil {
// 		err = om.ReadFloat32(rd, &v.Z)
// 	}
// 	if err != nil {
// 		return 0, err
// 	}
// 	return 12, nil
// }
