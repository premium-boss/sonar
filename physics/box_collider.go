package physics

// BoxCollider is a Collider of Rect
type BoxCollider struct {
	Dim    Vec2
	Bounds Rect
}

// Update updates the bounds rect for a new position
// TODO: consider origin: right now upper left
func (b *BoxCollider) Update(pos Vec2) {
	b.Bounds.Min = pos
	b.Bounds.Max = pos.Add(b.Dim)
}

// Collision checks for a collision
func (b *BoxCollider) Collision(o Collider) Collision {
	switch other := o.(type) {
	case *BoxCollider:
		return collisionBoxBox(b, other)
	case *CircleCollider:
		return collisionBoxCircle(b, other)
	default:
		panic("unexpected Collider implementation: maybe one day...")
	}
}

// Contains returns whether a point is in the Collider area
func (b *BoxCollider) Contains(point Vec2) bool {
	return b.Bounds.Contains(point)
}
