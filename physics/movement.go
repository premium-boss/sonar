package physics

// TODO: move serialization out of here to remove dep on orca

// Move2 is a struct for all kinematic info on a player.
type Move2 struct {
	Pos Vec2
	Vel Vec2
	Acc Vec2
}

// Move3 is a struct for all kinematic info on a player.
type Move3 struct {
	Pos Vec3
	Vel Vec3
	Acc Vec3
}

// Lerp performs linear interpolation between two Move2s
func (moo *Move2) Lerp(maa *Move2, amt float32) Move2 {
	return Move2{
		Pos: moo.Pos.Lerp(maa.Pos, amt),
		Vel: moo.Vel.Lerp(maa.Vel, amt),
		Acc: moo.Acc.Lerp(maa.Acc, amt),
	}
}

// Integrate performs a physics update for a Move2.
// NOTE: we are using Semi-Implicit Euler, which should perform better
// under variable acceleration. So yes, the velocity update comes first.
// NOTE: not super useful without collisions, and if we aren't using accel...
func (moo *Move2) Integrate(dt float32) {
	velUp := moo.Acc.Scale(dt)
	moo.Vel = moo.Vel.Add(velUp)
	posUp := moo.Vel.Scale(dt)
	moo.Pos = moo.Pos.Add(posUp)
}

// IsSame determines if two Move2s are equivalent
func (moo *Move2) IsSame(oth *Move2) bool {
	return moo.Pos.IsSame(oth.Pos) &&
		moo.Vel.IsSame(oth.Vel) &&
		moo.Acc.IsSame(oth.Acc)
}

// // MarshalBinary serializes Move2 into bytes
// func (moo *Move2) MarshalBinary() ([]byte, error) {
// 	buf := new(bytes.Buffer)
// 	err := writeVec2(buf, moo.Pos)
// 	if err == nil {
// 		err = writeVec2(buf, moo.Vel)
// 	}
// 	if err == nil {
// 		err = writeVec2(buf, moo.Acc)
// 	}
// 	if err != nil {
// 		return nil, err
// 	}
// 	return buf.Bytes(), nil
// }

// // UnmarshalBinary reads a Move2 from bytes
// func (moo *Move2) UnmarshalBinary(data []byte) (int, error) {
// 	r := bytes.NewReader(data)
// 	n, err := readVec2(r, &moo.Pos)
// 	if err != nil {
// 		return 0, err
// 	}
// 	nv, err := readVec2(r, &moo.Vel)
// 	if err != nil {
// 		return 0, err
// 	}
// 	n += nv
// 	na, err := readVec2(r, &moo.Acc)
// 	if err != nil {
// 		return 0, err
// 	}
// 	n += na
// 	return n, nil
// }

// // MarshalBinary serializes Move3 into bytes
// func (moo *Move3) MarshalBinary() ([]byte, error) {
// 	buf := new(bytes.Buffer)
// 	err := writeVec3(buf, moo.Pos)
// 	if err == nil {
// 		err = writeVec3(buf, moo.Vel)
// 	}
// 	if err == nil {
// 		err = writeVec3(buf, moo.Acc)
// 	}
// 	if err != nil {
// 		return nil, err
// 	}
// 	return buf.Bytes(), nil
// }

// // UnmarshalBinary reads a Move3 from bytes
// func (moo *Move3) UnmarshalBinary(data []byte) (int, error) {
// 	r := bytes.NewReader(data)
// 	n, err := readVec3(r, &moo.Pos)
// 	if err != nil {
// 		return 0, err
// 	}
// 	nv, err := readVec3(r, &moo.Vel)
// 	if err != nil {
// 		return 0, err
// 	}
// 	n += nv
// 	na, err := readVec3(r, &moo.Acc)
// 	if err != nil {
// 		return 0, err
// 	}
// 	n += na
// 	return n, nil
// }
