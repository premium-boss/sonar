package physics

import (
	"testing"
)

type pointRectTest struct {
	max      Vec2
	min      Vec2
	pt       Vec2
	expected Vec2
}

func TestIntersects(t *testing.T) {
	tests := []struct {
		a        Rect
		b        Rect
		expected bool
	}{
		{R(V(10, 10), V(0, 0)), R(V(2, 3), V(0, 3)), true},
		{R(V(10, 10), V(0, 0)), R(V(3, 2), V(3, 0)), true},
		{R(V(10, 10), V(0, 0)), R(V(6, 2), V(6, 0)), true},
		{R(V(10, 10), V(0, 0)), R(V(30, 30), V(20, 20)), false},
		{R(V(10, 10), V(0, 0)), R(V(20, 20), V(10, 10)), false},
		{R(V(10, 10), V(0, 0)), R(V(-5, -3), V(0, 0)), false},
	}
	for i, tt := range tests {
		act := tt.a.Intersects(tt.b)
		if act != tt.expected {
			t.Errorf("test: %d, expected: %v, got: %v", i, tt.expected, act)
		}
	}
}

func TestClosestPointInside(t *testing.T) {
	tests := []pointRectTest{
		{V(10, 10), V(0, 0), V(2, 3), V(0, 3)},
		{V(10, 10), V(0, 0), V(3, 2), V(3, 0)},
		{V(10, 10), V(0, 0), V(6, 2), V(6, 0)},
		{V(10, 10), V(0, 0), V(5, 7), V(5, 10)},
		{V(10, 10), V(0, 0), V(5, 5), V(10, 5)},
		{V(10, 10), V(0, 0), V(7, 7), V(10, 7)},
	}
	for i, tt := range tests {
		r := R(tt.max, tt.min)
		cp := r.ClosestPoint(tt.pt)
		if !cp.IsSame(tt.expected) {
			t.Errorf("test: %d, expected: %v, got: %v", i, tt.expected, cp)
		}
	}
}

func TestClosestPointOutside(t *testing.T) {
	tests := []pointRectTest{
		{V(10, 10), V(0, 0), V(11, 12), V(10, 10)},
		{V(10, 10), V(0, 0), V(15, 12), V(10, 10)},
		{V(10, 10), V(0, 0), V(8, 12), V(8, 10)},
		{V(10, 10), V(0, 0), V(12, 8), V(10, 8)},
		{V(10, 10), V(0, 0), V(10, 10), V(10, 10)}, // are these outside?
		{V(10, 10), V(0, 0), V(0, 0), V(0, 0)},
		{V(10, 10), V(0, 0), V(-1, -1), V(0, 0)},
	}
	for i, tt := range tests {
		r := R(tt.max, tt.min)
		cp := r.ClosestPoint(tt.pt)
		if !cp.IsSame(tt.expected) {
			t.Errorf("test: %d, expected: %v, got: %v", i, tt.expected, cp)
		}
	}
}
