package physics

// Rect is a struct for rectangles.
type Rect struct {
	Min Vec2
	Max Vec2
}

// ZR is the zero Rect.
var ZR = Rect{}

// R is a convenience constructor for a Rect.
func R(max, min Vec2) Rect {
	return Rect{Max: max, Min: min}
}

// Width returns the width of a Rect.
func (r Rect) Width() float32 {
	return r.Max.X - r.Min.X
}

// Height returns the height of a Rect.
func (r Rect) Height() float32 {
	return r.Max.Y - r.Min.Y
}

// Intersects returns a bool if two Rects intersect.
func (r Rect) Intersects(s Rect) bool {
	return r.Min.X < s.Max.X &&
		r.Max.X > s.Min.X &&
		r.Min.Y < s.Max.Y &&
		r.Max.Y > s.Min.Y
}

// Intersect returns the overlapping Rect among two Rects, if any. Otherwise,
// returns the empty / zero rect ZR.
func (r Rect) Intersect(s Rect) Rect {
	if r.Min.X < s.Min.X {
		r.Min.X = s.Min.X
	}
	if r.Min.Y < s.Min.Y {
		r.Min.Y = s.Min.Y
	}
	if r.Max.X > s.Max.X {
		r.Max.X = s.Max.X
	}
	if r.Max.Y > s.Max.Y {
		r.Max.Y = s.Max.Y
	}
	if r.IsEmpty() {
		return ZR
	}
	return r
}

// MTV returns the minimum translation vector of two Rects
func (r Rect) MTV(s Rect) Vec2 {
	v := ZV2
	left := s.Min.X - r.Max.X
	right := s.Max.X - r.Min.X
	top := s.Min.Y - r.Max.Y
	btm := s.Max.Y - r.Min.Y
	if left > 0 || right < 0 || top > 0 || btm < 0 {
		return v // no intersection
	}
	if Abs(left) < right {
		v.X = left
	} else {
		v.X = right
	}
	if Abs(top) < btm {
		v.Y = top
	} else {
		v.Y = btm
	}
	// chose closer axis
	if Abs(v.X) < Abs(v.Y) {
		v.Y = 0
	} else {
		v.X = 0
	}
	return v
}

// Contains determines if a point is in the Rect
func (r Rect) Contains(v Vec2) bool {
	return v.X < r.Max.X &&
		v.X > r.Min.X &&
		v.Y < r.Max.Y &&
		v.Y > r.Min.Y
}

// IsEmpty determines if a Rect is empty
func (r Rect) IsEmpty() bool {
	return r.Min.X >= r.Max.X || r.Min.Y >= r.Max.Y
}

// ClosestPoint returns closest point on border to another point
func (r Rect) ClosestPoint(pt Vec2) Vec2 {
	if r.Contains(pt) {
		return r.closestPointInside(pt)
	}
	return r.closestPointOutside(pt)
}

func (r Rect) closestPointInside(pt Vec2) Vec2 {
	top := r.Max.Y - pt.Y
	bottom := pt.Y - r.Min.Y
	clo := ZV2
	var dy float32
	if top > bottom {
		clo.Y = r.Min.Y // closer to bottom
		dy = bottom
	} else {
		clo.Y = r.Max.Y // closer to top
		dy = top
	}

	right := r.Max.X - pt.X
	left := pt.X - r.Min.X
	var dx float32
	if right > left {
		clo.X = r.Min.X // closer to left
		dx = left
	} else {
		clo.X = r.Max.X // closer to right
		dx = right
	}

	// adjust: clamp to the closer edge, favoring x sides if a tie
	if dx <= dy {
		clo.Y = pt.Y // closer to side than top / bottom
	} else {
		clo.X = pt.X // closer to top / bottom than side
	}

	return clo
}

func (r Rect) closestPointOutside(pt Vec2) Vec2 {
	return pt.Clamp(r.Max, r.Min)
}
