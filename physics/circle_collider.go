package physics

// CircleCollider is a Collider of Circle
type CircleCollider struct {
	Circle Circle
}

// Update updates the circle for a new position
func (c *CircleCollider) Update(pos Vec2) {
	c.Circle.Center = pos
}

// Collision checks for a collision
func (c *CircleCollider) Collision(o Collider) Collision {
	switch other := o.(type) {
	case *BoxCollider:
		return collisionBoxCircle(other, c)
	case *CircleCollider:
		return collisionCircleCircle(c, other)
	default:
		panic("collider with bad ShapeID")
	}
}

// Contains returns whether a point is in the Collider area
func (c *CircleCollider) Contains(point Vec2) bool {
	return c.Circle.Contains(point)
}
