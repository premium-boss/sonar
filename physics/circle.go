package physics

// Circle represents a Circle shape
type Circle struct {
	Center Vec2
	Radius float32
}

// NewCircle creates a new Circle
func NewCircle(center Vec2, radius float32) Circle {
	return Circle{Center: center, Radius: radius}
}

// Contains determines if a point is in the Circle
func (c Circle) Contains(v Vec2) bool {
	d2 := v.DistanceSq(c.Center)
	r2 := c.Radius * c.Radius
	return d2 < r2
}

// TODO: collision(): we might want to actually find the point of collision, in
// case overlap can cross center. but if steps aren't big enough, finding the
// overlap should suffice (you could make the same arg for a rect)
