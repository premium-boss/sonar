# Sonar

A go game engine and ECS (entity-component-system) framework.

## Getting started

### Linux

You'll need the following dependencies to build the engine on linux:

- libgl1-mesa-dev
- xorg-dev
- libopenal-dev


## Building the examples

Install git-lfs for assets in the examples directory. All png, wav, and ttf files are stored in lfs by default.
If you have an old version of the repo, please **clone** it again in a fresh location, do not simply pull.


## Credits

Art in the TopDown example
- Sprites in characters.png by Buch: https://opengameart.org/users/buch
- Art from hyptosis_tile-art-batch-1.png by Hyptosis and Zabin under CC 3.0: https://opengameart.org/content/lots-of-free-2d-tiles-and-sprites-by-hyptosis

https://creativecommons.org/licenses/by/3.0/legalcode
