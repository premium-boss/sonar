package tiled

// Manager preserves content for TileMaps and TileSets.
// This is something we can pass to the registry prior to loading.
type Manager struct {
	TileMap  *TileMap
	tileSets map[string]TileSet // TileSets by filename
}

// NewManager creates a new tile Manager
func NewManager() *Manager {
	m := Manager{
		tileSets: make(map[string]TileSet),
	}
	return &m
}

// Clear clears data from the Manager
func (m *Manager) Clear() {
	m.TileMap = nil
	for key := range m.tileSets {
		delete(m.tileSets, key)
	}
}

// TileSet returns the tileset by the source name (the .tsx..., not ideal)
// TODO: pointer!
func (m *Manager) TileSet(source string) (TileSet, bool) {
	ts, ok := m.tileSets[source]
	return ts, ok
}

// AddTileSet adds a TileSet to the manager, indexed by source name (.tsx)
// TODO: pointer!
func (m *Manager) AddTileSet(ts TileSet, source string) {
	m.tileSets[source] = ts
}
