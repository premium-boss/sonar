package tiled

// Layer type constants
const (
	LayerTypeTile   = "tilelayer"
	LayerTypeObject = "objectgroup"
)

//Layer is a TileMap layer with indexes of tiles.
type Layer struct {
	ID     int32  `json:"id"`
	Type   string `json:"type"`
	Name   string `json:"name"`
	Width  int32  `json:"width"`
	Height int32  `json:"height"`
	X      int32  `json:"x"`
	Y      int32  `json:"y"`
	// TODO: opacity? are x and y always ints?
	// TODO: do we want the sub-layer draworder field?

	Data    []int64  `json:"data"`    // indexes of a tile layer, can be longs with bit flags
	Objects []Object `json:"objects"` // object of an object layer

}
