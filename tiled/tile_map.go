package tiled

import (
	"encoding/json"
	"fmt"
	"sort"

	"gitlab.com/premium-boss/sonar/ecs"
	mov "gitlab.com/premium-boss/sonar/movement"
	phy "gitlab.com/premium-boss/sonar/physics"
	ren "gitlab.com/premium-boss/sonar/render"
)

// RenderOrder constants
const (
	RenderOrderRightUp   = "right-up"
	RenderOrderRightDown = "right-down"
	RenderOrderLeftUp    = "left-up"
	RenderOrderLeftDown  = "left-down"
)

// Flip and Rotation constants
const (
	FlippedHorizontally = 0x80000000
	FlippedVertically   = 0x40000000
	FlippedDiagonally   = 0x20000000
)

// SetIndicator contains source file spritesheet for the TileMap.
// FirstGID refers to the global id that maps to the start of the TileSet indicated by Source.
type SetIndicator struct {
	FirstGID int64  `json:"firstgid"`
	Source   string `json:"source"`
}

// TileMap loads JSON representation of Tiled tilemap file.
// It contains a set of indexes and render info into a spritesheet.
// We only load what we care about.
type TileMap struct {
	Width       int32          `json:"width"`
	Height      int32          `json:"height"`
	Version     float32        `json:"version"`
	Orientation string         `json:"orientation"`
	RenderOrder string         `json:"renderorder"`
	TileWidth   int32          `json:"tilewidth"`
	Sets        []SetIndicator `json:"tilesets"`
	Layers      []Layer        `json:"layers"`
}

func (tm *TileMap) tileCount() int {
	ct := 0
	for i := range tm.Layers {
		ct += len(tm.Layers[i].Data)
	}
	return ct
}

// FindSetIndicator gets the TileSetIndicator given a parsed ID for a tile index.
// sort.Search will use binary Search to return smallest index for which this
// condition is true. For GIDs, we want the index before the smallest that is > our value.
// This will retrieve the next GID index, so we can just subtract 1.
// Search returns the length of the set (n) if not found, in which case we can still use
// the last index, which is n - 1.
func (tm *TileMap) FindSetIndicator(id int64) SetIndicator {
	n := len(tm.Sets)

	// fastpath
	if n == 1 {
		return tm.Sets[0]
	}

	// binary search
	fn := func(i int) bool { return tm.Sets[i].FirstGID > id }
	j := sort.Search(n, fn) - 1
	if j < 0 {
		j = 0
	}
	return tm.Sets[j]
}

// TileMapComponent is the ecs component for the TileMap.
// It is used to load and retrieve the sprites.
// The Name can be used to fetch from a content manager.
// TODO: we want layers here? maybe instead of sprite interface retuning a slice,
// we should just pass the renderer in and let it add accordingly?
// Then we can control draw order in a sense...
type TileMapComponent struct {
	Name    string   `json:"name"`
	Map     *TileMap `json:"-"`
	sprites []ren.Sprite
}

// Init inits component with Entity
func (c *TileMapComponent) Init(ent ecs.Entity) {
}

// OnUnregister hook
func (c *TileMapComponent) OnUnregister(ent ecs.Entity) {}

// Sprite stub for renderable interface
func (c *TileMapComponent) Sprite() (ren.Sprite, bool) {
	return ren.Sprite{}, false
}

// Sprites returns sprites for renderable interface
func (c *TileMapComponent) Sprites() ([]ren.Sprite, bool) {
	return c.sprites, true
}

// TODO: revert to pointers, ditch this...

// UpdateSprite stub for Renderable interface
func (c *TileMapComponent) UpdateSprite(sprite ren.Sprite) {
}

// UpdateAt updates a Sprite at index i
func (c *TileMapComponent) UpdateAt(i int, sprite ren.Sprite) {
	c.sprites[i] = sprite
}

type tileSetID struct {
	firstGID int64
	name     string
}

type parsedGID struct {
	id                  int64
	isFlippedHorizontal bool
	isFlippedVertical   bool
	isFlippedDiagonal   bool
}

func parseTileFlags(gID int64) parsedGID {
	p := parsedGID{
		isFlippedHorizontal: gID&FlippedHorizontally != 0,
		isFlippedVertical:   gID&FlippedVertically != 0,
		isFlippedDiagonal:   gID&FlippedDiagonally != 0,
		id:                  gID &^ (FlippedHorizontally | FlippedVertically | FlippedDiagonally),
	}
	return p
}

// TODO: we want to preserve layers and order here somehow,
// not just return all sprites and defer to the texture order.
// TODO: consider adding props to tiles so we can reference them from scene files
// by id, still layout characters, etc.
func (c *TileMapComponent) load(
	mgr *Manager,
	tex ren.TextureManager,
) error {
	c.Map = mgr.TileMap
	c.sprites = make([]ren.Sprite, 0, c.Map.tileCount())
	for _, layer := range c.Map.Layers {
		var err error
		switch layer.Type {
		case LayerTypeTile:
			err = c.parseTileLayer(mgr, tex, layer)
		case LayerTypeObject:
			// do nothing on object layers for now...
			continue
		default:
			err = fmt.Errorf("TileMap: unhandled layer type: %s", layer.Type)
		}
		if err != nil {
			return err
		}
	}

	// TODO: use the RenderOrder and bounds properly to calculate position and setup the sprite
	return nil
}

// buildSprite creates a Sprite from a global ID.
// TODO: might not even need texture in here if we move the offset logic
// to the shader!
// we need two modes for the pos...
func buildSprite(m *Manager, tex ren.TextureManager, gID int64) (ren.Sprite, TileSet, error) {
	parsed := parseTileFlags(gID)
	indicator := m.TileMap.FindSetIndicator(parsed.id)
	set, ok := m.TileSet(indicator.Source)
	if !ok {
		return ren.Sprite{}, TileSet{}, fmt.Errorf("TileMap: could not find TileSet with file name: %s", indicator.Source)
	}

	texture, ok := tex.GetTexture(set.TextureName)
	if !ok {
		return ren.Sprite{}, TileSet{}, fmt.Errorf("TileMap: could not find Texture with name: %s", set.TextureName)
	}

	// this should already take care of the -1, as firstGID is 1-indexed
	iTex := int32(parsed.id - indicator.FirstGID)
	offset := calcOffset(set, iTex)
	dim := phy.V(float32(set.TileWidth), float32(set.TileHeight))
	spr := ren.NewSpriteOffset(dim, offset, texture)

	// deal w/ orientation flags
	if parsed.isFlippedHorizontal {
		// TODO: seems awkward, have to do in frame system too...
		spr = spr.Scaled(phy.V(-1.0, 1.0)).MovedOffset(phy.V(spr.Width, 0))
	}
	if parsed.isFlippedVertical {
		spr = spr.Scaled(phy.V(1.0, -1.0))
	}
	if parsed.isFlippedDiagonal {
		spr = spr.Scaled(phy.V(-1.0, -1.0)) // TODO: check this...
	}
	return spr, set, nil
}

// parseTileLayer parses a tile layer.
// We expect layer.Data to contain a series of longs representing tile indexes w/ flags.
// The goal here is to derive the actual index into the proper spritesheet.
// We cache the mapping of id to tileSet and parsed id.
// If not found, uses binary search to select the right tileSet.
// TODO: need a content manager, lookup by filename, the string trims here are suspect...
// TODO: the binary search and cache might be more expensive than just looping sets, esp. if we have only 1...
func (c *TileMapComponent) parseTileLayer(
	mgr *Manager,
	tex ren.TextureManager,
	layer Layer,
) error {
	for i, gID := range layer.Data {
		spr, set, err := buildSprite(mgr, tex, gID)
		if err != nil {
			return err
		}

		// TODO: this is a bug / source of confusion waiting to happen,
		// but model matrix doesn't update after scaling, only on movement
		// we should probably make it either explicit, or implicit for both
		// MovedOffset so that we don't undo movement due to flipping...
		// need to split it up still...
		pos := calcPosition(int32(i), layer, set)
		spr = spr.MovedOffset(pos)
		c.sprites = append(c.sprites, spr)
	}
	return nil
}

// calcPosition calculates sprite position given index i into the TileMap Layer data array
func calcPosition(i int32, layer Layer, set TileSet) phy.Vec2 {
	x := (i%layer.Width)*set.TileWidth + layer.X
	y := (i/layer.Width)*set.TileHeight + layer.Y
	pos := phy.V(float32(x), float32(y))
	return pos
}

// calcOffset calculates the texture offset given index i into the TileSet
func calcOffset(ts TileSet, i int32) phy.Vec2 {
	offX := float32((i % ts.Columns) * ts.TileWidth)
	offY := float32((i / ts.Columns) * ts.TileHeight)
	offset := phy.V(offX, offY)
	return offset
}

// findObjectForEntity finds the external object for an entity
// TODO: move...
func (tm *TileMap) findObjectForEntity(name string) (Object, error) {
	// lookup the object by name
	// if not found, error
	for _, layer := range tm.Layers {
		if layer.Type != LayerTypeObject {
			continue
		}
		for _, obj := range layer.Objects {
			if obj.Name == name {
				return obj, nil
			}
		}
	}
	return Object{}, fmt.Errorf("external data failed, can't find object with entity name: %s", name)
}

// JSONTileMapComponentFactory returns a factory function for loading TileMapComponents from JSON
func JSONTileMapComponentFactory(
	mgr *Manager,
	texMgr ren.TextureManager,
) ecs.JSONComponentFactory {
	return func(ent ecs.Entity, desc ecs.JSONComponentDescriptor) (ecs.Component, error) {
		tmc := TileMapComponent{}
		err := json.Unmarshal(desc.Data, &tmc)
		if err != nil {
			return nil, err
		}
		// NOTE: we may need to store or otherwise have continued access to TileSets later...
		err = tmc.load(mgr, texMgr)
		if err != nil {
			return nil, err
		}
		tmc.Init(ent)
		return &tmc, nil
	}
}

// LoadExternalComponents loads external components
// from a TileMap object layer
// TODO: can we make this fit better with the registration system? or otherwise data-define?
func LoadExternalComponents(
	mgr *Manager,
	texMgr ren.TextureManager,
	ent ecs.Entity,
) ([]ecs.Component, error) {
	obj, err := mgr.TileMap.findObjectForEntity(ent.Name)
	if err != nil {
		return nil, err
	}
	// TODO: rely on manual input of type? or use the fields?
	if obj.Point {
		return applyExternalPoint()
	}
	if obj.Text.Text != "" {
		return applyExternalText()
	}
	return applyExternalObject(mgr, texMgr, ent, obj)
}

func applyExternalObject(
	mgr *Manager,
	texMgr ren.TextureManager,
	ent ecs.Entity,
	obj Object,
) ([]ecs.Component, error) {
	// we expect a position and a sprite component from the obj
	pos := phy.V(obj.X, obj.Y)
	pc := mov.NewPositionComponent(pos)
	pc.Init(ent)

	// setting position here is a little shaky, but slightly
	// better now that it's in the same pkg
	spr, _, err := buildSprite(mgr, texMgr, obj.GID)
	if err != nil {
		return nil, err
	}

	sc := ren.NewSpriteComponent(spr)
	sc.Init(ent)
	sc.Instance = spr.MovedOffset(pos) // HACK: fix this!

	comps := []ecs.Component{pc, sc}
	return comps, nil
}

func applyExternalPoint() ([]ecs.Component, error) {
	// point can be used to move the entity
	// TODO:
	return nil, nil
}

func applyExternalText() ([]ecs.Component, error) {
	// text can specify text and position
	// TODO:
	return nil, nil
}
