package tiled

// Object of an object layer contains additional data for game directives.
// The object can be based on a Tile, Point, Rectangle, Polygon, etc.
type Object struct {
	ID       int     `json:"id"`
	Name     string  `json:"name"`
	Type     string  `json:"type"`
	GID      int64   `json:"gid"`
	Point    bool    `json:"point"`
	Text     Text    `json:"text"`
	Height   float32 `json:"height"`
	Width    float32 `json:"width"`
	Rotation int     `json:"rotation"` // TODO: check type
	Visible  bool    `json:"visible"`
	X        float32 `json:"x"`
	Y        float32 `json:"y"`
}

// Text object
type Text struct {
	Text string `json:"text"`
	Wrap bool   `json:"wrap"`
}
