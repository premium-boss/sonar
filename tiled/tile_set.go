package tiled

// TileSet describes how to access tiles for a spritesheet image
type TileSet struct {
	Name        string `json:"name"`
	Image       string `json:"image"`
	TextureName string `json:"-"`           // cache derived texture name
	ImageWidth  int32  `json:"imagewidth"`  // width in pixels
	ImageHeight int32  `json:"imageheight"` // height in pixels
	TileWidth   int32  `json:"tilewidth"`   // width of tile in pixels
	TileHeight  int32  `json:"tileheight"`  // height of tile in pixels
	Columns     int32  `json:"columns"`     // num columns, or width in tiles
	TileCount   int32  `json:"tilecount"`
}
