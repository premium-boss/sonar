package tiled

import (
	"gitlab.com/premium-boss/sonar/ecs"
	ren "gitlab.com/premium-boss/sonar/render"
)

// RegisterRenderables registers renderables from the environment package with the render registry
func RegisterRenderables(reg ren.RenderableRegistry) {
	reg["tiled.TileMapComponent"] = &TileMapComponent{}
}

// RegisterComponents registers all loader factories in pacakge
func RegisterComponents(
	reg map[string]ecs.JSONComponentFactory,
	texMgr ren.TextureManager,
	mgr *Manager,
) {
	reg["tiled.TileMapComponent"] = JSONTileMapComponentFactory(mgr, texMgr)
}
