package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/premium-boss/sonar/ecs"
	ren "gitlab.com/premium-boss/sonar/render"
	txt "gitlab.com/premium-boss/sonar/text"
	"gitlab.com/premium-boss/sonar/tiled"
)

// Deals with loading and unloading scenes, transitions, etc.

const (
	assets   = "assets"
	textSize = 12
	dpi      = 72
)

func loadSceneFile(fileName string) (ecs.JSONSceneDescriptor2D, error) {
	sd := ecs.JSONSceneDescriptor2D{}
	path := filepath.Join(assets, fileName)
	file, err := os.Open(path)
	if err != nil {
		return sd, err
	}
	dec := json.NewDecoder(file)
	err = dec.Decode(&sd)
	file.Close() // close regardless of err
	return sd, err
}

func loadScene(sceneFile string, systems map[string]ecs.JSONSystemFactory,
	components map[string]ecs.JSONComponentFactory, ctx Context,
) *ecs.Scene {
	if sceneFile == "" {
		log.Fatalln("main: transition declared but no scene name provided")
	}
	sd, err := loadSceneFile(sceneFile)
	check("loadScene: error decoding scene file:", err)

	// create scene and systems
	// TODO: this could be done first if not for world bounds. should do and set after
	// TODO: these should be somewhat dynamic!
	scene := ecs.NewScene(ecs.NewEntityManager())

	// load systems
	for _, desc := range sd.Systems {
		for _, name := range desc.Systems {
			factory := systems[name] // error if unknown
			system, err := factory()
			check("scene: failed to load system: "+name, err)
			scene.RegisterSystem(ecs.Phase(desc.Phase), system)
		}
	}

	// load textures
	for _, td := range sd.Textures {
		loadTexture(ctx.TextureManager, td.Name, td.FileName)
	}

	// load fonts
	for _, fd := range sd.Fonts {
		atl := loadAtlas(fd.Name, fd.FileName, ctx.TextureManager)
		ctx.Atlases[fd.Name] = atl
	}

	// load tilesets and provide for context
	// TODO: this is not ideal...
	for _, tsd := range sd.TileSets {
		ts := loadTileSet(tsd.FileName)
		ctx.TileManager.AddTileSet(ts, tsd.Name+".tsx")
	}

	// load tilemap and provide for context
	if sd.TileMap.FileName != "" {
		ctx.TileManager.TileMap = loadTileMap(sd.TileMap.FileName)
	}

	// load sounds
	// NOTE: context map is shared with the system
	for _, ad := range sd.Sounds {
		songPath := filepath.Join(assets, ad.FileName)
		sound, err := ctx.AudioManager.Load(songPath, true)
		check("failed to load song", err)
		ctx.Sounds[ad.Name] = sound
	}

	// load scene entity and components
	scene.Entity = loadEntity(scene, ctx, sd.Entity, components)

	// load entities and components from registered factory functions
	for _, ed := range sd.Entities {
		// TODO: merge w/ objects from TileMap?
		loadEntity(scene, ctx, ed, components)
	}
	return scene
}

func loadEntity(
	scene *ecs.Scene,
	ctx Context,
	ed ecs.JSONEntityDescriptor,
	components map[string]ecs.JSONComponentFactory,
) ecs.Entity {
	ent := scene.EntityManager.NewEntity(ed.Name)
	// check for external data we must merge...
	// TODO: build into registration system better...
	// TODO: the ordering here is FRAGILE! but render needs Sprite already loaded...
	if ed.HasExternalData {
		comps, err := tiled.LoadExternalComponents(ctx.TileManager, ctx.TextureManager, ent)
		check("error loading external tiled components", err)
		for _, cmp := range comps {
			scene.EntityManager.AddComponent(ent, cmp)
		}
	}
	for _, cd := range ed.Components {
		load := components[cd.Type]
		cmp, err := load(ent, cd)
		check("error loading component:"+cd.Type, err)
		scene.EntityManager.AddComponent(ent, cmp)
	}

	return ent
}

// TODO: move these out into respective packages too? has path logic that they don't care about though

func loadTexture(texMgr ren.TextureManager, nm, fileNm string) ren.Texture {
	path := filepath.Join(assets, fileNm)
	tex, err := texMgr.NewTextureFromFile(path, nm)
	check("error creating texture:"+path, err)
	return tex
}

func loadAtlas(name, fileName string, texMgr ren.TextureManager) *txt.Atlas {
	path := filepath.Join(assets, fileName)
	atlas, rgba, err := txt.NewAtlas(path, name, textSize, dpi)
	check("error creating font:"+path, err)
	fontTex, err := texMgr.NewTextureFromRGBA(rgba, name)
	check("error creating font texture:"+name, err)
	atlas.Texture = fontTex
	return atlas // TODO: must be tracked, either in scene or director or system
}

func loadTileMap(fileName string) *tiled.TileMap {
	path := filepath.Join(assets, fileName)
	bytes, err := ioutil.ReadFile(path)
	check("failed to load tile map file", err)
	tm := tiled.TileMap{}
	err = json.Unmarshal(bytes, &tm)
	check("failed to unmarshal tile map:", err)
	return &tm
}

// TODO: pointer?
func loadTileSet(fileName string) tiled.TileSet {
	path := filepath.Join(assets, fileName)
	bytes, err := ioutil.ReadFile(path)
	check("failed to load tile set file", err)
	ts := tiled.TileSet{}
	err = json.Unmarshal(bytes, &ts)
	check("failed to unmarshal tile set:", err)
	// TODO: make pointer and just call this?
	//ts.Init()
	ts.TextureName = strings.TrimSuffix(ts.Image, ".png")
	return ts
}
