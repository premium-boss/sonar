package main

import (
	aud "gitlab.com/premium-boss/sonar/audio"
	ipt "gitlab.com/premium-boss/sonar/input"
	phy "gitlab.com/premium-boss/sonar/physics"
	ren "gitlab.com/premium-boss/sonar/render"
	txt "gitlab.com/premium-boss/sonar/text"
	"gitlab.com/premium-boss/sonar/tiled"
)

// Context maintains all managers, window, etc.
// TODO: separate out interfaces? register globally? loose bad?
// TODO: rename: we want a better content manager anyway, but Context
// has a specific meaning in Go, and this isn't quite it...
// This is more of a mix of Scene / Game specifics and Content manangement
// We should have structures accordingly...
type Context struct {
	Program        uint32
	InputManager   ipt.Manager
	TextureManager ren.TextureManager
	AudioManager   aud.Manager
	Atlases        map[string]*txt.Atlas
	TileManager    *tiled.Manager
	Sounds         map[string]*aud.Sound
	SpatialHash    *phy.SpatialHash
}
