package menu

// Menu

// TODO: let's keep this as simple as possible - we don't need to anticipate everything now
// We can always rewrite to have multiple text components, activate / deactivate / transfer the input
// component, etc.

import (
	"encoding/json"
	"fmt"

	ecs "gitlab.com/premium-boss/sonar/ecs"
)

// Component is the menu
type Component struct {
	IsActive   bool
	SelectType string
	SelectData json.RawMessage
	Select     interface{} `json:"-"` // TODO: if we don't like forcing json here, could include all sub types...
}

// Transition is a simple menu item that can transition scenes or menus when selected
type Transition struct {
	Type   string // either "scene" or "menu"
	Target string
}

// Init inits component with Entity
func (c *Component) Init(ent ecs.Entity) {}

// OnUnregister hook
func (c *Component) OnUnregister(ent ecs.Entity) {}

// JSONComponentFactory returns factory function for loading the component
func JSONComponentFactory() ecs.JSONComponentFactory {
	return func(ent ecs.Entity, desc ecs.JSONComponentDescriptor) (ecs.Component, error) {
		c := Component{}
		err := json.Unmarshal(desc.Data, &c)
		if err != nil {
			return nil, err
		}
		switch c.SelectType {
		case "transition":
			t := Transition{}
			err = json.Unmarshal(c.SelectData, &t)
			if err != nil {
				break
			}
			c.Select = t
			c.SelectData = nil
		default:
			err = fmt.Errorf("menu: unknown SelectType: %s", c.SelectType)
			break
		}
		if err != nil {
			return nil, err
		}
		c.Init(ent)
		return &c, nil
	}
}
