package menu

// A simple menu-click system. If the item is selected and enter is pressed,
// we want to advance.

import (
	"log"
	"reflect"

	cam "gitlab.com/premium-boss/sonar/camera"
	"gitlab.com/premium-boss/sonar/ecs"
	"gitlab.com/premium-boss/sonar/input"
	ipt "gitlab.com/premium-boss/sonar/input"
	mov "gitlab.com/premium-boss/sonar/movement"
	phy "gitlab.com/premium-boss/sonar/physics"
)

const (
	selectionBuffer float32 = 20.0 // on either side
)

var selectionDim = phy.V(selectionBuffer, selectionBuffer)
var selectionOffset = selectionDim.Scale(-0.5)

// System for managing menu
type System struct {
	ecs.GameSystem
	TCam  reflect.Type
	TIpt  reflect.Type
	TCol  reflect.Type
	TMenu reflect.Type
}

// NewSystem creates a new System
func NewSystem() *System {
	sys := System{
		TCam:  reflect.TypeOf(&cam.Component{}),
		TIpt:  reflect.TypeOf(&input.Component{}),
		TCol:  reflect.TypeOf(&mov.ColliderComponent{}),
		TMenu: reflect.TypeOf(&Component{}),
	}
	sys.SetConstraints(sys.TCam, sys.TCol, sys.TIpt, sys.TMenu)
	return &sys
}

// Update updates the menu according to input
func (sys *System) Update(scene *ecs.Scene, ents []ecs.Entity, gin ecs.GameInfo) {
	// TODO: consider fetching ipt and cam through the EntityManager of the scene
	// This way, we don't force you to keep on the same entity
	// Not sure they really need to be components on an entity level anyway...
	// Alternately, consider a "scene" entity that is easy to access that we can
	// attach things like the camera and input state too, for all to use
	for _, e := range ents {
		mnu := e.Component(sys.TMenu).(*Component)
		if !mnu.IsActive {
			continue
		}
		cam := e.Component(sys.TCam).(*cam.Component)
		col := e.Component(sys.TCol).(*mov.ColliderComponent)
		ipt := e.Component(sys.TIpt).(*ipt.Component)
		// TODO: make sure we're not relying on an update to the collider
		// TODO: need a good way to check for input?
		// TODO: only if left button clicked!
		// TODO: this will be wrong for 1 frame?
		// TODO: text seems to print above, might need to look at that.
		// This also means we need a bit of a buffer for now...
		mouse := ipt.State.MousePosition.
			Add(cam.CurrentBounds.Min).
			Add(selectionOffset)
		bc := phy.BoxCollider{Dim: selectionDim}
		bc.Update(mouse)
		collision := col.Collider.Collision(&bc)
		if !collision.IsColliding {
			// default! return color
		} else if !ipt.State.MouseButtonLeft {
			// hover! TODO:
		} else {
			// selected!
			switch sel := mnu.Select.(type) {
			case Transition:
				// TODO: add exit!
				scene.Transition.Type = ecs.TransitionNext
				scene.Transition.Name = sel.Target
				scene.ShouldClose = true
			default:
				// TODO: return error instead?
				log.Fatalln("unknown menu select type")
				break
			}
		}
	}
}

// Close closes the system and releases resources
func (sys *System) Close(ents []ecs.Entity) {

}

// JSONSystemFactory returns a factory function for creating the system
func JSONSystemFactory() ecs.JSONSystemFactory {
	return func() (ecs.System, error) {
		return NewSystem(), nil
	}
}
