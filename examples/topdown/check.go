package main

import "log"

func check(message string, err error) {
	if err != nil {
		log.Fatalln(message, err)
	}
}
