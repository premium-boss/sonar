package main

import (
	"image/color"
	"log"

	ani "gitlab.com/premium-boss/sonar/animation"
	aud "gitlab.com/premium-boss/sonar/audio"
	cam "gitlab.com/premium-boss/sonar/camera"
	"gitlab.com/premium-boss/sonar/ecs"
	mnu "gitlab.com/premium-boss/sonar/examples/topdown/menu"
	frm "gitlab.com/premium-boss/sonar/frame"
	glr "gitlab.com/premium-boss/sonar/glrender"
	ipt "gitlab.com/premium-boss/sonar/input"
	mov "gitlab.com/premium-boss/sonar/movement"
	phy "gitlab.com/premium-boss/sonar/physics"
	ren "gitlab.com/premium-boss/sonar/render"
	txt "gitlab.com/premium-boss/sonar/text"
	"gitlab.com/premium-boss/sonar/tiled"
)

const (
	dt          float32 = 1.0 / 50.0 // 50 fps
	tileSize            = 32
	width               = tileSize * 30
	height              = tileSize * 22
	title               = "Top Down"
	scene1              = "scene.json"
	menuScene           = "menu.json"
	laserEscape         = "laser-escape.wav"
)

func main() {
	// TODO: wrap this in GLGame or something, seq for entity ids
	// TODO: better variable naming (and possibly type naming?) for the manager interfaces, this looks obnoxious
	// TODO: some abstraction around boiler plate, or at least split out into files

	// input management
	iptMgr := ipt.NewGLManager()

	// setup window
	win := ren.NewGLWindow(
		ren.Title(title),
		ren.Dimensions(width, height),
		ren.VSync(),
		ren.OnKey(iptMgr.OnKey()),
		ren.OnMouseButton(iptMgr.OnMouseButton()),
		ren.OnMouseMove(iptMgr.OnMouseMove()))
	defer win.Destroy()

	// gl init
	program := glr.NewProgram()

	// render resource managers
	// TODO: can we combine these? Is it better to separate the interfaces (keep separate), even if implemented
	// by the same concrete struct pointer? e.g. we have a GL instance that implements Projecter, Renderer, etc.
	// Then we don't have to pass around program
	// The systems that need them can accept the interface.
	// TODO: alternately, remove projectionmanager, have a GLCameraDisplaySystem
	// That might fix the inter-scene camera glitch too, since we'd have a new one...
	texMgr := glr.NewGLTextureManager(program)

	// audio resource manager
	audioMgr := aud.NewALManager()
	err := audioMgr.Init()
	check("failed to init audio manager:", err)
	defer func() {
		audioMgr.Close()
		audioMgr.Done()
	}()

	// for convenience, hold all managers
	ctx := Context{
		Program:        program,
		TextureManager: texMgr,
		InputManager:   iptMgr,
		AudioManager:   audioMgr,
		Atlases:        make(map[string]*txt.Atlas),
		TileManager:    tiled.NewManager(),
		Sounds:         make(map[string]*aud.Sound),
		SpatialHash:    phy.NewSpatialHash(tileSize),
	}

	// load registries
	systems := initSystemFactories(ctx)
	renderables := initRenderables(ctx)
	components := initComponentFactories(ctx, renderables)

	// run the game, blocking until close
	run(win, systems, components, ctx)
}

func initSystemFactories(ctx Context) map[string]ecs.JSONSystemFactory {
	// register system factory functions for loading systems
	systems := make(map[string]ecs.JSONSystemFactory)
	ani.RegisterSystems(systems)
	aud.RegisterSystems(systems, ctx.AudioManager, ctx.Sounds)
	cam.RegisterSystems(systems)
	frm.RegisterSystems(systems)
	// TODO: clearcolor: probably not a creation option, but something that should deserialize?
	clearColor := color.RGBA{100, 149, 237, 255} // cornflower blue (TODO: get from scene)
	glr.RegisterSystems(systems,
		ctx.Program, // TODO: adjust these?
		glr.WithTexManager(ctx.TextureManager),
		glr.WithClearColor(clearColor))
	ipt.RegisterSystems(systems, ctx.InputManager)
	mnu.RegisterSystems(systems)
	mov.RegisterSystems(systems, ctx.SpatialHash)
	return systems
}

func initRenderables(ctx Context) ren.RenderableRegistry {
	// register types for all renderable targets
	renderables := ren.NewRenderableRegistry()
	//env.RegisterRenderables(renderables)
	tiled.RegisterRenderables(renderables)
	txt.RegisterRenderables(renderables)
	return renderables
}

func initComponentFactories(ctx Context, renderables ren.RenderableRegistry) map[string]ecs.JSONComponentFactory {
	// collider types: could register more in extension packages
	colliders := phy.NewRegistry()

	// register component factory functions for loading components
	components := make(map[string]ecs.JSONComponentFactory)
	ani.RegisterComponents(components)
	aud.RegisterComponents(components)
	cam.RegisterComponents(components)
	//env.RegisterComponents(components, ctx.TextureManager, ctx.TileMaps)
	frm.RegisterComponents(components)
	ipt.RegisterComponents(components)
	mnu.RegisterComponents(components)
	mov.RegisterComponents(components, colliders)
	ren.RegisterComponents(components, renderables, ctx.TextureManager)
	txt.RegisterComponents(components, ctx.Atlases)
	// TODO: dangerous!!! structs don't work well here, captured by closures before loaded!
	// TODO: also doesn't work w/ nil...
	tiled.RegisterComponents(components, ctx.TextureManager, ctx.TileManager)
	return components
}

func run(win *ren.GLWindow,
	systems map[string]ecs.JSONSystemFactory,
	components map[string]ecs.JSONComponentFactory,
	ctx Context,
) {
	// load scene from data
	scene := loadScene(menuScene, systems, components, ctx)
	for {
		scene.Run(win, dt) // blocks during scene

		log.Println("scene run END")

		// TODO: swap cheap loading scene? then other? clear black?
		scene.Close()
		// release other resources only on scene
		for key, sound := range ctx.Sounds {
			ctx.AudioManager.Unload(sound) // TODO: may need to deal w/ scene specific, etc, better way to clear
			delete(ctx.Sounds, key)
		}
		for key := range ctx.Atlases {
			delete(ctx.Atlases, key)
		}
		ctx.TileManager.Clear()
		ctx.SpatialHash.Clear()
		// TODO: may need to reset projection mgr as well, or accept
		// the starting bounds from menu
		if scene.Transition.Type == ecs.TransitionClose {
			break
		}
		// TODO: anything to clear???? do we need to free batches from OpenGL?
		scene = loadScene(scene.Transition.Name, systems, components, ctx)
		// TODO: keep common resources, audio, fonts, e.g.
		// TODO: could also keep tilemaps, maybe an LRU cache w/ max size
		//
	}
}
