package tickbuf

// Buffer is a simple sequence buffer for tracking input states
type Buffer struct {
	size  uint64
	Ticks []uint64
	Data  []interface{}
}

// NewBuffer ...
func NewBuffer(sz uint64) *Buffer {
	tbuf := Buffer{
		size:  sz,
		Ticks: make([]uint64, sz, sz),
		Data:  make([]interface{}, sz, sz),
	}
	return &tbuf
}

// Set ...
func (tbuf *Buffer) Set(tick uint64, data interface{}) {
	i := tick % tbuf.size
	tbuf.Ticks[i] = tick
	tbuf.Data[i] = data
}

// Get ...
func (tbuf *Buffer) Get(tick uint64) (bool, interface{}) {
	i := tick % tbuf.size
	if tbuf.Ticks[i] == tick && tbuf.Data[i] != nil { // NOTE: nil check not effective on interface anyway...
		return true, tbuf.Data[i]
	}
	return false, nil
}

// Clear ...
func (tbuf *Buffer) Clear(tick uint64) {
	i := tick % tbuf.size
	tbuf.Ticks[i] = 0
	tbuf.Data[i] = nil
}
