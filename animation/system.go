package animation

import (
	"reflect"

	"gitlab.com/premium-boss/sonar/ecs"
)

// Animation constants
const (
	DT = 0.1 // TODO: we should allow this to differ per animation, and even per frame!
)

// System is a system for handling Animation frame updates
type System struct {
	ecs.GameSystem
	TAni reflect.Type
}

// NewSystem creates a new Animation system
func NewSystem() *System {
	a := System{
		TAni: reflect.TypeOf(&Component{}),
	}
	a.SetConstraints(a.TAni)
	return &a
}

// Update updates the Animation system.
func (sys *System) Update(scene *ecs.Scene, ents []ecs.Entity, gin ecs.GameInfo) {
	for _, ent := range ents {
		ani := ent.Component(sys.TAni).(*Component)
		animate(ani, gin.DT)
	}
}

// Close closes the system and releases resources
func (sys *System) Close(ents []ecs.Entity) {

}

// JSONSystemFactory returns a factory function for creating the system
func JSONSystemFactory() ecs.JSONSystemFactory {
	return func() (ecs.System, error) {
		return NewSystem(), nil
	}
}

func animate(ani *Component, dt float32) {
	a := ani.Animators[ani.State.Current] // TODO: may want to play it safe here...
	if ani.State.Current != ani.State.Previous {
		// track flip for Idle state from previous state
		if ani.State.Current == Idle {
			a.Flip = ani.Animators[ani.State.Previous].Flip
		}
		a.Reset()                              // reset the animation, as it has changed
		ani.State.Previous = ani.State.Current // update previous tracker
		return
	}
	ellapse(a, dt)
}

func ellapse(a *Animator, dt float32) {
	a.Time += dt
	for a.Time >= DT {
		a.Time -= DT
		a.I++
		if a.I >= len(a.Frames) {
			a.I = 0
		}
	}
}
