package animation

import "gitlab.com/premium-boss/sonar/ecs"

// RegisterComponents registers all component loader factories in pacakge
func RegisterComponents(reg map[string]ecs.JSONComponentFactory) {
	reg["animation.Component"] = JSONComponentFactory()
}

// RegisterSystems registers system factory loaders in package
func RegisterSystems(reg map[string]ecs.JSONSystemFactory) {
	reg["animation.System"] = JSONSystemFactory()
}
