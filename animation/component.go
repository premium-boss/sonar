package animation

import (
	"encoding/json"

	"gitlab.com/premium-boss/sonar/ecs"
	"gitlab.com/premium-boss/sonar/physics"
)

// State constants bridge movement / actions to animations
// TODO: make this flexible! maybe use the names to index?
// or don't hae in component itself but game logic
const (
	Idle = uint8(iota)
	WalkRight
	WalkLeft
	Jump
	Hit
	Slash
	Punch
	RunRight
	RunLeft
	Climb
	Back
)

// StateTracking tracks state of an entity
type StateTracking struct {
	Current  uint8
	Previous uint8
}

// Component is a component for animations
// TODO: consider an interface within for dealing with static vs dynamic if we
// really care
// TODO: bring out frames to this level? or the start, stop
type Component struct {
	Animators []*Animator
	State     StateTracking // TODO: split this out again! needed in interpolation...
}

// Animator tracks state of frames.
// Frames within must be same size for now. We could be more flexible but it
// makes loading more of a pain than it's worth.
type Animator struct {
	Name string
	Time float32 // TODO: should be an array? match w/ frames?
	//Size   physics.Vec2 // TODO: never set?
	I      int
	Flip   bool
	Frames []physics.Vec2
}

// NewComponent creates a new animation Component
func NewComponent(ani []*Animator) *Component {
	return &Component{
		Animators: ani,
	}
}

// Init initializes state with entity
func (a *Component) Init(ent ecs.Entity) {
	// TODO: generate the rectangles! but do we need bounds?
	// could also be part of main to load from data....
	// or in system. we want these dumb...
}

// OnUnregister hook for component removal
func (a *Component) OnUnregister(ent ecs.Entity) {
}

// NewAnimator creates a new frame set
func NewAnimator(frames []physics.Vec2, flip bool) *Animator {
	return &Animator{
		Frames: frames,
		Flip:   flip,
	}
}

// Reset resets an Animator
func (a *Animator) Reset() {
	a.Time = 0.0
	a.I = 0
}

// GetFrame fetches the current frame of an Animator
func (a *Animator) GetFrame() physics.Vec2 {
	fr := a.Frames[a.I]
	return fr
	//return physics.V(fr.X*a.Size.X, fr.Y*a.Size.Y)
}

// JSONComponentFactory returns factory for creating and loading component
func JSONComponentFactory() ecs.JSONComponentFactory {
	return func(ent ecs.Entity, desc ecs.JSONComponentDescriptor) (ecs.Component, error) {
		ac := Component{}
		err := json.Unmarshal(desc.Data, &ac)
		if err != nil {
			return nil, err
		}
		ac.Init(ent)
		return &ac, nil
	}
}
