package text

import (
	"encoding/json"
	"image/color"

	"gitlab.com/premium-boss/sonar/ecs"
	"gitlab.com/premium-boss/sonar/physics"
	ren "gitlab.com/premium-boss/sonar/render"
)

// Wrap options
// TODO: use standards for split chars and options, etc.
const (
	WrapNone = Wrap(iota) // does not wrap, and ignores right boundary
	WrapChar              // wraps as soon as one character goes beyond boundary
	WrapWord              // splits words on spaces, if word goes beyond, pushes to next line
)

// Wrap options for splitting text onto next line
type Wrap int

// Component for text area with bounds and text display
// NOTE: the font belongs on the system, need strategy similar to Batch
type Component struct {
	Bounds  physics.Rect
	Color   color.RGBA // TODO:  only one per batch right now...
	Message string
	FontTex string
	Wrap    Wrap
	Glyphs  []ren.Sprite
}

// NewComponent creates a new text area component with options
// TODO: consider passing in a fontID, or the texture itself
// fontID may be easier to express in data, as we don't know what the real ID will be
func NewComponent(txt, fontTex string, bounds physics.Rect, wrap Wrap) *Component {
	t := Component{
		Message: txt,
		FontTex: fontTex,
		Bounds:  bounds,
		Wrap:    wrap,
	}
	return &t
}

// Init inits component with Entity
func (c *Component) Init(ent ecs.Entity) {
}

// OnUnregister hook
func (c *Component) OnUnregister(ent ecs.Entity) {}

// Sprite stub for renderable interface
func (c *Component) Sprite() (ren.Sprite, bool) {
	return ren.Sprite{}, false
}

// Sprites returns sprites for renderable interface
func (c *Component) Sprites() ([]ren.Sprite, bool) {
	return c.Glyphs, true
}

// UpdateSprite stub for Renderable interface
func (c *Component) UpdateSprite(sprite ren.Sprite) {
}

// UpdateAt updates a Sprite at index i
func (c *Component) UpdateAt(i int, sprite ren.Sprite) {
	c.Glyphs[i] = sprite
}

// JSONComponentFactory creates a factory function for loading JSON components
// TODO: non pointer for atlas?
func JSONComponentFactory(atlases map[string]*Atlas) ecs.JSONComponentFactory {
	return func(ent ecs.Entity, desc ecs.JSONComponentDescriptor) (ecs.Component, error) {
		// this will be done except glyphs, which need to come through atlas!
		tc := Component{}
		err := json.Unmarshal(desc.Data, &tc)
		if err != nil {
			return nil, err
		}
		atlas := atlases[tc.FontTex]
		tc.Glyphs = atlas.CreateText(&tc)
		tc.Init(ent)
		return &tc, nil
	}
}
