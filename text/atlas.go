package text

import (
	"fmt"
	"image"
	"image/draw"
	"image/png"
	"io/ioutil"
	"os"

	"github.com/golang/freetype"
	"github.com/golang/freetype/truetype"
	"gitlab.com/premium-boss/sonar/physics"
	ren "gitlab.com/premium-boss/sonar/render"
	"golang.org/x/image/font"
	"golang.org/x/image/math/fixed"
)

const gridDimension = 16

// TODO: TextManager? and CreateText areas through that? or create sprites at end?

// Atlas contains font information
type Atlas struct {
	Ascent     float32
	Descent    float32
	LineHeight float32
	FontSize   float32
	Face       font.Face
	TexName    string
	Texture    ren.Texture
	Characters map[rune]Glyph
}

// Glyph stores glyph information for a character within a font
type Glyph struct {
	Advance     float32
	DotLocation physics.Vec2
	Bounds      physics.Rect
	Width       float32
	Height      float32
}

// GlyphArea stores renderable location and texture info for a glyph
// type GlyphArea struct {
// 	Dimensions physics.Vec2 // width and height
// 	Offset     physics.Vec2
// 	Position   physics.Vec2
// 	Sprite     render.Sprite
// }

// NewAtlas creates a new font Atlas (with Texture) from file.
// TODO: separate the font atlas logic from the gl-specific texture
func NewAtlas(file, name string, size, dpi int) (*Atlas, *image.RGBA, error) {
	// read in truetype font file
	fontBytes, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, nil, fmt.Errorf("atlas: error reading font file %s: %v", file, err)
	}
	// load it with freetype
	ttFace, err := freetype.ParseFont(fontBytes)
	if err != nil {
		return nil, nil, fmt.Errorf("atlas: error parsing font %s: %v", file, err)
	}

	// we will draw the font onto an rgba, from which we'll create a texture
	// make a 16x16 grid, where each square is size^2
	boundSize := int(nextPowerOf2(uint32(size * gridDimension)))  // image size in power of 2, keep textures / formats happy
	rgba := image.NewRGBA(image.Rect(0, 0, boundSize, boundSize)) // create the rgba
	fg, bg := image.White, image.Transparent                      // colors for bitmap
	draw.Draw(rgba, rgba.Bounds(), bg, image.ZP, draw.Src)        // draw black background
	opts := truetype.Options{
		Size:    float64(size),
		DPI:     float64(dpi),
		Hinting: font.HintingNone, // TODO:
	}
	// create atlas, font.Face with opts
	atlas := Atlas{
		Face:       truetype.NewFace(ttFace, &opts),
		FontSize:   float32(size),
		Characters: make(map[rune]Glyph),
		TexName:    name,
	}
	metrics := atlas.Face.Metrics()
	atlas.Ascent = fixedToFloat(metrics.Ascent)
	atlas.Descent = fixedToFloat(metrics.Descent)
	atlas.LineHeight = fixedToFloat(metrics.Height)
	d := &font.Drawer{Dst: rgba, Src: fg, Face: atlas.Face} // set up font drawer

	// loop characters, draw onto the RGBA, and store positions
	// we're grabbing main ASCII ones for now, first 32 seem to be junk...
	// TODO: store charsets as constants / var somewhere
	for i := 32; i < 128; i++ {
		r := rune(i)
		c := string(r)
		x, y := i%gridDimension, i/gridDimension
		// draw the character on the texture: 26.6 fixed-point number
		// (fractional part has 6 bits of precision)
		bds, advance, ok := atlas.Face.GlyphBounds(r)
		if !ok {
			continue // shouldn't happen
		}
		xLoc, yLoc := x*size, y*size
		d.Dot = fixed.P(xLoc, yLoc)
		d.DrawString(c)
		// store glyph info in map:
		// we provide a slight buffer for the rendered area. Having extra
		// transparency doesn't matter, but drawing at the exact borders seems to
		// clip. As long as we're consistent, and that we move the proper
		// advance relative to dot, and that the additional buffer doesn't cause
		// any collisions, then we're okay. This seems to be the case.
		maxX, maxY := float32(bds.Max.X.Ceil()), float32(bds.Max.Y.Ceil())
		minX, minY := float32(bds.Min.X.Floor()), float32(bds.Min.Y.Floor())
		//maxX, maxY := fixedToFloat(bds.Max.X), fixedToFloat(bds.Max.Y)
		//minX, minY := fixedToFloat(bds.Min.X), fixedToFloat(bds.Min.Y)
		bdsRect := physics.R(physics.V(maxX, maxY), physics.V(minX, minY))
		glyph := Glyph{
			Advance:     fixedToFloat(advance),
			DotLocation: physics.V(float32(xLoc), float32(yLoc)),
			Bounds:      bdsRect,
			Width:       maxX - minX,
			Height:      maxY - minY,
		}
		atlas.Characters[r] = glyph
	}

	// create the texture
	// NOTE: we now expect texture to be assigned from outside, after created form rgba
	//atlas.Texture = NewTextureFromRGBA(rgba)
	if err != nil {
		return nil, nil, err
	}
	return &atlas, rgba, nil
}

// CreateText sets up a string inside the bounds and returns a slice of
// calculated glyph areas for the position and texture
func (a *Atlas) CreateText(comp *Component) []ren.Sprite {
	// TODO: don't work sprite batch here! pass in a text area, create some
	// structure that the render system can convert to a batch. this way we
	// avoid the import and keep agnostic
	// TODO: the main issue will be: where do we create the texture?
	// if we really want to create here, either move texture to its own package,
	// move this to render, or return something for which render will know to create
	// the texture...
	prevC := rune(-1)
	x := comp.Bounds.Min.X
	baseY := comp.Bounds.Min.Y
	sprites := make([]ren.Sprite, 0, len(comp.Message))
	for _, c := range comp.Message {
		if prevC >= 0 {
			x += fixedToFloat(a.Face.Kern(prevC, c))
		}
		glyph, ok := a.Characters[c]
		if !ok {
			continue
		}
		dim := physics.V(glyph.Width, glyph.Height)
		off := physics.V(glyph.DotLocation.X+glyph.Bounds.Min.X,
			glyph.DotLocation.Y+glyph.Bounds.Min.Y)
		pos := physics.V(x, baseY+glyph.Bounds.Min.Y) // move y so that upper left starts at correct pt relative to dot
		sprite := ren.NewSpriteOffset(dim, off, a.Texture).Moved(pos)
		sprite.Color = comp.Color
		sprites = append(sprites, sprite)
		// advance position for next, moving down a row if needed
		x += glyph.Advance
		switch comp.Wrap {
		case WrapNone:
			break // nothing to do
		case WrapWord:
			fallthrough // TODO:!
		case WrapChar:
			if x >= comp.Bounds.Max.X {
				// we need to move down a row and reset
				baseY += a.LineHeight
				x = comp.Bounds.Min.X
			}
		}
		prevC = c // track previous for kern
	}
	return sprites
}

// nextPowerOf2 finds next power of 2 >= n
// Bit Twiddling Hacks: https://graphics.stanford.edu/~seander/bithacks.html
func nextPowerOf2(n uint32) uint32 {
	n--
	n |= n >> 1
	n |= n >> 2
	n |= n >> 4
	n |= n >> 8
	n |= n >> 16
	n++
	return n
}

// fixedToFloat converts a fixed 26_6 int to a float32
func fixedToFloat(fx fixed.Int26_6) float32 {
	return float32(fx) / 64.0
}

// writePNG writes out an rgba to a png file for debugging / visualization
func writePNG(name string, rgba *image.RGBA) error {
	f, err := os.Create(name)
	if err != nil {
		return err
	}
	png.Encode(f, rgba)

	return nil
}
