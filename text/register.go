package text

import (
	"gitlab.com/premium-boss/sonar/ecs"
	ren "gitlab.com/premium-boss/sonar/render"
)

// RegisterRenderables registers renderables from the text package with the render registry
func RegisterRenderables(reg ren.RenderableRegistry) {
	reg["text.Component"] = &Component{}
}

// RegisterComponents registers all loader factories in pacakge
func RegisterComponents(reg map[string]ecs.JSONComponentFactory, atlases map[string]*Atlas) {
	reg["text.Component"] = JSONComponentFactory(atlases)
}
